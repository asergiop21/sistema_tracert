<?php
include("menu.php");
require("Librerias/conn.php");
require("Controllers/PagosController.php");
require("Controllers/ClientesController.php");
$db = Conec_con_pass();
$clie = $_REQUEST['clie'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<script type="text/javascript" src="Librerias/jquery.js"></script>
<script type='text/javascript' src="Librerias/jquery.bgiframe.min.js"></script>
<script type='text/javascript' src="Librerias/jquery.ajaxQueue.js"></script>
<script type='text/javascript' src="Librerias/thickbox-compressed.js"></script>
<script type='text/javascript' src="Librerias/jquery.autocomplete.js"></script>
<script type='text/javascript' src="Librerias/localdata.js"></script>
<link rel="stylesheet" type="text/css" href="Librerias/jquery.autocomplete.css" />
</script>
</head>
<body>
    <?php
$sql =pg_exec($db, "select * from (pagos inner join clientes on pagos.idclientes = clientes.idclientes)inner join instalaciones on instalaciones.idclientes = clientes.idclientes where clientes.idclientes = '$clie' order by pagos.fecha_ing asc" );
$sql_pa = pg_exec($db,"select * from (pagos inner join clientes on pagos.idclientes = clientes.idclientes)inner join instalaciones on instalaciones.idclientes = clientes.idclientes where clientes.idclientes = '$clie' order by pagos.fecha_ing asc, idpago asc");
    ?>
        <div class="container">
        	<div class="span12">
        		<div class="span12">
					<ul class="nav nav-tabs">
	                        <li><a href="historial.php?clie=<?php echo $clie;?>" ><img src="image/historial.gif" width="25" align="middle">Historial</a></li>
	                        <li><a href="reclamo.php?clie=<?php echo $clie;?>">Visitas Tec.</a></li>
	                        <li class="active"><a href=pagos.php?clie=<?php echo $clie;?>>Pagos</a></li>
	                        <li><a href="<?php echo BASE_URL ?>views/clientes/editar.php?cliente_id=<?php echo $clie;?>"><img src="images/datos.gif" width="25" align="middle">Datos </a></li>
	                        <li><a href="novedades_diariasht.php"><img src="image/libreta.jpg" width="25" align="middle">Novedades Pendientes</a></li>
	                        <li><a href="otros.php">Novedades Otros</a></li>

					</ul>
				</div>
    		<div class="span12">
    			<div class="span6">
     <?php $row_ca = pg_fetch_object($sql) ?>
     <b>Cliente:</b> <?php echo $row_ca->apellido.", ".$row_ca->nombre ?>  - <b>Vel:</b> <?php echo $row_ca->velocidad ?>  <br>
     <b>Abono:</b> <?php echo importe_plan_cliente($clie)->importe;  ?> 

                <b>Estado:</b> <?php $status = $row_ca->elim_ser;

                if ($status == '0')
                {
                    echo "<b><font color='red'>Deshabilitado</font></b>";
                }
                elseif ($status == '1')
                {
                    echo "<b>Habilitado</b> ";
                }
                else
                {
                    echo "<b><font color='darkorange'>Aviso</font></b> ";
                }
$sql_rec = pg_exec($db, "Select * from reclamos where idclientes = '$clie' and terminado !='t' ");
$fdev_rec = pg_num_rows($sql_rec);

If ($fdev_rec > 0)
{
    echo "<b> Soporte:</b> <font color='red'>Pendiente</font>";
}
else
{
    echo "<b> Soporte:</b> Ninguno Pend.</b>";
}
?>
</div>
<div class="span5">
		<h5>Deuda Actual: <?php echo $total_deuda = pg_fetch_object(get_listar_pagos_cliente($clie)[0])->total ;?> </h5>	
		<h5>Saldo Proximo Mes: <?php  echo importe_plan_cliente($clie)->importe - $total_deuda;?> </h5>	
</div>

    </div>
    </div>
    <div class="span12">
    <form  name="frmdatos">
<table id="tlbnroins" class="table" name = "tlbnroins" >

		<tr>
			<th>#</th>
			<th>Fecha</th>
			<th>Detalle</th>
			<th>Debe</th>
			<th>Haber</th>
			<th>Deuda</th>
			<th>Marcar</th>
	</tr>
		<?php

			while  ($row = pg_fetch_object($sql_pa))
			{
				$sum_import =$row->importe_deuda + $sum_import;
				$sum_pagado = $row->importe_pagado + $sum_pagado;
				$deuda = $sum_pagado - ($sum_import );
				$deuda_total =   $sum_pagado - $sum_import;
		?>
		<tr>
			<td ><?php echo $row->idpago;?></td>

			<td><?php echo cambiaf_a_normal($row->fecha_ing);?></td>

			<td><?php echo $row->detalle;?></td>

			<td><?php echo "$ ".number_format($row->importe_deuda, 2, ",", "."); ?></td>

			<td><?php echo "$ ".number_format($row->importe_pagado, 2, ",", "."); ?></td>

			<td><?php echo "$ ". number_format($deuda, 2, ",", ".");  ?></td>

			
		</tr>


	  <?php
	}
	?>

	<tr>

		<td></td>

		<td></td>

		<td></td>

		<td><b><i><?php echo "$ ".number_format($sum_import, 2 , ",", ".") ?></i></b></td>

		<td><b><i><?php echo "$ ".number_format($sum_pagado, 2, ",", ".") ?></i></b></td>

		<td><b><i><?php echo "$ ".number_format($deuda_total, 2, ",", ".") ?></i></b></td>

		<td></td>

	</tr>
</table >
    </form>
        </div>
        </div>
</body>
</html>