<?php
include("menu.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- $Id: index.html,v 1.15 2005/03/05 14:38:10 mishoo Exp $ -->

<head>
<meta http-equiv="content-type" content="text/xml; charset=utf-8" />

<link rel="stylesheet" type="text/css" media="all" href="skins/aqua/theme.css" title="Aqua" />


<!-- import the calendar script -->
<script type="text/javascript" src="js/calendar.js"></script>

<!-- import the language module -->
<script type="text/javascript" src="lang/calendar-es.js"></script>

<!-- other languages might be available in the lang directory; please check
your distribution archive. -->

<!-- helper script that uses the calendar -->
<script type="text/javascript">

var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}

var patron = new Array(2,2,4)
function mascara(d,sep,pat,nums){
if(d.valant != d.value){
val = d.value
largo = val.length
val = val.split(sep)
val2 = ''
for(r=0;r<val.length;r++){
val2 += val[r] 
}
if(nums){
for(z=0;z<val2.length;z++){
if(isNaN(val2.charAt(z))){
letra = new RegExp(val2.charAt(z),"g")
val2 = val2.replace(letra,"")
}
}
}
val = ''
val3 = new Array()
for(s=0; s<pat.length; s++){
val3[s] = val2.substring(0,pat[s])
val2 = val2.substr(pat[s])
}
for(q=0;q<val3.length; q++){
if(q ==0){
val = val3[q]
}
else{
if(val3[q] != ""){
val += sep + val3[q]
}
}
}
d.value = val
d.valant = val
}
}


function esFechaValida(fecha){
    if (fecha != undefined && fecha.value != "" ){
        if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha.value)){
            alert("formato de fecha no v�lido (dd/mm/aaaa)");
            return false;
        }
        var dia  =  parseInt(fecha.value.substring(0,2),10);
        var mes  =  parseInt(fecha.value.substring(3,5),10);
        var anio =  parseInt(fecha.value.substring(6),10);
 
    switch(mes){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8: 
        case 10:
        case 12:
            numDias=31;
            break;
        case 4: case 6: case 9: case 11:
            numDias=30;
            break;
        case 2:
            if (comprobarSiBisisesto(anio)){ numDias=29 }else{ numDias=28};
            break;
        default:
            alert("Fecha introducida err�nea");
            return false;
    }
 
        if (dia>numDias || dia==0){
            alert("Fecha introducida err�nea");
            return false;
        }
        return true;
    }
}
 

function comprobarSiBisisesto(anio){
if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) {
    return true;
    }
else {
    return false;
    }
}

function consultar ()
{
var f_desde;
var f_hasta;
var zon;
var rep;
var ordenar

f_desde = document.all.txtf_desde.value;
f_hasta = document.all.txtf_hasta.value;
zon = document.all.txtloc.value;
rep = document.all.txtrep.value;
ordenar = document.all.sel.options[document.all.sel.selectedIndex].value;
ape = document.all.txtape.value;



/*if (f_desde == "" || f_hasta == "")
{
	alert ("Llene los dos campos de fecha");
}
else
{*/
document.location.href = "consulta_bajas.php?f_desde=" + f_desde + "&f_hasta=" + f_hasta + "&zon=" + zon + "&rep="+ rep + "&ordenar=" + ordenar +"&ape="+ ape;
//}

 
//return;
//}
}


</script>

<link rel="stylesheet" type="text/css" href="CSS/estilo_nuevo.css">
<link rel="stylesheet" type="text/css" href="CSS/estilo.css">

</head>
<body >
<h1 class="titulo">CONSULTAS CLIENTES BAJAS</h1> <hr width="60%" align="left">
<hr>
<form name="consul" >
<div id="contenedor">
	<table align="center" border="1" width="100%">
		<tr>
			<td class="nombre_campos">DESDE:</td>
			<td ><input type="text" name="txtf_desde" id="txtf_desde" maxlength="10"  onKeyUp="mascara(this,'/',patron,true);" onBlur="esFechaValida(this);"><input type="reset" value=" ... "
			onclick="return showCalendar('txtf_desde', '%d/%m/%Y');">  </td>
			<td class="nombre_campos">HASTA:</td>
			<td><input type="text" name="txtf_hasta" id="txtf_hasta" maxlength="10" onKeyUp="mascara(this,'/',patron,true);" onBlur="esFechaValida(this);"><input type="reset" value=" ... "
			onclick="return showCalendar('txtf_hasta', '%d/%m/%Y');"></td>
		</tr>
		<tr>
		<td class="nombre_campos">* ZONA </td>
		<td><span class="Estilo6">
    	  <?php
		require("Librerias/conn.php");
    	  // Conexion, seleccion de base de datos
				$conexion= Conec_con_pass();
				
				// Realizar una consulta SQL
				$consulta  = "SELECT * FROM zonas";
				$resultado = pg_query($consulta) or die('La consulta fall&oacute;: ' . pg_error());

				// Impresion de resultados en HTML

				echo "<select name='txtloc' id='txtloc' tabindex='9'>";
				echo "<option  selected>";
					while ($linea = pg_fetch_row($resultado)) {
   
   					//foreach ($linea as $valor_col) {
    			
       			
				echo " <option value='$linea[1]'> $linea[1] </option>";
				 }
				echo "</select>";
				// Liberar conjunto de resultados
				pg_free_result($resultado);
				// Cerrar la conexion
				pg_close($conexion);  
    		?>
    </span> </td>
   
    <td class="nombre_campos">* REPETIDORA </td>
    <td><span class="Estilo6">
    		<?php
				
    			// Conexion, seleccion de base de datos
				$conexion= Conec_con_pass();

				// Realizar una consulta SQL
				$consulta  = "SELECT * FROM repetidoras";
				$resultado = pg_query($consulta) or die('La consulta fall&oacute;: ' . pg_error());

				// Impresion de resultados en HTML

				echo "<select name='txtrep' id='txtrep' tabindex='9'>";
				echo "<option  selected>";
					while ($linea = pg_fetch_row($resultado)) {
   
   					//foreach ($linea as $valor_col) {   		
   				echo " <option value='$linea[1]'> $linea[1] </option>";
				 }
				echo "</select>";
				// Liberar conjunto de resultados
				pg_free_result($resultado);
				// Cerrar la conexion
				pg_close($conexion);  
    		?>
    </span> </td>
   			</tr>
		<label>
   			<tr>
   				<td class="nombre_campos">APELLIDO:</td>
					<td><input type="text" name="txtape" id="txtape" onBlur="this.value=this.value.toUpperCase();"  ></td>
   				
   				
   					<td class="nombre_campos">ORDENAR POR:</td>
   				<td><select name="sel" id="sel" >
   					<option value="1">Localidad</option>
   					<option value="0">Fecha</option>
   				</td>
   				</select>
   			</tr>
   		</label>
   			<tr>
			<td height="28" colspan="6" align="center"><input class="boton" type="button" name="consulta" id="consulta" value="consultar" onClick="consultar()">
			
		</tr>
	</table>
	</div>
</form>
</body>
</html>