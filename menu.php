 <?php
session_start();
require_once 'Controllers/AplicacionController.php';
  define('DS', DIRECTORY_SEPARATOR);
  define('ROOT', realpath(dirname(__FILE__)) . DS);
  define('APP_PATH', ROOT . 'application' . DS);
  define('BASE_URL', url());
  /*if ($_SESSION['dir_ip'] == '::1'){
  define('BASE_URL', 'http://localhost/Sistemas_tracert/');
  }else
  {
  if ($_SESSION['dir_ip'] == '10.0.100.152'){
    define('BASE_URL', 'http://192.168.200.2/sistema/');
  }else
  {
    define('BASE_URL', 'http://200.5.65.83/sistema/');
  }
  }
  */
 
  $conexion= pg_connect("host=localhost port=5432 user=postgres password=postgres dbname=sistemas_tracert");
  //VARIABLE GLOBAL
  global $db;
  $db = $conexion;
  require_once "clases/user_line.php";

  $user_ac = usuarios_activos();
  if ($_SESSION['access'] != 'true')
  {
          print "<meta http-equiv=Refresh content=\"0 ; url=index.php\">";
  }
?>

<html>
    <head>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/bootstrap/bootstrap.js"></script>
    <script type='text/javascript' src="<?php echo BASE_URL ?>js/jquery.autocomplete.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/highcharts.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/exporting.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/funciones.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>assets/javascript/planes.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>js/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>assets/javascript/clientes.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>assets/javascript/cantidad_clientes.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>assets/javascript/wizard.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL ?>assets/javascript/cupones.js"></script>

    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/thickbox/thickbox.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/bootstrap/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/estilo_nuevo.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/bootstrap/bootstrap-responsive.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/menu.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/datepicker/css/datepicker.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/jquery.autocomplete.css" / >
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL ?>CSS/wizard.css" / >

    <script type="text/javascript">
function abreFrame(id) {
document.getElementById(id).click();
//cuidado con esto que con Mozilla no corre por el tratamiento distinto de eventos.
}
     </script>
    </head>

 <?php
$perfil = $_SESSION['perfil'];
$perfil_user = $_SESSION['perfil_user'];
$user = $_SESSION['usuario'];
$iduser = $_SESSION['iduser'];
$us_tim = $_SESSION['us_tim'];
$nom_ape = $_SESSION['nom_ape'];
$_SESSION['row_for_page'] = 20;
$sql_perfil = pg_exec($conexion, "Select * from perfil where idperfil = '$perfil' ");

 while ($row_pf = pg_fetch_object($sql_perfil))
 {
  $clientes = $row_pf->pf_clientes;
  $pagos = $row_pf->pf_pagos;
  $instalaciones = $row_pf->pf_inst;
  $soporte = $row_pf->pf_soporte;
  $nov_dia = $row_pf->pf_nov_dia;
  $repo = $row_pf->pf_reportes;
  $teso = $row_pf->pf_tesoreria;
  $cpanel = $row_pf->pf_panel_cont;
  $reload = $row_pf->pf_reload;
}

$can_cl_maipu = pg_exec($conexion, "Select * from clientes inner join instalaciones on clientes.idclientes = instalaciones.idclientes inner join planes on planes.id = instalaciones.plan_id where localid IN (14,17,22,24,25,26,28,29,30,31,33,35,42,43,44,49,53,55,56,57,58,59,60,61,62,63,64,65) and elim_clie = 'f' and importe > '0' and elim_ser > '0' and tipo_cliente_id = 1  " );
$fdev_cl_maipu = pg_num_rows($can_cl_maipu);

$can_cl = pg_exec($conexion, "Select * from clientes inner join instalaciones on clientes.idclientes = instalaciones.idclientes inner join planes on planes.id = instalaciones.plan_id  where localid NOT IN (14,17,22,24,25,26,28,29,30,31,33,35,42,43,44,49,53,55,56,57,58,59,60,61,62,63,64,65) and elim_clie = 'f' and importe > '0' and elim_ser > '0' and tipo_cliente_id = 1   " );
$fdev_cl = pg_num_rows($can_cl);

?>

<div class="navbar navbar">
  <div class="navbar-inner">
      <!-- btn-navbar es el botón que reemplaza a los links ciuando la resolución es muy chica -->
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <!-- Marca o Logo -->
      
      <div class="nav-collapse"> 
    <?php if ($perfil_user == 'A'){ ?>
    <ul class="nav">
        <li class="dropdown"><a href="#"><b>G: <?php echo $fdev_cl ." - M:". $fdev_cl_maipu ?></b> </a>

    </ul>


     <?php  }   if ($clientes == '1'){ ?>
        <ul class="nav">
          <li class="dropdown"><a href="<?php echo BASE_URL ?>consulta_clientesht.php" class="dropdown-toggle" data-toggle="dropdown">Clientes<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo BASE_URL ?>views/clientes/nuevo.php">Nuevo</a></li>
              <li><a  href="<?php echo BASE_URL ?>views/clientes/index.php">Consultas Clientes</a></li>
              <?php if ($perfil_user != "D") {?>
              <li><a  href="<?php echo BASE_URL ?>consulta_bajasht.php">Bajas Clientes</a></li>
    <?php } ?>
          </ul>
             </ul>
      </li>
    </ul>

     <?php   } if ($pagos == '1'){ ?>

    <ul class="nav">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Pagos y Servicios<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo BASE_URL ?>views/cuentas/">Resumen de Cuentas</a> </li>
            <li><a href="<?php  echo BASE_URL ?>views/pagos/index.php">Pagos Electronicos</a></li>
            <li><a href="<?php echo BASE_URL ?>views/generate_files/files.php">Generar archivo Pagomiscuentas</a></li>
            <li><a href="<?php echo BASE_URL ?>views/reportes/cupones.php">Imprimir Cupon</a></li>
            <li><a href="<?php echo BASE_URL ?>views/administracion/avisos_cortesht.php">Avisos y Cortes</a></li>
            <li><a href="<?php echo BASE_URL ?>mensajes_deuda.php">Listado de SMS</a></li>
          </ul>
          </li>
        </ul>
<?php }  if ($instalaciones == '1' ){ ?>

      <ul class="nav">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Instalaciones y Stock<b class="caret"></b></a>
          <ul class="dropdown-menu">

            <li class="dropdown-submenu"> 
              <a href="#">Instalaciones</a>
                <ul class="dropdown-menu">
            <li><a href="<?php echo BASE_URL ?>views/clientes_instalar/clientes_a_instalar.php">Instalaciones Abonadas</a></li>
            <li><a href="<?php echo BASE_URL ?>consulta_instalacion_fechaht.php">Consultas por Fecha</a></li>          
                     
                </ul>
             </li>
            <?php if ($perfil_user != "D") {?>
            <li class="dropdown-submenu"> 
              <a href="#">Equipos</a>
                <ul class="dropdown-menu">
                      <li><a href="<?php echo BASE_URL ?>alta_equiposht.php">Alta de Equipos</a></li>
            <li><a href="<?php echo BASE_URL ?>consultas_equiposht.php">Consultas Equipos</a></li>
            <li><a href="<?php echo BASE_URL ?>views/clientes/consulta_ip.php">Consultas por Ip's</a></li>
                </ul>
             </li>
        <?php } ?>
          </ul>
          </li>
        </ul>


<?php } if (( $instalaciones == '1' || $pagos =='1') && $perfil_user != "D") { ?>
      <ul class="nav">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Promesas<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo BASE_URL ?>views/cuentas/cuenta_morosos.php">Deuda</a></li>
            <li><a href="<?php echo BASE_URL ?>views/cuentas/agenda.php">Promesas</a></li>
            <li><a href="<?php echo BASE_URL ?>views/cuentas/clientes_tratar.php">Tratamientos</a></li>
            <li><a href="<?php echo BASE_URL ?>listado_prome.php">Resumen llamadas</a></li>
            <li><a href="<?php echo BASE_URL ?>aplicar_avisos_cortes.php">Aplicar Avisos y Cortes</a></li>
             <li><a href="<?php echo BASE_URL ?>consulta_bajas_tratamiento.php">Consulta tratamientos</a></li>

          </ul>
          </li>
        </ul>

 <?php } if ($soporte == '1') { ?>
   <ul class="nav">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Soporte <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo BASE_URL ?>imprimir_reglamosht.php?tipo=9000&cliente_elim=false">Imprimir Soporte</a></li>
            <li><a href="<?php echo BASE_URL ?>imprimir_reglamosht.php?tipo=9001&cliente_elim=true">Imprimir Retiros</a></li>
            <li><a href="<?php echo BASE_URL ?>imprimir_reglamosht.php?tipo=9002&cliente_elim=false">Imprimir Cambios</a></li>
    <?php if ($perfil_user != "D") {?>
            <li><a href="<?php echo BASE_URL ?>reportes_reclamos.php">Soporte Generales</a></li>
            <li><a href="<?php echo BASE_URL ?>repo_tec_sop.php">Soporte Asig Tec</a></li>
            <li><a href="<?php echo BASE_URL ?>cambio_eqht.php">Cambio de Equipo</a></li>
            <li><a href="<?php echo BASE_URL ?>consulta_sop_techt.php">Consulta Soporte</a></li>
            <li><a href="<?php echo BASE_URL ?>grafico_items_opciones.php">Grafico Soporte</a></li>
            
<?php }?>
          </ul>
          </li>
        </ul>

    
<?php }  if ($teso == '1'){ ?>
    <ul class="nav">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tesoreria<b class="caret"></b></a>
          <ul class="dropdown-menu">
              <li><a href="<?php echo BASE_URL ?>repo_pagosht.php">Pagos </a></li>
              <li><a href="<?php echo BASE_URL ?>views/caja_diaria/caja_diaria.php">Caja Diaria </a></li>
              <li><a href="<?php echo BASE_URL ?>historial_caja.php">Historial Caja </a></li>
              <li><a href="<?php echo BASE_URL ?>views/facturacion/index.php">Consulta Facturacion </a></li>
          </ul>
          </li>
        </ul>

<?php } ?>
        <ul class="nav">
           <li class="dropdown" ><a   href="<?php echo BASE_URL ?>novedades_diariasht.php">Novedades Diarias</a></li>
        </ul>

<?php if ($cpanel == '1'){ ?>
  <ul class="nav">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" >Panel de Control<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li class="dropdown-submenu"> <a href="#">Abonos y Recargos</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo BASE_URL?>views/administracion/insertar_abonos.php">Abono Mensual</a></li>
                    <li><a href="<?php echo BASE_URL ?>insertar_abono11.php">Recargos Dia 11 </a></li>
                    <li><a href="<?php echo BASE_URL ?>insertar_abono21.php">Recargos Dia 19</a></li>
                </ul>
             </li>
              <li class="dropdown-submenu"> <a href="#">Reportes</a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo BASE_URL ?>repopagosaux.php">Deudores</a></li>
                  <li><a href="<?php echo BASE_URL ?>imprimir_repozonas.php">Pagos Zonas</a></li>
                  <li><a href="<?php echo BASE_URL ?>consulta_stht.php">Reclamos General</a></li>
                  <li><a href="<?php echo BASE_URL ?>consulta_pago1ht.php">Consulta de pagos</a></li>
                  <li><a href="<?php echo BASE_URL ?>consulta_sop_techt.php">Consulta Soporte</a></li>
                  <li><a href="<?php echo BASE_URL ?>reporte_stht.php">Reporte Novedades x Usuario</a></li>
                  <li><a href="<?php echo BASE_URL ?>consulta_sp_userht.php">Consulta Soporte por Usuario</a></li>
                  <li><a href="<?php echo BASE_URL ?>informes_reclamos.php">Informes reclamos</a></li>
                  <li><a href="<?php echo BASE_URL ?>views/reportes/cantidad_clientes.php">Cantidad Clientes</a></li>
               </ul>
              </li>

                <li><a href="<?php echo BASE_URL ?>views/nodos/index.php">Nodos</a></li>
                <li><a href="<?php echo BASE_URL ?>views/usuarios/index.php">Usuarios</a></li>
                <li><a href="<?php echo BASE_URL ?>views/planes/index.php">Planes</a></li>
                <li class="dropdown-submenu"> <a href="#">Mikrotiks</a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo BASE_URL ?>views/mikrotiks/index.php">Agregar Mikrotik</a></li>
                    <li><a href="<?php echo BASE_URL ?>views/mikrotiks_bordes/index.php">Agregar Mikrotik Borde</a></li>
                    <li><a href="<?php echo BASE_URL ?>views/mikrotiks/add_queue_mkt.php"> Cargar queue principal</a></li>
                  </ul>
                </li>
                

            </ul>
          </li>
      </ul>
        <?php } ?>
        <ul class="nav pull-right">
          <li><a href="<?php echo BASE_URL ?>cerrar.php">Cerrar Sesion</a></li>
        </ul>
    </div>


  </div>
</div>


