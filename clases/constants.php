<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

abstract class FEComprobante
{
    const FacturaA = '01';
    const NotaDebitoA = '02';
    const NotaCreditoA = '03';
    const ReciboA = '04';

    const FacturaB = '06';
    const NotaDebitoB = '07';
    const NotaCreditoB = '08';
    const ReciboB = '09';

    const FacturaC = '10';
    const NotaDebitoC = '12';
    const NotaCreditoC = '13';
    const ReciboC = '15';
}

abstract class FEConcepto
{
    const Productos = '01';
    const Servicios = '02';
    const ProductosYServicios = '03';
}

abstract class FEDocumento
{
    const CUIT = 80;
    const CUIL = 86;
    const DNI = 96;
}

abstract class FEIVA
{
    const Iva00 = '03';
    const Iva10 = '04';
    const Iva21 = '05';
    const Iva27 = '06';

    public static function IvaMultiplicador($iva)
    {
        $IVA = ['03' => 0.0, '04' => 0.105, '05' => 0.21, '06' => 0.27];
        return $IVA[$iva];
    }
}

abstract class FEEntorno
{
    const Testing = 'TESTING';
    const Produccion = 'PRODUCCION';

    public static function WSURL($ent)
    {

        $FEURLS = [
        
          'TESTING' =>
                ['WSAA' =>
                    'https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL',
                 'WSFE' =>
                    'https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL'],
           'PRODUCCION' =>
                ['WSAA' =>
                    'https://wsaa.afip.gov.ar/ws/services/LoginCms?WSDL',
                 'WSFE' =>
                    'https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL']
           ];

        return $FEURLS[$ent];
    }
}

// Archivos temporales. En caso de ejecutar en un entorno windows hay que
// adaptar las rutas a un directorio temporal o colocar rutas relativas dentro
// del mismo directorio del script.
abstract class FEPathsTech
{
    const TokenSignPath = '/tmp/afip_token.xml';
    const TokenRequestPath = '/tmp/afip_token_request.xml';
    const TokenTempFile = '/tmp/afip_token_request.tmp';
}

abstract class FEPathsProtec
{
    const TokenSignPath = '/tmp/afip_token_protec.xml';
    const TokenRequestPath = '/tmp/afip_token_request_protec.xml';
    const TokenTempFile = '/tmp/afip_token_request_protec.tmp';
}
