<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

require 'bill.class.php';

class PruebaAfip
{
    /**
     * Función para autorizar un lote e imprimir los resultados.
     *
     * @param $bill Lote de comprobantes.
     **/
    public static function autorizarImprimirComprobantes($bill)
    {

        try {
            // Autorizamos el lote
            $bill->autorizar();

            /*if ($bill->resultado == 'A') {
                echo "<p>Se autorizaron todas las facturas del lote.</p>";
            } else if ($bill->resultado == 'P') {
                echo "<p>Se autorizaron <strong>algunas</strong> facturas.</p>";
            } else {
                echo "<p>No se autorizó ningún comprobante</p>";
            }

            echo "<br>";
*/
            // Las facturas autorizadas van a estar con sus respectivos datos en
            // el arreglo de invoices de $bill
            $invoices = $bill->comprobantes();
          print_r($invoices);
return $invoices;
            foreach ($invoices as $i) {
               echo "<p>Comprobante Nº <strong>"
                    . $i->numero . "</strong></p>";

                if ($i->resultado == 'A') {
                    echo "<p>CAE: " . $i->cae . "</p>";
                    echo "<p>Fecha Vto CAE: " .
                        $i->caeFechaVto->format('d-m-Y') . "</p>";
                    echo "<p>Resultado: autorizado correctamente</p>";
                } else {
                    echo "<p>Resultado: rechazado</p>";
                }

                // Pueden haber observaciones tanto si el comprobante es
                // aceptado o rechazado.
                if ($i->observacion) {
                    echo "<p>Observación: " . $i->observacion . "</p>";
                    echo "<p>Código observación: " . $i->obsCode . "</p>";
                }

                echo "<br><br>";
                
            }

        } catch (AFIPErrorException $e) {
            echo "<p>Ocurrió un error al autorizar el lote: " .
                $e->getMsg() . "</p>";
        }

        return $invoices;
    }

}

function initial($invoices, $tipo_factura, $organizacion, $codigo_entidad){
// Configuración general.
//
// El entorno sirve para determinar la URL del servicio.
//
// El tipo de comprobante se debe especificar para todo el lote. Si hay que
// autorizar dos tipos de comprobantes distintos (por ej. una Factura A y una
// Factura B) éstos deben ir en lotes diferentes.
//
// Las opciones de debug son útiles tenerlas activadas siempre (incluso en
// producción) por si Ocurrióe algún problema, que quede registro en los logs.
//


if ($organizacion == 'techtron' ){
$opciones = [
   'entorno' => FEEntorno::Testing,
    'certificado' => dirname(__FILE__).'/cert/homologacion.crt',
    'clave_privada' => dirname(__FILE__).'/cert/pk.key',
    //'entorno' => FEEntorno::Produccion,
    //'certificado' => dirname(__FILE__).'/cert/tech_16c721525955b90d.crt',
    //'clave_privada' => dirname(__FILE__).'/cert/privada.key',
    'punto_de_venta' => 3,
    'cuit_emisor' =>  30711188165,
    //'tipo_comprobante' => FEComprobante::FacturaB,
    'debug_wsaa' => true,
    'debug_wsfe' => true
];
}
if ($organizacion == 'protec' ){
$opciones = [
   'entorno' => FEEntorno::Testing,
    'certificado' => dirname(__FILE__).'/cert/homologacionprotec.crt',
    'clave_privada' => dirname(__FILE__).'/cert/keyprotecprivada13042018.key',
    //'entorno' => FEEntorno::Produccion,
    //'certificado' => dirname(__FILE__).'/cert/tech_16c721525955b90d.crt',
    //'clave_privada' => dirname(__FILE__).'/cert/privada.key',
    'punto_de_venta' => 6,
    'cuit_emisor' =>  20295831848 ,
    //'tipo_comprobante' => FEComprobante::FacturaB,
    'debug_wsaa' => true,
    'debug_wsfe' => true
];
}

if ($organizacion == '' ){
    return;

  }



$opciones['organizacion'] = $organizacion;
$opciones['tipo_comprobante'] = $tipo_factura;

//var_dump($opciones);
/*
if (!isset($_GET['dni'])) {
    echo "No se ha especificado el parámetro cuit en la URL";
    exit(1);
}
*/
// Creamos un Bill (o lote) nuevo con las opciones de configuración.
$bill = new Bill($opciones);

// Cargamos los datos de las facutras.
///$invoices = [];

// Una factura A por $1000,00. Como el CUIT no es un Responsable Inscripto
// devuelve una observación, pero autoriza igual.
//$invoices[] = ['cuit_dni' => $_GET['dni'],
//               'doc_tipo' => FEDocumento::DNI,
//              'importe_total' => 1000.0];

// Factura A por $2000,00 por Servicios. Por lo tanto lleva el 10,5% de IVA
/****test factura */

/*$invoices[] = ['cuit_dni' => $_GET['cuit'],
               'doc_tipo' => FEDocumento::CUIT,
               'importe_total' => 2000.0,
               'tipo_iva' => FEIVA::Iva10,
               'concepto' => FEConcepto::Servicios];
*/
// Agregamos las facturas al lote

foreach ($invoices as $i) {
    $bill->agregarComprobante(new Invoice($i));
}

$invo = PruebaAfip::autorizarImprimirComprobantes($bill);

// Creamos otro lote para notas de crédito A. Usamos las mismas opciones
// cambiando el tipo de comprobante.
/**********nota de credito************/
/*$opciones['tipo_comprobante'] = FEComprobante::NotaCreditoA;
$bill = new Bill($opciones);

// Nota de crédito A (para factura A) por $1000,00. Cargamos los datos de la
// factura A como comprobante asociado.
$notaCredito = new Invoice(
    ['cuit_dni' => $_GET['cuit'],
     'doc_tipo' => FEDocumento::CUIT,
     'importe_total' => 1000.0]
);

// Nota de crédito para Factura A Nº 2183, del punto de venta 4
$notaCredito->comprobanteAsociado(2183, FEComprobante::FacturaA, 4);
$bill->agregarComprobante($notaCredito);

PruebaAfip::autorizarImprimirComprobantes($bill);*/
return $invo;
}


function nota_credito($invoices, $tipo_nota, $comprobanteAsoc, $tipo_comprobante_para_nota){
// Configuración general.
//
// El entorno sirve para determinar la URL del servicio.
//
// El tipo de comprobante se debe especificar para todo el lote. Si hay que
// autorizar dos tipos de comprobantes distintos (por ej. una Factura A y una
// Factura B) éstos deben ir en lotes diferentes.
//
// Las opciones de debug son útiles tenerlas activadas siempre (incluso en
// producción) por si Ocurrióe algún problema, que quede registro en los logs.
//


$opciones = [
    'entorno' => FEEntorno::Testing,
    'certificado' => dirname(__FILE__).'/cert/homologacion.crt',
    'clave_privada' => dirname(__FILE__).'/cert/pk.key',
  // 'entorno' => FEEntorno::Produccion,
   // 'certificado' => dirname(__FILE__).'/cert/tech_16c721525955b90d.crt',
   // 'clave_privada' => dirname(__FILE__).'/cert/privada.key',
    'punto_de_venta' => 3,
    'cuit_emisor' =>  30711188165,
    //'tipo_comprobante' => FEComprobante::FacturaB,
    'debug_wsaa' => true,
    'debug_wsfe' => true
];

$opciones['tipo_comprobante'] = $tipo_nota;


$bill = new Bill($opciones);

// Creamos otro lote para notas de crédito A. Usamos las mismas opciones
// cambiando el tipo de comprobante.
/**********nota de credito************/
//$opciones['tipo_comprobante'] = FEComprobante::NotaCreditoA;
//$bill = new Bill($opciones);

// Nota de crédito A (para factura A) por $1000,00. Cargamos los datos de la
// factura A como comprobante asociado.
/*$notaCredito = new Invoice(
    ['cuit_dni' => '12345678',
     'doc_tipo' => FEDocumento::CUIT,
     'importe_total' => 1000.0]
);*/

foreach ($invoices as $i) {

    $notaC = new Invoice($i);
    $notaC->comprobanteAsociado($comprobanteAsoc,$tipo_comprobante_para_nota ,$opciones['punto_de_venta'] );

   // $bill->agregarComprobante(new Invoice($i));
    $bill->agregarComprobante($notaC);
}

// Nota de crédito para Factura A Nº 2183, del punto de venta 4
//$notaCredito->comprobanteAsociado(2183, FEComprobante::FacturaA, 4);


$invo = PruebaAfip::autorizarImprimirComprobantes($bill);
return $invo;
}


