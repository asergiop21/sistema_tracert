<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PHP version 5
 *
 * @category AFIP
 * @package  WSFEv1
 * @author  Sergio Palacios <asergiop21@gmail.com>
 * @license  GPL v3.0+
 * 
 **/

require_once 'constants.php';

/**
 * Clase que representa un comprobante que se va a autorizar
 **/
class Invoice
{
    // Valores requeridos
    private $_cuitDni;
    private $_docTipo;
    private $_importeTotal;

    // Valores por defecto, pero requeridos
    private $_concepto = FEConcepto::Servicios;
    private $_tipoIva = FEIVA::Iva21;
    private $_fechaEmision;

    // Valores nulos por defecto a menos que se establezcan en el constructor
    private $_fechaServDesde;
    private $_fechaServHasta;
    private $_fechaVtoPago;

    // Calculado
    private $_importeIva;
    private $_importeNeto;

    // Valores que se establecen mediante una llamada explícita
    private $_compAsoc;

    // Se establece después de obtener el último número y antes de autorizar
    public $numero;

    // Valores de respuesta del WSFE
    public $observacion;
    public $obsCode;
    public $cae;
    public $caeFechaVto;
    public $resultado;

    public function __construct($datos)
    {
        $this->_cuitDni = $datos['cuit_dni'];
        $this->_docTipo = $datos['doc_tipo'];
        $this->_importeTotal = $datos['importe_total'];

        $this->_fechaEmision = date('Ymd');

        if (array_key_exists('tipo_iva', $datos))
            $this->_tipoIva = $datos['tipo_iva'];
        /*if (array_key_exists('tipo_iva', $datos))
            $this->_tipoIva = $datos['tipo_iva'];*/

        if (array_key_exists('fecha_emision', $datos))
            $this->_fechaEmision = $datos['fecha_emision'];

        if (array_key_exists('concepto', $datos))
            $this->_concepto = $datos['concepto'];

        if ($this->_concepto != FEConcepto::Productos) {
            if (array_key_exists('fecha_serv_desde', $datos)) {
                $this->_fechaServDesde = $datos['fecha_serv_desde'];
            } else {
                $this->_fechaServDesde = date('Ymd');
            }

            if (array_key_exists('fecha_serv_hasta', $datos)) {
                $this->fechaServHasta = $datos['fecha_serv_hasta'];
            } else {
                $this->_fechaServHasta = date('Ymd');
            }

            if (array_key_exists('fecha_vto_pago', $datos)) {
                $this->_fechaVtoPago = $datos['fecha_vto_pago'];
            } else {
                $this->_fechaVtoPago = date('Ymd');
            }
        }

        $this->_calcularIvaYNeto();
        $this->_compAsoc = [];
    }

    /**
     * Agrega un comprobante asociado a éste. Se utiliza para notas de
     * crédito o débito que 'anulan' facturas.
     *
     * @param $numero Número del comprobante que se va a anular
     * @param $tipo Tipo de comprobante que se va a anular
     * @param $puntoDeVenta Punto de venta donde se emitió el comprobante
     **/
    public function comprobanteAsociado($numero, $tipo, $puntoDeVenta)
    {
        $this->_compAsoc[] = ['Tipo' => $tipo,
                              'PtoVta' => $puntoDeVenta,
                              'Nro' => $numero];
    }

    /**
     * Devuelve un array asociativo con los campos en formato esperado por el
     * WSFE.
     *
     * @param $numero Número asignado al comprobante. (Ultimo autorizado + 1).
     **/
    public function getWSRequestFormat()
    {
        $invoice = ['Concepto' => $this->_concepto,
                    'DocTipo' => $this->_docTipo,
                    'DocNro' => $this->_cuitDni,
                    'CbteFch' => $this->_fechaEmision,
                    'ImpNeto' => $this->_importeNeto,
                    'ImpTotConc' => 0,
                    'ImpIVA' => $this->_importeIva,
                    'ImpTrib' => 0,
                    'ImpOpEx' => 0,
                    'ImpTotal' => $this->_importeTotal,
                    'FchServDesde' => $this->_fechaServDesde,
                    'FchServHasta' => $this->_fechaServHasta,
                    'FchVtoPago' => $this->_fechaVtoPago,
                    'MonId' => 'PES',
                    'MonCotiz' => 1,
                    'Iva' =>
                        ['AlicIva' =>
                            ['Id' => $this->_tipoIva,
                             'BaseImp' => $this->_importeNeto,
                             'Importe' => $this->_importeIva]
                         ],
                    'CbteDesde' => $this->numero,
                    'CbteHasta' => $this->numero];

        // Creamos un campo CbtesAsoc solamente cuando hay alguno
        if (count($this->_compAsoc) > 0) {
            $invoice['CbtesAsoc'] = ['CbteAsoc' => []];

            foreach ($this->_compAsoc as $comp) {
                $invoice['CbtesAsoc']['CbteAsoc'][] = $comp;
            }
        }

        return $invoice;
    }

    /**
     * Calcular el importe neto y el monto de IVA a partir del importe total y
     * del porcentaje de IVA.
     **/
    private function _calcularIvaYNeto()
    {
        $m = FEIVA::IvaMultiplicador($this->_tipoIva) + 1;
        $this->_importeNeto = round($this->_importeTotal / $m, 2);
        $this->_importeIva = $this->_importeTotal - $this->_importeNeto;
    }
}
