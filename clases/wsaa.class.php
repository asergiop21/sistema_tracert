<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

require_once 'constants.php';

class WSAA
{

    

    const TOKEN_REQUEST_FILE_TECH = FEPathsTech::TokenRequestPath;
    const TOKEN_TMP_FILE_TECH = FEPathsTech::TokenTempFile;
    const TOKEN_REQUEST_FILE_PROTEC = FEPathsProtec::TokenRequestPath;
    const TOKEN_TMP_FILE_PROTEC = FEPathsProtec::TokenTempFile;

    const SERVICE = 'wsfe';

    /**
    * Cliente SOAP
    */
    private $_client;

    private $_certificado;
    private $_clavePrivada;
    private $_passphrase;
    private $_url;
    private $_tokenSignXml;
    private $_debug;
    private $_organizacion;

    /*+
     * Constructor
     **/
    public function __construct($opciones)
    {

        $this->_certificado = $opciones['certificado'];
        $this->_clavePrivada = $opciones['clave_privada'];
        $this->_url = FEEntorno::WSURL($opciones['entorno'])['WSAA'];
        $this->_debug = $opciones['debug_wsaa'];
        $this->_organizacion = $opciones['organizacion'];

        if (array_key_exists('passphrase', $opciones)) {
            $this->_passphrase = $opciones['passphrase'];
        } else {
            $this->_passphrase = '';
        }

        // validar archivos necesarios
        if (!file_exists($this->_certificado))
            throw new Exception('Falta archivo de certificado');

        if (!file_exists($this->_clavePrivada))
            throw new Exception('Falta archivo de clave privada');

        $this->_client = new SoapClient(
            $this->_url,
            ['soap_version'   => SOAP_1_2,
             'trace'          => 1,
             'exceptions'     => 0]
        );
    }

    /*
    * Función principal que llama a las demas para generar el archivo que
    * contiene el token y sign.
    */
    public function generateTokenFile()
    {
        $this->createTRA();
        $TA = $this->callWSAA($this->signTRA());
        $organizacion = $this->_organizacion;
        if ($organizacion == 'techtron'){
        if (!file_put_contents(FEPathsTech::TokenSignPath, $TA))
            throw new Exception("Error al generar al archivo de token");
        }else{
            if (!file_put_contents(FEPathsProtec::TokenSignPath, $TA))
            throw new Exception("Error al generar al archivo de token");
        }        

        $this->_tokenSignXml = $this->xml2Array($TA);

        return true;
    }

    /**
     * Obtener la fecha de expiracion del token. Si no existe el archivo
     * devuelve false.
     **/
    public function getExpiration()
    {
        // si no esta en memoria abrirlo
        $organizacion = $this->_organizacion;
        if ($organizacion == 'techtron'){

        if (empty($this->_tokenSignXml)) {
            $tokenFile = file(FEPathsTech::TokenSignPath, FILE_IGNORE_NEW_LINES);

            if ($tokenFile) {

                $tokenXML = '';
                for ($i = 0; $i < sizeof($tokenFile); $i++)
                    $tokenXML .= $tokenFile[$i];

                $this->_tokenSignXml = $this->xml2Array($tokenXML);
                $r = $this->_tokenSignXml['header']['expirationTime'];
            } else {
                $r = false;
            }
        } else {
            $r = $this->_tokenSignXml['header']['expirationTime'];
        }
    }else{

        if (empty($this->_tokenSignXml)) {
            $tokenFile = file(FEPathsProtec::TokenSignPath, FILE_IGNORE_NEW_LINES);

            if ($tokenFile) {

                $tokenXML = '';
                for ($i = 0; $i < sizeof($tokenFile); $i++)
                    $tokenXML .= $tokenFile[$i];

                $this->_tokenSignXml = $this->xml2Array($tokenXML);
                $r = $this->_tokenSignXml['header']['expirationTime'];
            } else {
                $r = false;
            }
        } else {
            $r = $this->_tokenSignXml['header']['expirationTime'];
        }
    }   


        return $r;
    }

    /**
     * Crea un archivo XML con el TRA. Esta es la solicitud que luego se firma
     * y se envía al WSAA.
     **/
    private function createTRA()
    {

        $TRA = new SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?>' .
            '<loginTicketRequest version="1.0">'.
            '</loginTicketRequest>'
        );

        $TRA->addChild('header');
        $TRA->header->addChild('uniqueId', date('U'));
        $TRA->header->addChild('generationTime', date('c', date('U') - 60));
        $TRA->header->addChild('expirationTime', date('c', date('U') + 60));
        $TRA->addChild('service', self::SERVICE);
        $organizacion = $this->_organizacion;
        if ($organizacion == 'techtron'){
        $TRA->asXML(self::TOKEN_REQUEST_FILE_TECH);
        }else{
            $TRA->asXML(self::TOKEN_REQUEST_FILE_PROTEC);
        }
    }

    /**
     * This functions makes the PKCS#7 signature using TRA as input file, CERT
     * and PRIVATEKEY to sign. Generates an intermediate file and finally trims
     * the MIME heading leaving the final CMS required by WSAA.
     */
    private function signTRA()
    {
                $organizacion = $this->_organizacion;
        if ($organizacion == 'techtron'){
        $STATUS = openssl_pkcs7_sign(
            self::TOKEN_REQUEST_FILE_TECH,
            self::TOKEN_TMP_FILE_TECH,
            "file://" . $this->_certificado,
            ["file://" . $this->_clavePrivada, $this->_passphrase],
            [],
            !PKCS7_DETACHED
        );

        if (!$STATUS)
            throw new Exception("ERROR generating PKCS#7 signature");

        $inf = fopen(self::TOKEN_TMP_FILE_TECH, "r");
        $i = 0;
        $CMS = "";
        while (!feof($inf)) {
            $buffer = fgets($inf);
            if ( $i++ >= 4 ) $CMS .= $buffer;
        }

        fclose($inf);
        unlink(self::TOKEN_TMP_FILE_TECH);
}else{
   echo $this->_clavePrivada;
$STATUS = openssl_pkcs7_sign(
            self::TOKEN_REQUEST_FILE_PROTEC,
            self::TOKEN_TMP_FILE_PROTEC,
            "file://" . $this->_certificado,
            ["file://" . $this->_clavePrivada, $this->_passphrase],
            [],
            !PKCS7_DETACHED
        );
print_r($STATUS);

        if (!$STATUS)
            throw new Exception("ERROR generating PKCS#7 signature");

        $inf = fopen(self::TOKEN_TMP_FILE_PROTEC, "r");
        $i = 0;
        $CMS = "";
        while (!feof($inf)) {
            $buffer = fgets($inf);
            if ( $i++ >= 4 ) $CMS .= $buffer;
        }

        fclose($inf);
        unlink(self::TOKEN_TMP_FILE_PROTEC);

}

        return $CMS;
    }

    /**
     * Conecta con el web service y obtiene el token y sign
     **/
    private function callWSAA($cms)
    {
        $results = $this->_client->loginCms(['in0' => $cms]);

        // para logueo
        if ($this->_debug) {
            error_log($this->_client->__getLastRequest());
            error_log($this->_client->__getLastResponse());
        }

        if (is_soap_fault($results))
            throw new Exception(
                "SOAP Fault revisar: " .
                $results->faultcode . ': ' .
                $results->faultstring
            );

        return $results->loginCmsReturn;
    }

    /*
    * Convertir un XML a Array
    */
    private function xml2array($xml)
    {
        $json = json_encode(simplexml_load_string($xml));
        return json_decode($json, TRUE);
    }
}
