<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PHP version 5
 *
 * @category AFIP
 * @package  WSFEv1
 * @author   Sergio Palacios <asergiop21@gmail.com>
 * @license  GPL v3.0+
 * @link     
 **/

require_once 'invoice.class.php';
require_once 'wsaa.class.php';
require_once 'wsfe.class.php';

/**
 * Clase que encapsula las llamadas SOAP al servicio web de AFIP.
 * Un Bill se compone de una o más Invoices.
 **/
class Bill
{
    private $_wsaa;
    private $_wsfe;

    private $_invoices;

    private $_puntoDeVenta;
    private $_tipoCbte;

    // A = Todos autorizados,
    // P = Autorización parcial (solo algunos del lote)
    // R = Todos rechazados.
    public $resultado;

    public function __construct($opciones)
    {
        $this->_wsaa = new WSAA($opciones);
        $this->_wsfe = new WSFE($opciones);

        $this->_invoices = [];

        $this->_puntoDeVenta = $opciones['punto_de_venta'];
        $this->_tipoCbte = $opciones['tipo_comprobante'];

        // Establecer la zona horaria de AFIP para trabajar con fechas


        if (date_default_timezone_get() == 'UTC') {
            date_default_timezone_set('America/Buenos_Aires');
        }
    }

    /**
     * Agrega un comprobante al lote para ser autorizado.
     *
     * @param $invoice Instancia de Invoice con los datos del comprobante
     **/
    public function agregarComprobante($invoice)
    {
        $this->_invoices[] = $invoice;
    }

    /**
     * Retorna un arreglo con los comprobantes de este lote.
     **/
    public function comprobantes()
    {
        return $this->_invoices;
    }

    /**
     * Función principal. Luego de agregar los comprobantes hace las llamadas
     * al WSAA para autenticarse, al WSFEv1 para obtener el número de último
     * comprobante y nuevamente para autorizar el lote de comprobantes.
     **/
    public function autorizar()
    {
        

        $this->loginWsaa();

        $nuevoNumero = $this->_wsfe->FECompUltimoAutorizado() + 1;

        $result =
            $this->_wsfe->FECAESolicitar($this->buildRequest($nuevoNumero));

        $this->setupInvoices($result);
    }

    /**
     * Comprueba si ya hay un archivo de token y si todavía no expiró. De lo
     * contrario llama al WSAA para generar uno nuevo.
     **/
    private function loginWsaa()
    {
        

        if ($this->_wsaa->getExpiration() < date('c')) {
            $this->_wsaa->generateTokenFile();
        }

        $this->_wsfe->loadTokenFile();
    }

    /**
     * Construye el parte del request que corresponde al encabezado y cuerpo
     * con los comprobantes del lote.
     *
     * @param $nuevoNumero Número de comprobante que se va a enviar al WSFE.
     **/
    private function buildRequest($nuevoNumero)
    {
        $request = ['FeCabReq' =>
                        ['CantReg' => count($this->_invoices),
                         'PtoVta' => $this->_puntoDeVenta,
                         'CbteTipo' => $this->_tipoCbte],
                    'FeDetReq' => ['FECAEDetRequest' => []]];

        foreach ($this->_invoices as $invoice) {
            $invoice->numero = $nuevoNumero++;

            $request['FeDetReq']['FECAEDetRequest'][] =
                $invoice->getWSRequestFormat();
        }
        return $request;
    }

    /**
     * Lee los resultados de la autorización y los carga en el Invoice
     * correspondiente, buscando por número.
     *
     * @param $result Array con resultados de la autorización.
     **/
    private function setupInvoices($result)
    {
        $this->resultado = $result->FECAESolicitarResult->FeCabResp->Resultado;
        $lote = $result->FECAESolicitarResult->FeDetResp->FECAEDetResponse;

        foreach ($lote as $afip) {
            $invoice = $this->findInvoiceByNumber($afip->CbteDesde);

            $invoice->cae = $afip->CAE;
            $invoice->caeFechaVto =
                DateTime::createFromFormat('Ymd', $afip->CAEFchVto);
            $invoice->resultado = $afip->Resultado;

            // Verificamos si devolvió alguna observación. Por lo general es
            // una sola.
            if (property_exists($afip, 'Observaciones')) {
                $invoice->observacion = $afip->Observaciones->Obs[0]->Msg;
                $invoice->obsCode = $afip->Observaciones->Obs[0]->Code;
            }
        }
    }

    /**
     * Busca un comprobante por número en el arreglo de invoices y lo retorna.
     *
     * @param $num Número a buscar.
     **/
    private function findInvoiceByNumber($num)
    {
        foreach ($this->_invoices as $i) {
            if ($i->numero == $num) {
                return $i;
            }
        }
    }
}
