<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

require_once 'constants.php';

/**
 * Cuando el servicio de la AFIP devuelve un error se lanza esta excepción
 * para distinguirla de otro tipo de errores dentro de un try/catch.
 **/
class AFIPErrorException extends Exception
{
}

/**
 * Clase que encapsula las llamadas SOAP al servicio web de AFIP WSFEv1
 **/
class WSFE
{
    /**
     * Cliente SOAP
     */
    private $_client;
    private $_cuit;
    private $_puntoDeVenta;
    private $_tipoCbte;
    private $_organizacion;

    private $_debug;

    /*
     * objeto que va a contener el xml de TA
     */
    private $_tokenSignXml;

    /**
     * Constructor
     **/
    public function __construct($opciones)
    {
        $this->_cuit = $opciones['cuit_emisor'];
        $this->_puntoDeVenta = $opciones['punto_de_venta'];
        $this->_tipoCbte = $opciones['tipo_comprobante'];
        $this->_debug = $opciones['debug_wsfe'];
        $this->_organizacion = $opciones['organizacion'];

        $url = FEEntorno::WSURL($opciones['entorno'])['WSFE'];

        $this->_client = new SoapClient(
            $url,
            ['soap_version' => SOAP_1_2,
             'exceptions' => 0,
             'trace' => 1,
             'features' => SOAP_SINGLE_ELEMENT_ARRAYS]
        );
    }

    /**
     * Chequea los errores en la operacion, si encuentra algun error falta
     * lanza una exepcion. Si encuentra un error no fatal, loguea lo que pasó
     * en $this->error
     **/
    private function _checkErrors($results, $method)
    {
        if ($this->_debug) {
            error_log($this->_client->__getLastRequest());
            error_log($this->_client->__getLastResponse());
        }

        if (is_soap_fault($results)) {
            throw new Exception(
                'WSFE class. FaultString: ' .
                $results->faultcode . ' ' .
                $results->faultstring
            );
        }

        $XXX = $method . 'Result';
        if (property_exists($results->$XXX, 'Errors') &&
            $results->$XXX->Errors->Err[0]->Code != 0) {
            throw new AFIPErrorException(
                "Method=$method errcode=" .
                $results->$XXX->Errors->Err[0]->Code .
                " errmsg=" . $results->$XXX->Errors->Err[0]->Msg
            );
        }

        return false;
    }

    /**
     * Lee el archivo de token xml. Si hay algún problema lanza una excepción.
     **/
    public function loadTokenFile()
    { 
        $organizacion = $this->_organizacion;
        if ($organizacion == 'techtron'){
        $this->_tokenSignXml = simplexml_load_file(FEPathsTech::TokenSignPath);

        if (!$this->_tokenSignXml) {
            throw new Exception('No se pudo abrir el archivo de token');
        }
    }else{
        $this->_tokenSignXml = simplexml_load_file(FEPathsProtec::TokenSignPath);

        if (!$this->_tokenSignXml) {
            throw new Exception('No se pudo abrir el archivo de token');
        }
    }
}
    /**
     * Retorna el ultimo número autorizado.
     **/
    public function FECompUltimoAutorizado()
    {
        $results = $this->_client->FECompUltimoAutorizado(
            ['Auth' =>
                ['Token' => $this->_tokenSignXml->credentials->token,
                 'Sign' => $this->_tokenSignXml->credentials->sign,
                 'Cuit' => $this->_cuit],
             'PtoVta' => $this->_puntoDeVenta,
             'CbteTipo' => $this->_tipoCbte]
        );

        $this->_checkErrors($results, 'FECompUltimoAutorizado');

        return $results->FECompUltimoAutorizadoResult->CbteNro;
    }

    /**
     * Retorna el ultimo comprobante autorizado para el tipo de comprobante
     * /cuit / punto de venta ingresado.
     **/
    public function recuperaLastCMP()
    {
        $results = $this->_client->FERecuperaLastCMPRequest(
            ['argAuth' =>
                ['Token' => $this->_tokenSignXml->credentials->token,
                 'Sign' => $this->_tokenSignXml->credentials->sign,
                 'cuit' => $this->_cuit],
                 'argTCMP' =>
                    ['PtoVta' => $this->_puntoDeVenta,
                     'TipoCbte' => $this->_tipoCbte]]
        );
        $e = $this->_checkErrors($results, 'FERecuperaLastCMPRequest');

        return $e ? false : $results->FERecuperaLastCMPRequestResult->cbte_nro;
    }


    /**
     * Solicitud CAE y fecha de vencimiento
     **/
    public function FECAESolicitar($partialRequest)
    {
        $params = [
            'Auth' =>
                ['Token' => $this->_tokenSignXml->credentials->token,
                 'Sign' => $this->_tokenSignXml->credentials->sign,
                 'Cuit' => $this->_cuit],
            'FeCAEReq' => $partialRequest];

        $result = $this->_client->FECAESolicitar($params);
        $this->_checkErrors($result, 'FECAESolicitar');
        return $result;
    }
}
