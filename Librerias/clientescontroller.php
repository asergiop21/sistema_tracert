<?php

	require_once('conn.php');

	
	function clientes_habilitados(){
		$db = Conec_con_pass();		
		$clientes_ids = pg_exec($db, "Select * from clientes where elim_clie = 'f' order by apellido asc");
		return $clientes_ids;
		
	}

	function clientes_habilitados_instalaciones_plan(){
		$db = Conec_con_pass();		
		$clientes_ids = pg_exec($db, "Select * from clientes 										
									inner join instalaciones on clientes.idclientes = instalaciones.idclientes
									inner join planes on instalaciones.plan_id = planes.id
									where clientes.elim_clie = 'f' order by apellido asc");
		return $clientes_ids;

	}

	function cierre_factura(){


		$db = Conec_con_pass();		
		$factura_pendiente = factura_pendiente();
		$cuota_pendiente =  cuotas();

		$cierre_factura = pg_exec($db, "select DISTINCT(factura_id), SUM(importe) as importe, cliente_id from detalle_factura 
										where cerrado = 'false' group by factura_id, cliente_id order by cliente_id asc");
		while ($row = pg_fetch_object($cierre_factura)) {

			$factura_id = $row->factura_id;
			$cliente_id = $row->cliente_id;
			
			$importe = $row->importe;
			$detalle = "Factura";
			

			$insert = pg_exec($db, "Insert into facturas(id,  cliente_id, importe, detalle) 
									values('$factura_id', '$cliente_id', '$importe', '$detalle' )");			
			if (pg_affected_rows($insert) > 0);
			{
				$update = pg_exec($db,"update detalle_factura set cerrado = 'true' where factura_id = '$factura_id'");
			}

		}

		crear_factura();		
	}

	function factura_pendiente(){
			$db = Conec_con_pass();	
			$factura_pendiente = pg_exec($db, "Select * from facturas where estado = 'false' ");


			if (pg_num_rows($factura_pendiente) > 0)
			{
				while ($row = pg_fetch_object($factura_pendiente)){
					$cliente_id = $row->cliente_id;
					$factura_actual_id = factura_actual($cliente_id);
					$detalle = "Saldo Anterior";
					$importe = $row->importe;
					$factura_id = $row->id;
				
					$insert_factura_pendiente = pg_exec($db, "Insert into detalle_factura(id, detalle, importe, factura_id, cliente_id) 
									values(DEFAULT, '$detalle', '$importe', '$factura_actual_id', '$cliente_id' ) ");
			
			if (pg_affected_rows($insert_factura_pendiente) > 0)
			{
				$update_factura_pendiente = pg_exec($db, "Update facturas set estado = 'true' where id = '$factura_id'");
				pg_affected_rows($update_factura_pendiente);
			}}}
	}


	function cuotas(){

		$db = Conec_con_pass();		
		
		$cuotas = pg_exec($db, "Select * from cuotas where cerrado = 'false'");

		if (pg_num_rows($cuotas) > 0)
		{
			
			$row_cuotas = pg_fetch_object($cuotas);
				$cliente_id = $row_cuotas->cliente_id;
				$factura_actual_id = factura_actual($cliente_id);
				$cantidad_cuotas = $row_cuotas->cantidad;
				$cuotas_pagadas = $row_cuotas->cuotas_pagadas + 1 ;
				$importe = $row_cuotas->importe;
				$detalle = $row_cuotas->detalle;
				$cuota_id = $row_cuotas->id;
				
			$insert_cuota = pg_exec($db, "Insert into detalle_factura(id, detalle, importe, factura_id, cliente_id) 
									values(DEFAULT, '$detalle', '$importe', '$factura_actual_id', '$cliente_id' ) ");

			if (pg_affected_rows($insert_cuota) > 0)
			{
				if ($cantidad_cuotas == $cuotas_pagadas ){
						$cerrado = 'true';
					}else{
						$cerrado = 'false';
					}

				$update_cuota = pg_exec($db, "Update cuotas set cuotas_pagadas = '$cuotas_pagadas', cerrado = '$cerrado' where id = '$cuota_id'");

			}		
		}
		
		return;	
	}




	function crear_factura(){
	
		$db = Conec_con_pass();		
		$crear_factura_clientes = clientes_habilitados_instalaciones_plan();
		
		while ($clientes_habilitados = pg_fetch_object($crear_factura_clientes)) {

			$factura_id = pg_exec($db,"select nextval('facturas_id_seq')");
			$factura_id = pg_fetch_result($factura_id,0);
			$cliente_id = $clientes_habilitados->idclientes;
			$nombre_plan = $clientes_habilitados->nombre;
		 	$importe_plan = $clientes_habilitados->importe;
			//$nombre_importe_cuota = cuotas($cliente_id, $factura_id);


			$insert_cuota = pg_exec($db, "Insert into detalle_factura(id, detalle, importe, factura_id, cliente_id) 
									values(DEFAULT, '$nombre_plan', '$importe_plan', '$factura_id', '$cliente_id') ");

		}
	}

	function factura_actual($cliente_id)
	{
		$db = Conec_con_pass();		
		$factura_actual_id = pg_exec($db, "select factura_id from detalle_factura where cliente_id = '$cliente_id' and cerrado = 'false' limit 1");
		$factura_actual_id = pg_fetch_result($factura_actual_id, 0);
		return $factura_actual_id;
	}


?>
