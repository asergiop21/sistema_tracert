<?php

require('../Librerias/conn.php');
require('../Librerias/pdf.php');
require('../Librerias/code128/code128.php');
require_once ('../Controllers/PagosController.php');
$db =Conec_con_pass();

$dni = $_REQUEST['dni'];
$id = $_REQUEST['id'];
$comentario = $_REQUEST['texto'];
$num_mes = fecha_meses();
$ano = $_REQUEST['ano'];
$im = $_REQUEST['im'];

$pdf = new PDF_Code128();
$pdf->AddPage('P', 'A4');
$pdf->SetFont('Arial','B',16);

if ($dni != "")
{
	$val = $dni;
}else
{
 $val = $id;
}

$val = split(',',$id);
$total_rows = count($val);
$cant = 0;

	$x = 20;
	$y = 60;
	$X = 75;
	$Y = 20;
	$rx = 17;
	$ry = 103;

	$sql_gral = "select clientes.*, instalaciones.*, planes.nombre as nombre_plan, planes.importe as importe, localidad.* from clientes 
									inner join instalaciones on clientes.idclientes = instalaciones.idclientes
									inner join localidad on clientes.localid = localidad.idlocalidad
									inner join planes on planes.id = instalaciones.plan_id	where 1 = 1";
	
	if ($dni != ""){
		$sql_gral .="and dni = '$dni'";
	}

	if ($id !=""){
		$sql_gral .="and clientes.idclientes = '$id'";
	}

	$debe = 0;
	$haber = 0;
	$deuda = 0;

	$sql_mm = pg_exec($db, $sql_gral);

	while ($cli = pg_fetch_object($sql_mm)) {
		$cant = $cant + 1;

		$fe_meses = $num_mes;
		if ($fe_meses == '12')
		{
			$fe_meses2 =  '01' ;
			$ano2 = $ano + 1;
		}
		else
		{
			$fe_meses2 = $num_mes + 1 ;
			$ano2 = $ano;
		}
		$imp_ver = $im;
		$ndias = 1;
		$fe_diez = sumarmeses_nuevo(fecha_10(),$ndias);

        $pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"APELLIDO y NOMBRE:")	;
		$x = $x + 35;

		$nomyape = $cli->apellido.", ". $cli->nombre;
		$texto = substr($nomyape,0,35);

		$pdf->SetFont('Arial','',14);
		$pdf->Text($x,$y,$texto);
		//$y= $y + 10;

		$fec_ser = fecha_servidor_total();
		$x = $x + 117;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,"$fec_ser");
		$y= $y + 10;


		$x= $x - 153;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"DOMICILIO: ");
		$x = $x + 35;
		$pdf->SetFont('Arial','',14);
		$pdf->Text($x,$y,$cli->domicilio);
		$y= $y + 10;

		$x= $x - 35;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"ENTRE: ");
		$x = $x + 35;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,$cli->entre);
		$y= $y + 10;

		$x= $x - 35;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"BARRIO: ");
		$x = $x + 20;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->barrio);
		//$y= $y + 5;

		$x = $x + 70;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"LOCALIDAD: ")	;
		$x= $x + 18;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->num_loc);

		$y= $y + 5;

		$x = 20;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"DEPARTAMENTO: ")	;
		$x= $x + 25;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->dpto)	;


		$x = $x + 70;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"C.P.: ")	;
		$x= $x + 8;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->cp)	;
                $x= $x + 23;
                $pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y," CLIENTE.: ")	;
                $x= $x + 25;
		$pdf->SetFont('Arial','B',10);
		$pdf->Text($x,$y,$cli->idnum_tarjeta);

		$y= $y + 15;
		$x = 35;
		$pdf->Rect($rx , $ry,175,12);
		$ry = $ry + 35;

                $clie = $cli->idclientes;

		$sql1 = pg_exec($db,"select * from pagos where idclientes = '$clie'");
		while ($pagos = pg_fetch_object($sql1))
		{
			$debe = $debe + $pagos->importe_deuda;
			$haber = $haber + $pagos->importe_pagado;
			$deuda = $haber - $debe;
			if ($deuda > 0)
			{
				$deuda = 0;
			}
		}
                $deuda = 0 - $deuda ;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"Deuda a la fecha: $".$deuda.".00" );
		$x = $x + 60;
		$pdf->Text($x,$y,utf8_decode("Fecha de Emisión: ".$fe_em = fecha_servidor_total()));
		$x = $x + 50;
		//$pdf->Text($x,$y, "Abono: $".$cli->abono);
		$x = $x - 90;
		$y = $y + 20;

		$pdf->SetFont('Arial','',12);
		$pdf->Text($x,$y, "Sr. Cajero por favor respete las fechas de vencimiento");

		$x = $x - 30;
		$y = $y + 15;


                    $abono = $cli->importe;


                switch ($abono)
                {
                        case $abono <= '99';
                               $abo_int = $abono + 12;
                               break;
                         case $abono > '99' && $abono <='129':
                                $abo_int = $abono + 15;
                                break;
                            case  $abono > '129' && $abono <= '189':
                                $abo_int = $abono + 18;
                                break;
                            case $abono > '189' :
                                $abo_int = $abono + 25;

                }


		$pdf->Rect($rx, $ry,67,40);
		$rx = $rx + 107;
		$pdf->SetFont('Arial','UB',10);
		$pdf->Text($x,$y,"Vencimientos: ".meses($fe_meses)." ".$ano);
		$x = $x + 105;

		$pdf->Rect($rx , $ry,67,40);
		$rx = $rx - 100;
		$ry = $ry + 100;

		$pdf->Text($x,$y,"Vencimientos: ".meses($fe_meses2)." ".$ano2);
		$x = 25; // 20
		$y = $y + 10; //105
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,"Del 1/".$fe_meses. " al 10/".$fe_meses.": $".$cli->importe); //Abono mensual
		$x = $x + 110;
		$pdf->Text($x,$y,"Del 1/".$fe_meses2. " al 10/".$fe_meses2.": $".$cli->importe); //Abono mensual
		$x = 25; // 20
		$y = $y + 10; //115




		$pdf->Text($x,$y,"Del 11/".$fe_meses. " al 18/".$fe_meses.": $". $abo_int.".00");
		$x = $x + 110;
		$pdf->Text($x,$y,"Del 11/".$fe_meses2. " al 18/".$fe_meses2.": $". $abo_int.".00") ;
		$x = 25; // 20
		$y = $y + 10; //115

                                switch ($abono)
                {
                        	
                        case $abono <= '99';
                               $abo_int = $abo_int + 12;
                               break;
                         case $abono > '99' && $abono <='129':
                                $abo_int = $abo_int + 15;
                                break;
                            case  $abono > '129' && $abono <= '189':
                                $abo_int = $abo_int + 18;
                                break;
                            case $abono > '189' :
                                $abo_int = $abo_int + 25;
                }

 switch ($abo_nue)
                {
                        case $abo_nue <= '99';
                               $abo_int3 = $abo_int2 + 12;
                               break;
                         case $abo_nue > '99' && $abo_nue <='129':
                                $abo_int3 = $abo_int2 + 15;
                                break;
                            case  $abo_nue > '129' && $abo_nue <= '189':
                                $abo_int3 = $abo_int2 + 18;
                                break;
                            case $abo_nue > '189' :
                                $abo_int3 = $abo_int2 + 25;
                }

		$pdf->Text($x,$y,"19/".$fe_meses." al 23/".$fe_meses." $". $abo_int.".00");
		$x = $x + 110;
		$pdf->Text($x,$y,"19/".$fe_meses2." al 23/".$fe_meses." $". $abo_int.".00");


		$x = 35; // 20
		$y = $y + 15; //95
		$pdf->SetY(180);
		$pdf->SetX(20);
        $pdf->SetFont('Arial','',8);
		$pdf->Rect(17, 180, 175 ,50);
		$pdf->Multicell(170,6, utf8_decode($pdf->SetFont('Arial','B',8)."Puede utilizar este código de barras para regularizar deuda anterior (incluso fuera de las fechas de vencimiento)."),'0');                
		$pdf->SetY(185);
		$pdf->SetX(20);
		$pdf->Multicell(170,6, utf8_decode($pdf->SetFont('Arial','',8)."Mediante este cupón puede abonar su servicio en Rapipago o Pagofácil. Su deuda aparece en el recuadro superior del cupón. Recuerde abonar antes de los 30 días corridos del primer vencimiento para evitar la suspensión del servicio y cargos por reconexión. La baja del servicio ante la falta de pago del mismo no implica en el cliente el cese de su obligación de cancelar los documentos impagos. El tiempo durante el cual esté suspendido el servicio es responsabilidad del cliente y será facturado. Si por alguna razón no recibe nuestro cupón de pago, puede solicitarlo también telefónicamente antes del día 5 de cada mes, como así para solicitar servicios de mayores velocidades. Recomendamos evitar el pago posterior al mes del vencimiento ya que el mismo generará un gasto de gestión de cobranza y/o pago fuera de término."),'0');                

		$y = $y + 50;
		$x = 90;
		$tar = $cli->idnum_tarjeta;
		$digito = digito_verificador($tar);

		$pdf->Code128($x,$y,'152'.$tar.$digito);


        $pdf->SetY(5);
        $pdf->SetX(5);
        $pdf->Image('../image/encabezado.jpg', '5', '5', '200', '50');


        $pdf->SetY(230);
        $pdf->SetX(140);
        $pdf->Image('../image/pie.jpg', '5', '265', '200', '30');


		$fe_em = cambiaf_a_bd($fe_em);
		$pdf->Ln(10);
	}

   $pdf->Output();
?>