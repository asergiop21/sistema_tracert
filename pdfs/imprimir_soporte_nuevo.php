<?php
require_once ("../Librerias/conn.php");

require("../fpdf.php");
$db=Conec_con_pass();

$marc = $_REQUEST["aBorrar"];
$tec = $_REQUEST["tec"];
$val = split(',', $marc);


$pdf=new FPDF('P');
$pdf->AddPage();
$pdf->SetFont('Arial','B',8);

foreach ($val as $pa=>$c)
{
		$sql12 = pg_exec($db,"select * from ((clientes inner join reclamos on clientes.idclientes = reclamos.idclientes) inner join instalaciones  on instalaciones.idclientes = clientes.idclientes)inner join localidad on clientes.localid = localidad.idlocalidad  where reclamos.idreclamos = '$c'   order by idlocalidad asc");
		$total_rows = pg_num_rows($sql12);
		$sql_ins_rec = pg_exec($db, "update reclamos set rc_impre = 't', rc_tec_imp = '$tec', rc_fec_imp= '$fec_ser_db', rc_tim_imp = '$tim_ser', rc_imp_user= '$id_user_imp'   where idreclamos = '$c'");
		$sql_tec = pg_exec($db, "select * from usuario where idusuario = '$tec'");
		while ($row_insta = pg_fetch_object($sql12)) {

			$pdf->SetFont('Arial','B',14);
			$cant = $cant + 1;
			$pdf->SetY(10);
			$pdf->SetX(25);
			$pdf->MultiCell(175,10,utf8_decode("ORDEN SOPORTE TÉCNICO"),1,'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->SetY(25);
			$pdf->SetX(145);
			$pdf->MultiCell(55,5,"RECLAMO: ".cambiaf_a_normal($row_insta->fecha_recla),1,'');
			$pdf->SetFont('Arial','B',10);
			$pdf->SetY(30);
			$pdf->SetX(25);
			$pdf->MultiCell(175,5,"DATOS PERSONALES",1,'');
			$clie = $row_insta->idclientes;
			$clie_tarj = $row_insta->idnum_tarjeta;
      
			$pdf->SetFont('Arial','B',8);
			$pdf->SetY(20);
			$pdf->SetX(145);
			$pdf->MultiCell(55,5,utf8_decode("N°:".$clie_tarj."- Nº Reclamo: ".$row_insta->idreclamos) ,1,'');

			$nom = $row_insta->nombre;
			$ape = $row_insta->apellido;

			$pdf->Text(25,41,'CLIENTES:',1,0,'C');
			$pdf->Text(47,41,$row_insta->apellido.", ".$row_insta->nombre,1,0,'C');
			$pdf->Rect(45,37,155,5);

			$pdf->Text(25,48,'BARR. - DOM..:',1,0,'C');
			$pdf->Text(47,48,$row_insta->barrio." ".$row_insta->domicilio,1,0,'C');
			$pdf->Rect(45,44,155,5);

			$pdf->Text(25,55,'REFERENCIA:',1,0,'C');
			$pdf->Text(47,55,$row_insta->entre,1,0,'C');
			$pdf->Rect(45,51,155,5);


			$pdf->Text(25,62,'TELEFONO:',1,0,'C');
			$pdf->Text(47,62,$row_insta->tel_fijo." / ".$row_insta->celular ,1,0,'C');
			$pdf->Rect(45,58,50,5);

			$pdf->Text(100,62,'Localidad',1,0,'C');
			$pdf->Text(122,62,$row_insta->num_loc." - ".$row_insta->dpto,1,0,'C');
			$pdf->Rect(120,58,80,5);

			$pdf->SetY(65);
			$pdf->SetX(25);

			$pdf->MultiCell(175,5,"DATOS TECNICOS",1,'');
			$equipo = $row_insta->idequipo;
			$eq_wif = $row_insta->id_eq_wifi;

			$sql_eq = pg_exec($db, "Select * from equipos inner join modelos on equipos.eq_modelos = modelos.idmodelos  where idequipos = '$equipo' ");
			$sql_eq_wf = pg_exec($db, "Select * from equipos inner join modelos on equipos.eq_modelos = modelos.idmodelos where idequipos = '$eq_wif' ");

			$row_eq = pg_fetch_object($sql_eq);
			$row_eq_wf = pg_fetch_object($sql_eq_wf);
//-----------------------------------datos tecnicos

	        $pdf->Text(26,74,'IP PC:',1,0,'C');
	        $pdf->Text(42,74,$row_insta->ip_pc,1,0,'C');
	        $pdf->Rect(40,71,65,5);

	        $pdf->Text(117,74,'IP ANTENA:',1,0,'C');
	        $pdf->Text(136,74,$row_insta->ip_equipo,1,0,'C');
	        $pdf->Rect(135,71,65,5);

	        $pdf->Text(26,81,'ABONO:',1,0,'C');
	        $pdf->Text(42,81,$row_insta->abono,1,0,'C');
	        $pdf->Rect(40,78,65,5);

	        $pdf->Text(117,81,'VELOCIDAD:',1,0,'C');
	        $pdf->Text(136,81,$row_insta->velocidad,1,0,'C');
	        $pdf->Rect(135,78,65,5);

	        $pdf->SetY(85);
			$pdf->SetX(25);
	        $pdf->MultiCell(175,5,"EQUIPOS",1,'');

	        $pdf->Text(26,95,'TRANSMISOR:',1,0,'C');
	        $pdf->Text(49,95,$row_eq->modelos." -  mac: ".$row_eq->eq_mac,1,0,'C');
	        $pdf->Rect(48,91,152,5);

	        $pdf->Text(26,99,'WIFI:',1,0,'C');
	        $pdf->Text(49,99,"Wifi: ".$row_eq_wf->modelos." -  mac - n/s: ".$row_eq_wf->eq_mac,1,0,'C');
	        $pdf->Rect(48,96,152,4);

	        $pdf->Text(26,103,'SWITCH:',1,0,'C');
	        //$pdf->Text(49,103,'' ,1,0,'C');
	        $pdf->Rect(48,100,152,4);

			$pdf->Text(26,107,'OTROS:',1,0,'C');
	        //$pdf->Text(49,107,$row_eq->modelos." -  mac: ".$row_eq->eq_mac." -  Wifi: ".$row_eq_wf->modelos." -  mac - n/s: ".$row_eq_wf->eq_mac ,1,0,'C');
	        $pdf->Rect(48,104,152,4);

			$pdf->SetY(150);
			$pdf->SetX(25);
			$pdf->SetFont('Arial','B',10);
			$pdf->MultiCell(175,5,"HISTORIAL",1,'J');

			$sql_recl = pg_exec($db, "select * from reclamos where idclientes = '$clie' order by fecha_recla desc limit 4");
			$re_obs = "";
			$re_sol ="";
			$re_falla = "";
			$x = '25';
			$y = '155';

			while ($recl = pg_fetch_object($sql_recl))
			{
			$pdf->SetFont('Arial','',7);
			       $re_solucion = $recl->solucion;
			       if ($re_solucion != "")
			       {
			       		$x = 25;
			            $pdf->SetY($y);
			            $pdf->SetX($x);
			            $pdf->MultiCell(95,3,$recl->fecha_recla." - ".$recl->falla,1,'J');
			           	$x = 120;
			           	$pdf->SetY($y);
			            $pdf->SetX($x);
			            $pdf->MultiCell(80,3,"$recl->solucion",1,'C');
			            //$x = $x + 110;
			            $y = $y + 9;
				       }
			 else {
			             $re_falla = $re_falla."".$recl->falla;
			       }
			    }

			$pdf->SetFont('Arial','',10);
			$pdf->SetY(115);
			$pdf->SetX(25);
			$pdf->MultiCell(95,5,$re_falla,1,'J',0,8);

			//-----Recuadro de solucion tarea realizada
			$pdf->Rect(120,115,80,35);

			$pdf->SetFont('Arial','B',10);
			$pdf->SetY(110);
			$pdf->SetX(25);
			$pdf->MultiCell(95,5,"TIPO DE PROBLEMA",1,'J');

			$pdf->SetY(110);
			$pdf->SetX(120);
			$pdf->MultiCell(80,5,"TAREA REALIZADA",'1','J');


			//-----------Opciones
			$pdf->Rect(30,190,3,3);
			$pdf->Text(35,193,'Cambio POE',1,0,'C');

			$pdf->Rect(95,190,3,3);
			$pdf->Text(100,193,'Orientacion Antena',1,0,'C');

			$pdf->Rect(160,190,3,3);
			$pdf->Text(165,193,'Cambio IP',1,0,'C');

			$pdf->Rect(30,195,3,3);
			$pdf->Text(35,198,'Cambio Cable',1,0,'C');

			$pdf->Rect(95,195,3,3);
			$pdf->Text(100,198,'Reconfiguracion EQ.',1,0,'C');

			$pdf->Rect(160,195,3,3);
			$pdf->Text(165,198,'PC con virus',1,0,'C');

			$pdf->Rect(30,200,3,3);
			$pdf->Text(35,203,'Cambio RJ45',1,0,'C');

			$pdf->Rect(95,200,3,3);
			$pdf->Text(100,203,'Reconfiguracion WIF',1,0,'C');

			$pdf->Rect(160,200,3,3);
			$pdf->Text(165,203,utf8_decode('Cambio de Señal'),1,0,'C');



			$pdf->Rect(28,206,3,3);
			$pdf->SetY(205);
			$pdf->SetX(25);
			$pdf->MultiCell(90,5,"ES NECESARIO VOLVER CON:",'1','C');
			$pdf->Rect(25,210,90,20);

			$pdf->Rect(120,206,3,3);
			$pdf->SetY(205);
			$pdf->SetX(115);
			$pdf->MultiCell(85,5,"CON CARGO:",'1','C');
			$pdf->Rect(115,210,85,20);


$pdf->SetFont('Arial','B',8);
$pdf->SetY(230);
$pdf->SetX(28);
$pdf->MultiCell(172,6,utf8_decode("Sr. Cliente: Recuerde verificar que lo escrito en \"TAREA REALIZADA \"  coincida con el trabajo realizado por el técnico. Cualquier consulta llame al 0-810-333-1637, de Lun. a Vie. de 9:00 a 18:00 y sáb. de 9:00 a 13:00. 
Techtron Argentina S.A. se exime de responsabilidad en caso de no poder efectivizar la instalacion y/o continuar con la prestacion del servicio por imposibilidades de order tecnico, u otros factores que le sean ajenos. "),'','C');
$pdf->Rect(27,231,171,25);
$pdf->Rect(25,230,175,27);

$tec_nya = pg_fetch_object($sql_tec);


//$pdf->Line(25,275,85,275);
$pdf->SetFont('Arial','B',8);
$pdf->SetY(266);
$pdf->SetX(30);
$pdf->MultiCell(45,2,"TECNICO RESPONSABLE: ",'','C');
$pdf->SetY(270);
$pdf->SetX(30);

$tec_nom =  $tec_nya->us_nombre;
$tec_ape = $tec_nya->us_apellido;

$pdf->MultiCell(50,2,"$tec_nya->us_apellido, $tec_nya->us_nombre",'','C');

$pdf->SetFont('Arial','B',6);
$pdf->SetY(273);
$pdf->SetX(30);
$pdf->MultiCell(50,2,utf8_decode("Fecha Emisión: $fec_ser "),'','C');

$pdf->SetFont('Arial','B',8);
//$pdf->Line(125,260,185,260);

$pdf->SetY(266);
$pdf->SetX(125);
$pdf->MultiCell(63,4,"CONFORMIDAD. (Lea antes de firmar).        FIRMA Y  ACLARACION DEL CLIENTE.       ",'','C');

		}

$contador_soporte = $contador_soporte + 1;
		}
	if ($contador_soporte < $cantidad_soportes )
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',8);
}



$pdf->Output();

?>
