<?php
require_once("AplicacionController.php");


function get_cliente($cliente_id){

	$db= Conec_con_pass();
	$query = pg_exec($db, "select * from clientes where idclientes= '$cliente_id' ");
	$cliente = pg_fetch_object($query);
	return $cliente;
}

function get_phones($cliente_id){
	$db= Conec_con_pass();
}

function get_estado_cliente($estado_id){
	$db= Conec_con_pass();
	$query = pg_exec($db, "select * from  tipo_estados where id= $estado_id ");
	$estado = pg_fetch_object($query);
	return $estado;
}

function get_datos_server($cliente_id){
	$db= Conec_con_pass();
	$query = pg_exec($db, "Select idclientes, se_ip_pc, se_ip_ant, m.mikrotik_id, m.ip from server s
							inner join mikrotiks m on s.parent_mkt = m.mikrotik_id
							where idclientes = $cliente_id "); 
	return $query;
}

function set_nuevo_cliente($datos){
	require_once("../../Librerias/conn.php");
	require("PagosController.php");

	$db= Conec_con_pass();

pg_query("BEGIN");	
	$token = md5(datetime());
	$datos['fecha_nacimiento'];
	$fecha_nacimiento = cambiaFechaYYYYMMDD($datos['fecha_nacimiento']);


	if ($datos['txtdomdes'] == '') {$datos['txtdomdes'] = $datos['domicilio'];}
	if ($datos['txtbardes'] == '') {$datos['txtbardes'] = $datos['barrio'];}
	if ($datos['txtentdes'] == '') {$datos['txtentdes'] = $datos['referencia'];}


	$sql_cliente =  pg_exec($db,"INSERT INTO clientes(
		nombre, 
		apellido, 
		domicilio, 
		email, 
		dni, 
		observacion, 
		cuit,
		fecha_nac, 	
		localid, 
		barrio, 
		entre, 
		cl_emp, 
		cl_dirdes, 
		cl_bardes, 
		cl_refdes, 
		elim_ser,
		tipo_factura,
		token,
		organizacion_id)  
	VALUES (
		trim(upper('$datos[nombre]')),
		trim(upper('$datos[apellido]')),
		trim(upper('$datos[domicilio]')),
		'$datos[email]', 
		'$datos[dni]', 
		trim(upper('$datos[observaciones]')), 
		'$datos[cuit]', 
		'$fecha_nacimiento',
		'$datos[localidad_id]', 
		trim(upper('$datos[barrio]')),
		trim(upper('$datos[referencia]')),
		'1',
		trim(upper('$datos[txtdomdes]')), 
		trim(upper('$datos[txtbardes]')), 
		trim(upper('$datos[txtentdes]')),
		'1',
		'$datos[tipo_factura]',
		'$token',
		'$datos[organizacion_id]'
		)");


	if (pg_affected_rows($sql_cliente) == 0) {
		pg_query("ROLLBACK");
		return false;
	}

 	$cliente_id = pg_fetch_object(pg_exec($db, "select idclientes from clientes where token = '$token'"))->idclientes;
 	$num_tarjeta = pg_fetch_object(pg_exec($db, "select idnum_tarjeta from clientes where token = '$token'"))->idnum_tarjeta;
 	$costo_instalacion = $datos['costo_instalacion'];

 	$query_cliente_instalacion = pg_num_rows(pg_exec($db, "Select idclientes from instalaciones where idclientes = '$cliente_id'"));
 	
 	if ($query_cliente_instalacion == 0){

if (isset($datos['instalado']) == true){
	$importe_plan = get_plan($datos['plan_id'])->importe;
	$fecha_instalacion =cambiaFechaYYYYMMDD(date_server());
	$importe_proporcional = calcular_proporcional($fecha_instalacion, $importe_plan);
	$fecha_vencimiento =  primerDiaMesSiguiente($fecha_instalacion);
	$query1 = pg_exec($db, 
		"INSERT INTO instalaciones
		( 
			idclientes, 
			fecha_inst, 
			ip_pc, 
			ip_equipo, 
			instaladores, 
			cost_inst, 
			plan_id,
			instalado) 
	VALUES (
		'$cliente_id',
		'$fecha_instalacion',
		'$datos[ip_navegacion]', 
		'$datos[ip_antena]', 
		'$datos[instalador]', 
		'$datos[costo_instalacion]',
		'$datos[plan_id]',
		'$datos[instalado]')");

	if (pg_affected_rows($query) == 0) {
		pg_query("ROLLBACK");
		return false;
	}
		$validadDeuda = insertar_items_deuda($fecha_vencimiento, $importe_proporcional, 'Abono Proporcional', $cliente_id,$num_tarjeta);
		if ($validadDeuda == false){
		pg_query("ROLLBACK");
		return false;
	}
}else{
	$query1 = pg_exec($db, 
	"INSERT INTO instalaciones
		( 	idclientes, 
			cost_inst,
			plan_id)
	VALUES (
		'$cliente_id',
		'$datos[costo_instalacion]',
		'$datos[plan_id]')");


	if (pg_affected_rows($query1) == 0) {
		pg_query("ROLLBACK");
		return false;
	}

}
}
#insertamos equipos 
	if ($datos['equipo_id'] != ''){

	$insert = pg_exec($db, "Insert Into clientes_equipos(cliente_id, equipo_id, eliminado) values ('$cliente_id', '$datos[equipo_id]', false)");
    

	if (pg_affected_rows($insert) == 0) {
		pg_query("ROLLBACK");
		return false;
	}
    $update_equipos = pg_exec($db, "update equipos set eq_estado = 3 where idequipos = '$datos[equipo_id]'");

	}

#insertamos telefonos 
	$tel = $datos['telefono'];
	$cel = $datos['celular'];
	$cel2 = $datos['celular2'];

	if (!empty($tel)){
		$sql_phones_tel = pg_exec($db, "Insert into phones(cliente_id, number_phone) values('$cliente_id', '$tel')"); 
	}
	if (!empty($cel)){
		$sql_phones_cel = pg_exec($db, "Insert into phones(cliente_id, number_phone) values('$cliente_id', '$cel')");
	}
	if (!empty($cel2)){
		$sql_phones_cel = pg_exec($db, "Insert into phones(cliente_id, number_phone) values('$cliente_id', '$cel2')");
	}
#Costo de Instalacion
if ($costo_instalacion != ''){
	$cuotas = $datos['cuota_id'];
	$validarCuotas = insertar_cuotas_instalacion($costo_instalacion, $cuotas, $cliente_id);
	if ($validarCuotas == 0 || empty($validarCuotas)) {
		pg_query("ROLLBACK");
		return false;
	}
}
$cantidad_meses = $datos['cantidad_meses'];
if ($cantidad_meses != ''){
	$porcentaje = $datos['porcentaje_promocion'];
	$insertar_promocion =  insertar_promocion($cliente_id, $cantidad_meses, $porcentaje);
	if ($insertar_promocion == 0 || empty($insertar_promocion)) {
		pg_query("ROLLBACK");
		return false;
	}
}

	pg_query("COMMIT");
	return true;
}

function insertar_promocion($cliente_id, $cantidad_meses, $porcentaje){

	$db= Conec_con_pass();
	$row = pg_exec($db, "Insert into promociones (cliente_id, cantidad_meses, porcentaje) values('$cliente_id', '$cantidad_meses', '$porcentaje')");
	$validacion = pg_affected_rows($row);
	return $validacion;
}


function calcular_proporcional($fecha_instalacion, $importe){
	$ultimo_dia = ultimoDia($fecha_instalacion);
	$importe_dia = floor($importe / $ultimo_dia);
	$fecha_ultimo_dia = ultimoDiaMes($fecha_instalacion);
	$importe_dias = restarFechas($fecha_instalacion, $fecha_ultimo_dia) * $importe_dia ;
	return $importe_dias;
}

function calcular_proporcional_cliente_deshabilitado($fecha, $importe){
	
	$ultimo_dia = ultimoDia($fecha);
	$importe_dia = floor($importe / $ultimo_dia);
	$fecha_primer_dia = primerDiaMesWithoutParametes();
	$importe_dias = restarFechas($fecha_primer_dia, $fecha) * $importe_dia ;
	return $importe_dias;
}

function cambio_estado($array_clientes, $estado){
	$db= Conec_con_pass();
	foreach ($array_clientes as $pa)
	{
		 pg_query("BEGIN");
		switch ($estado) {
			case 'aviso':
				$sql_av_co = pg_exec($db, "UPDATE server SET se_esta ='2' where idclientes = '$pa'");
				$query1= pg_affected_rows($sql_av_co);

				$sql_cl_av = pg_exec($db, "update clientes set elim_ser ='2' where idclientes = '$pa'");
				$query2 = pg_affected_rows($sql_cl_av);
			break;
			case 'corte':
				$fec = cambiaf_a_bd(fecha_servidor_total());
				$tim_user = time_completa();
				$iduser = $_SESSION['iduser'];
				$tra_obs = "Falta de pagos";
				/*Actaulizo el estado del cliente*/ 
				$result1 = pg_exec($db,"UPDATE clientes SET elim_ser ='0', cl_tratam = '1' WHERE idclientes = '$pa'");
				$query1 = pg_affected_rows($result1);

				/*Inserto al cliente en tratamientos*/
				$result2 = pg_exec($db, "Insert into tratamientos (idtratam, tra_idclientes, tra_observaciones, tra_fecha, tra_time, tra_user, tra_idmotivo) values ((select nextval('seq_tratam')),'$pa','$tra_obs', '$fec', '$tim_user', '$iduser', '1')  ");
				$query2 = pg_affected_rows($result2);

				/* Deshabilito el Servicio */
				$dat_ser = pg_exec($db,"UPDATE server SET se_esta='0'  where idclientes = '$pa'");
				$query3 = pg_affected_rows($dat_ser);

				/*Inserto Novedad Diaria indicando que se dehabilito y esta en tratamientos */
				$sql_nd = pg_exec($db, "insert into novedades_diarias (id_nd, nd_fecha, nd_time, idcliente, nd_observacion, nd_solucion, nd_recibido, nd_referido, nd_fechasol, nd_timesol, nd_usersol, nd_esta) VALUES ((select nextval('seq_nd')), '$fec', '$tim_user','$pa','Tratamiento - $tra_obs','COMENTARIO', '$iduser', '0','$fec','$tim_user','$iduser', '1')");
        		//$nd_esta = si el estado es 1 = normal, 2 = Urgente
				echo $query4 = pg_affected_rows($sql_nd);

				$query2 =" INSERT INTO pagos ( idpago, fecha_ing, importe_deuda, idnum_tarjeta, idclientes, detalle) VALUES ((select nextval('seq_pagos')), '$fec', '300', '$num_tarj', '$pa', 'Recargo por Reconexion')";
				$query5= pg_query($query2);
                //echo $res2 = pg_affected_rows($result2);
                break;
			case 'sacar_aviso' :
				$sql_av_co = pg_exec($db, "UPDATE server SET se_esta ='1' where idclientes = '$pa'");
				$query1= pg_affected_rows($sql_av_co);
				$sql_nd_cl_av = pg_exec($db, "update clientes set elim_ser ='1' where idclientes = '$pa'  ");
				$query2 = pg_affected_rows($sql_nd_cl_av);
				break;
		}

		if (isset($query1) > 0 || isset($query2) > 0 || isset($query) > 0 || isset($query4) > 0 || isset($query5) > 0 ){
			pg_query("COMMIT");
		}else{
			pg_query("ROLLBACK");

		}
	}
	return;
}

function importe_plan_cliente($cliente_id){

			$db= Conec_con_pass();
		$query_plan  = pg_exec($db, "SELECT case when p.importe is null then 0::numeric else p.importe end  FROM instalaciones i left join planes p on i.plan_id = p.id  where idclientes = $cliente_id");
		$plan = pg_fetch_object($query_plan);
		
		return $plan;
}


function consulta_ip_o_rango($ip){

	$db= Conec_con_pass();
	$sql = pg_exec($db, "select * from clientes inner join server on clientes.idclientes = server.idclientes where se_ip_pc ilike '%$ip%' or se_ip_ant ilike '%$ip%' order by elim_ser asc  "); 
	//$ip = pg_fetch_object($sql);
	return $sql;
}

function update_clientes($datos){
	$db= Conec_con_pass();

$nombre = $datos['nombre'];
  $apellido = $datos['apellido'];
  $domicilio = $datos['domicilio'];
  $barrio = $datos['barrio'];
  $entre = $datos['referencia'];
  $localidad_id = $datos['localidad_id'];
  $observacion = $datos['observacion'];
  $email = $datos['email'];
  $dni = $datos['dni'];
  $cuit = $datos['cuit'];
  $cliente_id = $datos['cliente_id'];
  $fecha_nacimiento = cambiaFechaYYYYMMDD($datos['fecha_nacimiento']);


  $phones_new = $datos['phone_number_form'];
  $phones_update = $datos['telefono'];

    pg_query("BEGIN");
      $clientes = pg_exec($db,"UPDATE clientes SET 
                    nombre = '$nombre', 
                    apellido = '$apellido', 
                    domicilio = '$domicilio',
                    barrio = '$barrio',
                    entre = '$entre',
                    email = '$email',
                    observacion = '$observacion',
                    localid = '$localidad_id',
                    dni = '$dni',
                    cuit = '$cuit',
                    fecha_nac = '$fecha_nacimiento'
                    WHERE idclientes = '$cliente_id'");
    
      if (!empty($phones_new)){
        for ($i=-1; $i < count($phones_new) ; $i++) { 
          if(!empty($phones_new[$i])){
            $phones = pg_query($db, "Insert into phones(cliente_id, number_phone) values ('$cliente_id', '$phones_new[$i]')");
          }
        }
      }

      foreach ($phones_update as $key => $value) {
            if (!empty($value)){
              $phones_update = pg_query($db, "UPDATE phones SET number_phone = '$value' WHERE id = '$key'");
            }else{
            $phones_update = pg_query($db, "DELETE fROM phones WHERE id = '$key'");
        }
      }

 if (pg_affected_rows($clientes))
      {
          pg_query("COMMIT");
          $mensaje = "<div class ='alert-success'>Actualizado Exitosamente </div>";

          return $mensaje;

       }else{
pg_query("ROLLBACK");
          $mensaje = "<div class ='alert-warning'>Registro no Actualizado </div>";
          return $mensaje;
      }
}

function habilitar_precliente($cliente_id){
	require_once("PagosController.php");
	require_once("PlanesController.php");

	$db= Conec_con_pass();
	
	$query = pg_exec("select plan_id, idnum_tarjeta, fecha_inst from clientes c join instalaciones i on c.idclientes = i.idclientes  where c.idclientes = '$cliente_id'");
	$datos = pg_fetch_object($query);
	$num_tarjeta = $datos->idnum_tarjeta;
	$importe_plan = get_plan($datos->plan_id)->importe;
	$fecha_instalacion =$datos->fecha_inst;
	$dia_actual = dia_actual(); //trae el numero del dia
	$mes_actual = mes_actual();
	$ano_actual = ano_actual();
	$fecha_insertar_cuota = $ano_actual.'-'.$mes_actual.'-23';
	$fecha_primer_dia_mes = primerDiaMes(date('Y-m-d'));
	$fecha_actual = date('Y/m/d');

	if(is_null($fecha_instalacion)){
		$msj = "Debe completar la fecha de instalacion";
		return array('false', $msj);
	}
	pg_query("BEGIN");
	
    $mes_fecha_instalacion = obtener_mes_fecha($fecha_instalacion);
	
	if ($mes_fecha_instalacion != $mes_actual){
    	$cantidad_dias_instalacion =  restarFechas($fecha_instalacion, $fecha_actual);
    	if( $cantidad_dias_instalacion > 7){
       $msj = "La cantidad de dias atrazados no puede ser mayor a 7 dias ";
      return array('false', $msj);
    }else{
    	$validar_abono_mes_actual = pg_exec($db, "select * from pagos where fecha_ing = '$fecha_primer_dia_mes' and detalle ilike '%Abono Mensual%' and idclientes = $cliente_id ");
    	if(pg_num_rows($validar_abono_mes_actual) < 1){
    		$insertar_abono_mes = insertar_items_deuda($fecha_primer_dia_mes, $importe_plan, 'Abono Mensual', $cliente_id,$num_tarjeta);	
    		if($insertar_abono_mes == false){
				pg_query("ROLLBACK");
				$msj = "Error al insertar Abono Mensual";
				return array('false', $msj);	
				}	
    		}
    	}
	}
	
	$importe_proporcional = calcular_proporcional($fecha_instalacion, $importe_plan);
	
	$validarDeudaProporcional = insertar_items_deuda($fecha_instalacion, $importe_proporcional, 'Abono Proporcional', $cliente_id,$num_tarjeta);
	
	if($validarDeudaProporcional == false){
		pg_query("ROLLBACK");
		$msj = "Error al insertar proporcional";
		return array('false', $msj);	
	}

	if($fecha_instalacion < $fecha_insertar_cuota){
		$query_cuotas = pg_fetch_object(pg_exec($db, "select * from cuotas where cliente_id = '$cliente_id' and finalizado = 'false'"));
		$importe_cuota = $query_cuotas->importe_cuota;
		$cuota_abonadas = $query_cuotas->cuota_abonada;
		$finalizado_cuotas = $query_cuotas->finalizado;
		$cuota_id = $query_cuotas->id;
		$cantidad_cuotas = $query_cuotas->cantidad_cuota;
		$numero_cuota_abonar = $cuota_abonadas + 1;

		$detalle_cuota = "Cuota Instalacion ". $numero_cuota_abonar."/".$cantidad_cuotas; 

		if($cantidad_cuotas > $cuota_abonadas){
			$insertar_cuotas = insertar_items_deuda($fecha_instalacion, $importe_cuota, $detalle_cuota, $cliente_id, $num_tarjeta, true);
			if ($insertar_cuotas == false){
				pg_query("ROLLBACK");
				$msj = "Error al insertar cuotas";
				return array('false', $msj);	
			}
			if($numero_cuota_abonar == $cantidad_cuotas){
					$finalizado_cuotas = 'true';
				}
			$update_cuotas = pg_exec($db, "update cuotas set cuota_abonada = '$numero_cuota_abonar' , finalizado = '$finalizado_cuotas'where id = $cuota_id ");

			if (pg_affected_rows($update_cuotas) < 1){
				pg_query("ROLLBACK");
				$msj = "Error al actualizar cuotas";
				return array('false', $msj);	

			}
		}
	}
	
	$habilitar = pg_exec($db, "update instalaciones set fecha_inst = '$fecha_instalacion', instalado = 'true' where idclientes = '$cliente_id' ");
	
	if(pg_affected_rows($habilitar) == 0 ){
		pg_query("ROLLBACK");
		$msj = "Error al habilitar el cliente instalacion";
		return array('false', $msj);
	}

	$cambio_tipo_cliente = pg_exec($db, "update clientes set tipo_cliente_id = 1 where idclientes = '$cliente_id'");

	if(pg_affected_rows($cambio_tipo_cliente) == 0 ){
		$msj = "Error al habilitar el cliente tipo de cliente";
		return array('false', $msj);
	}

	pg_query("COMMIT");
		$msj = "Cliente habilitado";
		return array('true', $msj);
}


function eliminar_cliente_pre($cliente_id){

		$db= Conec_con_pass();
		$query = pg_exec($db, "Select * from pagos p
								inner join clientes c on p.idclientes = c.idclientes
								inner join comprobantes co on p.idpago = co.pago_id
								where c.idclientes = '$cliente_id' and tipo_cliente_id = '2' and importe_pagado > 0 ");

		$datos = pg_num_rows($query);

		if ($datos > 0){
			$validar_exite_nota_credito = pg_exec($db, "select * from comprobantes 
						inner join pagos on comprobantes.pago_id = pagos.idpago
						where tipo_comprobante ilike 'NotaCredito%' and pagos.idclientes = '$cliente_id'");
			if (pg_num_rows($validar_exite_nota_credito) > 0){
				$update_cliente_pre = pg_exec($db, "update clientes set elim_clie = 'true' and elim_ser = '0' where idclientes = '$cliente_id'");
			if(pg_affected_rows($update_cliente_pre)){
					$msj = "Registro Eliminado";
					return array(true,$msj);
			}else{
				$msj = "Error al eliminar el registro";
				return array(false,$msj);
			}
			}else{
				$msj = "Tiene Comprobante asignado, Por favor realizar nota de credito";
				return array(false,$msj);
			}

	}else{
pg_query("BEGIN");
		$pagos_eliminar = pg_exec($db,"delete from pagos where idclientes = '$cliente_id'");
		$instalaciones_eliminar = pg_exec($db,"delete from instalaciones where idclientes = '$cliente_id'");
		$server_eliminar = pg_exec($db,"delete from server where idclientes = '$cliente_id'");
		$cliente_eliminar = pg_exec($db,"delete from clientes where idclientes = '$cliente_id'");
		
		if (pg_affected_rows($cliente_eliminar)> 0 and pg_affected_rows($instalaciones_eliminar)> 0){
			pg_query("COMMIT");
			$msj="Eliminado con exito";
			return array(true, $msj);
		}else{
			pg_query("ROLLBACK");
			$msj = "No se pudo Eliminar";
			return array(false, $msj);
		}
	}
	}


function insertar_cuotas_instalacion($costo_instalacion, $cuotas, $cliente_id ){

	$db= Conec_con_pass();
	$importe_cuota = floor($costo_instalacion / $cuotas) ; 
	$detalle = "Costo instalacion";
	$insertar_cuotas = pg_exec($db, "Insert into cuotas(cliente_id, cantidad_cuota, importe_cuota, importe_total, detalle) values($cliente_id, $cuotas, $importe_cuota, $costo_instalacion, '$detalle')");
	
	$validacion = pg_affected_rows($insertar_cuotas);
	return $validacion;
}

function deshabilitar_cliente($cliente_id, $user_id, $comentario){
	require_once("PagosController.php");
	require_once("ClientesController.php");
	require_once("MikrotiksController.php");

	pg_query("BEGIN");
		$db= Conec_con_pass();
		
		$actualizar_estado = pg_exec($db, "update clientes set elim_ser = '0' where idclientes = $cliente_id ");
		$estado_cliente = pg_affected_rows($actualizar_estado);

		$actualizar_estado_server = pg_exec($db, "update server set se_esta = '0' where idclientes = $cliente_id ");
		$estado_cliente_server = pg_affected_rows($actualizar_estado_server);

		$cerrar_promersa = cerrar_promesas($cliente_id);

		if($cerrar_promesas[0] == 'false' ){
			pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado";
			return['false', $mensaje];	
		}
			$fecha_nd = cambiaFechaYYYYMMDD(date_server());
			$hora_actual = time_completa();
			
			$comentario_nd = "Tratamientos: " . $comentario;
		 $sql_nd = pg_exec($db, "insert into novedades_diarias ( nd_fecha, nd_time, idcliente, nd_observacion, nd_solucion, nd_recibido, nd_referido, nd_fechasol, nd_timesol, nd_usersol, nd_esta ) VALUES ('$fecha_nd', '$hora_actual','$cliente_id','$comentario_nd','COMENTARIO', '$user_id', '0','$fecha_nd','$hora_actual','$user_id', '1')");

		$fecha_actual = cambiaFechaYYYYMMDD(date_server());
		$importe_recargo_reconexion = 300;
		$detalle = "Recargo por reconexion";
		$num_tarjeta = get_cliente($cliente_id)->idnum_tarjeta;
		$insertar_recargo = insertar_items_deuda($fecha_actual, $importe_recargo_reconexion, $detalle, $cliente_id, $num_tarjeta);
		if ($insertar_recargo == false){
			pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado - no se pudo insertar importe de reconexion";
			return['false', $mensaje];	
		}

		$mes_actual = mes_actual();
		$ano_actual = ano_actual();
		$fecha_abono = $ano_actual."-".$mes_actual."-01";
		$importe_abono = importe_plan_cliente($cliente_id)->importe;

		$validar_existe_abono = pg_exec($db, "select * from pagos where fecha_ing='$fecha_abono' and detalle='Abono Mensual' and idclientes = $cliente_id");
		
		if(pg_num_rows($validar_existe_abono) > 0 ){
			$detalle_bonificacion = "Anulacion Abono mensual";
			$insertar_nota_credito_abono = insertar_items_bonificacion($fecha_actual, $importe_abono, $detalle_bonificacion, $cliente_id, $num_tarjeta);
			if ($insertar_nota_credito_abono == false){
				pg_query("ROLLBACK");
				$mensaje = "Cliente no actualizado - no se pudo anular el abono";
				return['false', $mensaje];	
			}
		}

		$importe_proporcional = calcular_proporcional_cliente_deshabilitado($fecha_actual, $importe_abono);
		$detalle_inactivo = "Proporcional dias consumidos";
		$insertar_proporcional_consumido = insertar_items_deuda($fecha_actual, $importe_proporcional, $detalle_inactivo, $cliente_id, $num_tarjeta);

		if ($insertar_proporcional_consumido == false){
			pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado - no se pudo insertar proporcional días consumido";
			return['false', $mensaje];	
		}
			
		$mikrotik_id =add_user_mikrotik_navegacion(pg_fetch_object(get_datos_server($cliente_id))->mikrotik_id);

		if($mikrotik_id != 1 ){
			pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado - Error  conexion mikrotik";
			return['false', $mensaje];	
			}

		$insertar_cliente_tratamiento = insertar_cliente_tratar($cliente_id, $comentario,  $user_id);

		if($insertar_cliente_tratamiento == 'false'){
			pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado - Error al insertar en tratamientos";
			return['false', $mensaje];	
		}

	if ($estado_cliente == 1 && $estado_cliente_server == 1){
		pg_query("COMMIT");

		$mensaje = "Cliente deshabilitado";
		return['true', $mensaje];
	}else{
		pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado";
		return['false', $mensaje];	
	}
}

function habilitar_cliente($cliente_id){
	require_once("PagosController.php");
	require_once("ClientesController.php");
	require_once("MikrotiksController.php");

	//pg_query("BEGIN");
		$db= Conec_con_pass();
		
		$validar_estado_cliente =pg_fetch_object(pg_exec($db, "Select elim_ser, elim_clie, se_esta from clientes c join server s 
												on c.idclientes = s.idclientes where c.idclientes=$cliente_id"));

		$estado_cliente_servidor = $validar_estado_cliente->se_esta;
		$estado_cliente = $validar_estado_cliente->elim_ser;
		$estado_cliente_no_activo = $validar_estado_cliente->elim_clie;

		/*if($estado_cliente_no_activo == 'true'){
			//pg_query("ROLLBACK");
			$mensaje = "Cliente esta de baja";
			return['false', $mensaje];	
		}*/

		/*if($estado_cliente_servidor > 0 && $estado_cliente != '0' ){
			//pg_query("ROLLBACK");
			$mensaje = "Cliente está habilitado";
			return['false', $mensaje];	
		}*/
		
		$actualizar_estado = pg_exec($db, "update clientes set elim_ser = '1', elim_clie='false' where idclientes = $cliente_id ");
		$estado_cliente = pg_affected_rows($actualizar_estado);

		$actualizar_estado_server = pg_exec($db, "update server set se_esta = '1' where idclientes = $cliente_id ");
		$estado_cliente_server = pg_affected_rows($actualizar_estado_server);

		$fecha_actual = cambiaFechaYYYYMMDD(date_server());
		$importe_abono = importe_plan_cliente($cliente_id)->importe;
		$importe_proporcional = calcular_proporcional($fecha_actual, $importe_abono);
		$detalle_proporcional = "Proporcional de habilitacion";
		$num_tarjeta = get_cliente($cliente_id)->idnum_tarjeta;
		$insertar_proporcional = insertar_items_deuda($fecha_actual, $importe_proporcional, $detalle_proporcional, $cliente_id, $num_tarjeta);

		if ($insertar_proporcional == false){
			//pg_query("ROLLBACK");
			$mensaje = "Cliente no habilitado";
			return['false', $mensaje];	
		}

		 echo $mikrotik_id =add_user_mikrotik_navegacion(pg_fetch_object(get_datos_server($cliente_id))->mikrotik_id);

		if($mikrotik_id != 1){
			//pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado - Error  conexion mikrotik";
			return['false', $mensaje];	
		}

	if ($estado_cliente == 1 && $estado_cliente_server == 1){
		//pg_query("COMMIT");
		$mensaje = "Cliente habilitado";
		return['true', $mensaje];
	}else{
		//pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado";
		return['false', $mensaje];	
	}
}

function estado_actual_cliente($cliente_id){ 
	$db= Conec_con_pass();

}

function consulta_cliente_en_tratamiento($cliente_id){
	$db= Conec_con_pass();
	$query_tratamientos = pg_exec($db, "Select * from tratamientos where tra_idclientes = $cliente_id and tra_cerrada = 'false'");

	if(pg_num_rows($query_tratamientos)> 0 ){
		return 'true';
	}else{
		return 'false';
	}
}

function eliminar_cliente($data){
	require_once("PagosController.php");
	require_once("MikrotiksController.php");
	$db= Conec_con_pass();

	$cliente_id = $data['cliente_id'];
	$fecha_baja = cambiaFechaYYYYMMDD(date_server());
	$hora_actual = time_completa();
	$user = $data['user_id'];
	$motivo_id = $data['mot_baja'];

	pg_query("BEGIN");
	
    $nombre_motivo = pg_fetch_object(pg_exec($db, "select nombre from motivos where id = $motivo_id"))->nombre;	
	$motivo =  $nombre_motivo. " - ".  $data['tra_obs'];
    $deuda_actual = calcular_deuda($cliente_id);
    $ps_obs = "Deuda a la Fecha: $deuda_actual - Se le retira equipos - $motivo";
    
       /* Inserto una novedad para indicar que al cliente se le va a retirar el equioi*/
     $sql_nd = pg_exec($db, "insert into novedades_diarias ( nd_fecha, nd_time, idcliente, nd_observacion, nd_solucion, nd_recibido, nd_referido, nd_fechasol, nd_timesol, nd_usersol, nd_esta ) VALUES ('$fecha_baja', '$hora_actual','$cliente_id','$ps_obs','COMENTARIO', '$user', '0','$fecha_baja','$hora_actual','$user', '1')");


    if(pg_affected_rows($sql_nd) < 1){
    	pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado - Error  Insertar Novedad";
		return['false', $mensaje];
    }
     
     /*Inserto una orden de retiro*/
    $ins_nd_recl = pg_exec($db, "INSERT INTO reclamos (idreclamos, fecha_recla, idclientes, falla, usuario, terminado, rc_idnov_dia, rc_ret_equ)values ((select nextval('seq_reclamos')),'$fecha_baja', '$cliente_id', '$ps_obs', '$user','false', '0','9001')");
    if(pg_affected_rows($ins_nd_recl) < 1){
    	pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado - Error  Insertar Order Retiro";
		return['false', $mensaje];
    }

	$actualizar_instalacion = pg_exec($db, "UPDATE instalaciones SET elim_inst = 'true'  WHERE idclientes = $cliente_id");

	if(pg_affected_rows($actualizar_instalacion) < 1){
    	pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado - Error  actualizar estado";
		return['false', $mensaje];
    }


	$actualizar_baja_cliente = pg_exec($db,"UPDATE clientes SET  elim_clie = 'true', fecha_baja = '$fecha_baja', user_baja = '$user', motivo = '$motivo', id_motivo = $motivo_id, elim_ser ='0', cl_tratam = '1' WHERE idclientes = '$cliente_id' ");

	if(pg_affected_rows($actualizar_baja_cliente) < 1){
    	pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado - Error  actualizar estado";
		return['false', $mensaje];
    }

	$validar_tratamiento =  pg_exec($db, "select * from tratamientos where tra_idclientes ='$cliente_id' and tra_cerrada = 'false'");
	if(pg_num_rows($validar_tratamiento) > 0 ){
		$tratamiento_id = pg_fetch_object($validar_tratamiento)->idtratam;
	$query = pg_exec($db, "Update tratamientos set tra_cerrada = 'true' where idtratam in($tratamiento_id)");

	if(pg_affected_rows($query) < 1){
    	pg_query("ROLLBACK");
		$mensaje = "Cliente no actualizado - Error no se pudo actualizar tratamientos";
		return['false', $mensaje];
    }
}
	$mikrotik_id =add_user_mikrotik_navegacion(pg_fetch_object(get_datos_server($cliente_id))->mikrotik_id);

		if($mikrotik_id != 1){
			pg_query("ROLLBACK");
			$mensaje = "Cliente no actualizado - Error  conexion mikrotik";
			return['false', $mensaje];	
		}
		pg_query("COMMIT");
		$mensaje = "Cliente Eliminado";
		return["true", $mensaje];
}

function restaurar_cliente_tratamiento($tratamiento_id){
	pg_query("BEGIN");
	$db= Conec_con_pass();

	$cliente_id = pg_fetch_object(pg_exec($db, "select tra_idclientes from tratamientos where idtratam = '$tratamiento_id'"))->tra_idclientes;
	
	$habilitar_cliente = habilitar_cliente($cliente_id);

	if ($habilitar_cliente[0] == 'false'){
		pg_query("ROLLBACK");
		$mensaje = "Error al habilitar el cliente";
		return['false', $mensaje];	
	}

	$query = pg_affected_rows(pg_exec($db, "Update tratamientos set tra_cerrada = 'true' where idtratam = $tratamiento_id"));
	
	if ($query == 1){
		pg_query("COMMIT");
		$mensaje = "Cliente habilitado";
		return['true', $mensaje];
	}else{
		pg_query("ROLLBACK");
		$mensaje = "Error al actualizar estado del cliente";
		return['false', $mensaje];	
	}
}


function restaurar_cliente_baja($cliente_id){
	pg_query("BEGIN");
	$db= Conec_con_pass();

	$habilitar_cliente = habilitar_cliente($cliente_id);

	if ($habilitar_cliente[0] == 'false'){
		pg_query("ROLLBACK");
		$mensaje = $habilitar_cliente[1];
		return['false', $mensaje];	
	}else{
		pg_query("COMMIT");
		$mensaje = "Cliente Habilitado";
		return['true', $mensaje];	
	}
}

function pasar_estado_habilitado($cliente_id){

	$db= Conec_con_pass();
	$query_clientes = pg_affected_rows(pg_exec($db, "update clientes set elim_ser = '1' where idclientes = $cliente_id"));
	$query_server = pg_affected_rows(pg_exec($db, "update server set se_esta = '1' where idclientes = $cliente_id"));

	if($query_clientes == 1 && $query_server == 1){
		$mensaje = "Actualizado";
		return['true', $mensaje];
	}else{
		$mensaje = "No actualizado";
		return['false', $mensaje];
	}
}

?>