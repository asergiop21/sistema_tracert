<?php
function generar_comprobantes_pdf($cliente_ids, $mes, $ano, $imprimir_imagen='true'	){

	require_once('AplicacionController.php');
	require_once("../../Librerias/conn.php");
	require_once('../../Librerias/pdf.php');
	require_once('../../Librerias/code128/code128.php');


	$db= Conec_con_pass();
	$ids  = implode(',', $cliente_ids);
	if(count($ids)== 0){
		$ids = $cliente_ids;
	}
	
	$datos = pg_exec($db, "Select c.*, i.*, p.*, l.*, o.nombre as nombre_organizacion, c.nombre as nombre_cliente from clientes 
		c inner join instalaciones i on c.idclientes = i.idclientes 
		inner join planes p on i.plan_id = p.id
		inner join localidad l on c.localid = l.idlocalidad
		inner join organizaciones o on c.organizacion_id = o.id
								where c.idclientes in ($ids)"); //(6932, 3777, 6021, 5943, 3572)
	$total_rows = pg_num_rows($datos);

	$numero_mes = $mes;
	if ($numero_mes == '12'){
		$fe_meses2 =  '01' ;
		$ano2 = $ano + 1;
	}else{
		$fe_meses2 = $numero_mes + 1 ;
		$ano2 = $ano;
	}	
	
	$pdf = new PDF_Code128();
	$pdf->AddPage('P', 'A4');
	$pdf->SetFont('Arial','B',16);

	$cant = 1;
	while ($cli = pg_fetch_object($datos)){
		$numero_mes = $mes;
		if ($numero_mes == '12'){
			$fe_meses2 =  '01' ;
			$ano2 = $ano + 1;
		}else{
			$fe_meses2 = $numero_mes + 1 ;
			$ano2 = $ano;
		}	
		$x = 20;
		$y = 60;
		$X = 75;
		$Y = 20;
		$rx = 17;
		$ry = 99;
		$debe = 0;
		$haber = 0;
		$deuda = 0;

		$code = '';
	//while ($cli = pg_fetch_object($sql_mm)) {
		$ano_calculo = substr($ano, -2);
		$fecha_primer_vto = date($ano.'/'.$numero_mes.'/10');
		$fecha_primer_dia_ano = date($ano.'/01/01');


		
		$ndias = 1;
		$fe_diez = sumarmeses_nuevo(fecha_10(),$ndias);

		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"APELLIDO y NOMBRE:")	;
		$x = $x + 35;

		$nomyape = $cli->apellido.", ". $cli->nombre_cliente;
		$nombre_apellido = substr($nomyape,0,35);

		$pdf->SetFont('Arial','',14);
		$pdf->Text($x,$y,$nombre_apellido);
		//$y= $y + 10;

		$fec_ser = fecha_servidor_total();
		$x = $x + 117;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,"$fec_ser");
		$y= $y + 10;


		$x= $x - 153;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"DOMICILIO: ");
		$x = $x + 35;
		$pdf->SetFont('Arial','',14);
		$pdf->Text($x,$y,$cli->domicilio);
		$y= $y + 10;

		$x= $x - 35;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"ENTRE: ");
		$x = $x + 35;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,$cli->entre);
		$y= $y + 10;

		$x= $x - 35;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"BARRIO: ");
		$x = $x + 20;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->barrio);
		//$y= $y + 5;

		$x = $x + 70;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"LOCALIDAD: ")	;
		$x= $x + 18;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->num_loc);

		$y= $y + 5;

		$x = 20;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"DEPARTAMENTO: ")	;
		$x= $x + 25;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->dpto)	;


		$x = $x + 70;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"C.P.: ")	;
		$x= $x + 8;
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,$cli->cp)	;
		$x= $x + 23;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y," CLIENTE.: ")	;
		$x= $x + 25;
		$pdf->SetFont('Arial','B',10);
		$pdf->Text($x,$y,$cli->idnum_tarjeta)	;

		$y= $y + 12;
		$x = 35;
		$pdf->Rect($rx , $ry,175,12);
		$ry = $ry + 30;

		$clie = $cli->idclientes;

		$sql1 = pg_exec($db,"select * from pagos where idclientes = '$clie'");
		while ($pagos = pg_fetch_object($sql1)){
			$debe = $debe + $pagos->importe_deuda;
			$haber = $haber + $pagos->importe_pagado;
			$deuda = $haber - $debe;
			if ($deuda > 0)
			{
				$deuda = 0;
			}
		}
		$deuda = 0 - $deuda ;
		$pdf->SetFont('Arial','',8);
		$pdf->Text($x,$y,"Deuda a la fecha: $". number_format($deuda,2));
		$x = $x + 60;
		$pdf->Text($x,$y,utf8_decode("Fecha de Emisión: ".$fe_em = fecha_servidor_total()));
		$x = $x + 50;
		//$pdf->Text($x,$y, "Abono: $".$cli->abono);
		$x = $x - 90;
		$y = $y + 15;

		$pdf->SetFont('Arial','',12);
		$pdf->Text($x,$y, "Sr. Cajero por favor respete las fechas de vencimiento");

		$x = $x - 30;
		$y = $y + 15;

		$abono = $cli->importe;

		$pdf->Rect($rx, $ry,67,40);
		$rx = $rx + 107;
		$pdf->SetFont('Arial','UB',10);
		$pdf->Text($x,$y,"Vencimientos: ".meses($numero_mes)." ".$ano);
		$x = $x + 105;

		$pdf->Rect($rx , $ry,67,40);
		$rx = $rx - 100;
		$ry = $ry + 100;

		$pdf->Text($x,$y,"Vencimientos: ".meses($fe_meses2)." ".$ano2);
		$x = 25; // 20
		$y = $y + 10; //105
		$pdf->SetFont('Arial','',10);
		$pdf->Text($x,$y,"Del 1/".$numero_mes. " al 10/".$numero_mes.": $".number_format($cli->importe,2, ',', '.')); //Abono mensual
		$x = $x + 110;
		$pdf->Text($x,$y,"Del 1/".$fe_meses2. " al 10/".$fe_meses2.": $".number_format($cli->importe,2, ',', '.')); //Abono mensual
		$x = 25; // 20
		$y = $y + 10; //115

		$primer_recargo = $cli->importe + $cli->importe_recargo;
		$pdf->Text($x,$y,"Del 11/".$numero_mes. " al 18/".$numero_mes.": $".number_format($primer_recargo,2, ',', '.'));
		$x = $x + 110;
		$pdf->Text($x,$y,"Del 11/".$fe_meses2. " al 18/".$fe_meses2.": $". number_format($primer_recargo,2, ',', '.')) ;
		$x = 25; // 20
		$y = $y + 10; //115


		$segundo_recargo = $primer_recargo + $cli->importe_recargo;
		$pdf->Text($x,$y,"19/".$numero_mes." al 23/".$numero_mes.": $". number_format($segundo_recargo,2, ',', '.'));
		$x = $x + 110;
		$pdf->Text($x,$y,"19/".$fe_meses2." al 23/".$fe_meses2.": $". number_format($segundo_recargo,2, ',', '.'));

//Original_generación_codigo_de_Barras		
		$pdf->SetFont('Arial','',8);
		$y = $y + 5 ;
		$x = 18;
		//$x = $x - 60;
		$organizacion = $cli->nombre_organizacion;
		for ($i=0; $i < 2 ; $i++) { 

			if ( $organizacion == 'techtron'){
				$tar = $cli->idnum_tarjeta;
				$digito = digito_verificador_nuevo($tar);
				$pdf->Code128($x,$y,'152'.$tar.$digito, 65,10);
				$y = $y + 13;
				$pdf->Text($x,$y,'152'.$tar.$digito,110,10);
			}
			if ( $organizacion == 'protec'){
				$code .='1720';
				$code .=str_pad(number_format($cli->importe, 2 ,"",""), 8, "0", STR_PAD_LEFT);
				$code .=$ano_calculo . str_pad(diasTranscurridos($fecha_primer_dia_ano, $fecha_primer_vto), 3, "0", STR_PAD_LEFT);
				$code .='01963';
				$code .= '9';
				$code .= substr(number_format($cli->idnum_tarjeta, 2 , "", ""), 2);
				$code .= '0';
				$code .= str_pad(number_format($cli->importe_recargo	, 2 , "", ""), 4, "0", STR_PAD_LEFT);
				$code .= '08';
				$code .= calculo_digito_verificador($code);
				$code .= calculo_digito_verificador($code);

				$pdf->Code128($x,$y,$code,65,10);
				$y = $y + 13;
				$pdf->Text($x,$y,$code,110,10);
			}
			$x = 125;
			$y = $y - 13;
			$code = '';
			$ano_calculo = substr($ano2, -2);
			$numero_mes = $fe_meses2;
			$fecha_primer_vto = date($ano.'/'.$numero_mes.'/10');
			$fecha_primer_dia_ano = date($ano2.'/01/01');
		}

		$x = 35; // 20
		$y = $y + 15; //95
		$pdf->SetY(190);
		$pdf->SetX(20);
		$pdf->Rect(17, 190, 175 ,50);
		$pdf->Multicell(170,6, utf8_decode($pdf->SetFont('Arial','B',8)."Puede utilizar este código de barras para regularizar deuda anterior (incluso fuera de las fechas de vencimiento)."),'0');                
		$pdf->SetY(195);
		$pdf->SetX(20);
		$pdf->Multicell(170,6, utf8_decode($pdf->SetFont('Arial','',8)."Mediante este cupón puede abonar su servicio en Rapipago o Pagofácil. Su deuda aparece en el recuadro superior del cupón. Recuerde abonar antes de los 30 días corridos del primer vencimiento para evitar la suspensión del servicio y cargos por reconexión. La baja del servicio ante la falta de pago del mismo no implica en el cliente el cese de su obligación de cancelar los documentos impagos. El tiempo durante el cual esté suspendido el servicio es responsabilidad del cliente y será facturado. Si por alguna razón no recibe nuestro cupón de pago, puede solicitarlo también telefónicamente antes del día 5 de cada mes, como así para solicitar servicios de mayores velocidades. Recomendamos evitar el pago posterior al mes del vencimiento ya que el mismo generará un gasto de gestión de cobranza y/o pago fuera de término."),'0');                

		$y = $y + 45;
		$x = 55;


//Test_1_codigo_de_barras
		/*$code .='1720';
		$code .= str_pad(number_format(250.50, 2 , "", ""), 8, "0", STR_PAD_LEFT);
		$code .=$ano_calculo . diasTranscurridos('2016/01/01', '2016/11/30');
		$code .='01963';
		$code .= '9';
		$code .= str_pad(number_format($row->idnum_tarjeta, 2 , "", ""), 8, "0", STR_PAD_LEFT);
		$code .= '0';
		$code .= str_pad(number_format(300, 2 , "", ""), 6, "0", STR_PAD_LEFT);
		$code .= diasTranscurridos('2016/11/30', '2016/12/25');;
		$code .= calculo_digito_verificador($code);
		$code .= calculo_digito_verificador($code);*/

		if ($imprimir_imagen == 'true'){
			$pdf->SetY(5);
			$pdf->SetX(5);
			$pdf->Image('../../image/encabezado.jpg', '5', '5', '200', '50');

			$pdf->SetY(230);
			$pdf->SetX(140);
			$pdf->Image('../../image/pie.jpg', '5', '265', '200', '30');
		}

		$fe_em = cambiaf_a_bd($fe_em);
		$pdf->Ln(10);

		if ($cant < $total_rows  ){
			
			$pdf->AddPage('P', 'A4');
		}
		$cant = $cant + 1;
	}
	$pdf->Output();

}


function calculo_digito_verificador($code){

	$code_array = str_split($code);

	$totalString = count($code_array);
	$digitos = 1;
	$code_original =str_split(str_pad($digitos, $totalString,'3579'));
	$sumados = array_sum(array_map('sumar_array', $code_array, $code_original));
	$primer_division = $sumados / 2;
	$resto_segunda_division = $primer_division % 10;
	return $resto_segunda_division;

}

function digito_verificador_nuevo($clie)
{
	$tar=array( "152".$clie);

	foreach ($tar as $key => $value)
	{
		$suma = 0;
		$suma = $value[0] * 1 + $suma;
		$suma = $value[1] * 3 + $suma;
		$suma = $value[2] * 5 + $suma;
		$suma = $value[3] * 7 + $suma;
		$suma = $value[4] * 9 + $suma;
		$suma = $value[5] * 3 + $suma;
		$suma = $value[6] * 5 + $suma;
		$suma = $value[7] * 7 + $suma;
		$suma = $value[8] * 9 + $suma;
		$suma = $value[9] * 3 + $suma;
		$suma = $value[10] * 5 + $suma;
		$suma = $value[11] * 7 + $suma;
		$suma = $value[12] * 9 + $suma;
	}

	$resultado = $suma / 2;

	if ($resultado < '100')
	{
		return (int)$sum =substr($resultado,1);
	}
	else
	{
		return (int)$sum =substr($resultado,2);
	}

}



function imprimir_factura($pago_id){


	require_once('../../Librerias/pdf.php');
	require_once("LocalidadesController.php");
	require_once("AplicacionController.php");
	$db = Conec_con_pass();

	require('../../i25.php');

  $pago_id = $pago_id;//373436;
 //$pago_id = $_REQUEST['pago_id'];

  $count = 0;

  $sql_pagos  = pg_exec($db, "Select p.importe_pagado,c.*, p.idclientes, c.id, cl.*,
  	o.direccion as direccion_factura,
  	o.cuit as cuit_factura,
  	o.iibb as iibb,
  	o.inicio_actividad,
  	0.sede_timb,
  	o.numero_estab,
  	o.localidad as localidad_factura,
  	o.punto_venta,
  	o.nombre_organizacion
  	from pagos p  left join comprobantes c on p.idpago = c.pago_id 
  	inner join clientes cl on p.idclientes = cl.idclientes
  	inner join organizaciones o on c.organizacion_id = o.id
  	where pago_id = '$pago_id'");

  $cantidad_clientes = pg_num_rows($sql_pagos);

  $comprobante  = ['Original', 'Duplicado'];

  $pdf = new PDF_i25();
  $pdf->AddPage('P', 'A4');
  $pdf->SetFont('Arial','B',10);
  $idcd = "";

  foreach ($comprobante as $key => $value) {

  	if ($value == 'Duplicado'){
  		$pdf->AddPage('P', 'A4'); 
  		pg_result_seek($sql_pagos, 0);
  	}

  	while ($row = pg_fetch_object($sql_pagos))
  	{


  		if ($idcd != "")
  		{
  			$monto = $row->cd_debe;
  		}else{
  			$monto = $row->importe_pagado;
  		}

  		$count = $count + 1;
  		/*********Parte superior Izquierda ********/
  		$pdf->Rect(10, 10,190,30);
  		$pdf->SetFont('Arial','B',18);
  		$pdf->Text(20,18, $row->nombre_organizacion);
  		$pdf->SetFont('Arial','B',10);
  		$pdf->Text(15,25, $row->direccion_factura); 
  		$pdf->Text(15,30, $row->localidad_factura); 
  		$pdf->Text(15,35, "IVA RESPONSABLE INSCRIPTO"); 

  		/******************************************/
  		/*********Parte superior Derecha********/

  		$pdf->SetFont('Arial','',9);
  		$pdf->Text(155,14, "FACTURA");
  		$pdf->Text(130,18, $value . "         Pagina 1 de 1");
  		$pdf->SetFont('Arial','',12);
  		$pdf->Text(125,24, "N:".$row->punto_venta."-" . str_pad($row->numero_comprobante, 8, "0", STR_PAD_LEFT));
  		$pdf->SetFont('Arial','',9);
  		$pdf->Text(125,28, "Fecha:  " . cambiaf_a_normal($row->fecha_comprobante));
  		$pdf->Text(110,33, "CUIT: ".$row->cuit_factura); 
  		$pdf->Text(150,33, "IIBB: ". $row->iibb); 
  		$pdf->Text(175,33, "Sede Timb: ".$row->sede_timb); 
  		$pdf->Text(110,38, "Inicio de actividad: ".$row->inicio_actividad); 
  		$pdf->Text(158,38, "Nro Estab: ".$row->numero_estab); 


  		/**************Parte supercior Centro **********/

  		$pdf->Rect(97.5, 10,15,14);
  		$pdf->SetFont('Arial','B',20);
  		$pdf->Text(103,18, "B"); /*esto debe ser Variable */
  		$pdf->Line(105, 24,105,40);
  		$pdf->SetFont('Arial','',8);
  		$pdf->Text(100, 22, "COD.06"); /*esto debe ser Variable */

  		$pdf->Rect(10, 40,190,30); 
  		$pdf->SetFont('Arial','B',10);
  		$pdf->Text(15,50, "Nombre y Apellido: "); 
  		$pdf->Text(120,50, "CUIT/CUIL/DNI: "); 
  		$pdf->Text(15,55, "Direccion:"); 
  		$pdf->Text(120,55, "Localidad:"); 
  		$pdf->Text(15,60,"Condicion de Venta:");
  		$pdf->Text(15,65,"Cond. IVA:");
  		$pdf->Text(120,60,"Periodo Facturado:");
  		$pdf->Text(120,65,"Vto Pago:");

  		$pdf->SetFont('Arial','',10);
  		$pdf->Text(50,50 ,substr(($row->apellido . " ".  $row->nombre), 0, 30));
  		$pdf->Text(157,50 ,$row->dni);
  		$pdf->Text(50,55 ,substr(($row->domicilio),0,30));
  		$pdf->Text(157,55 , get_localidad($row->localid)->dpto);
  		$pdf->Text(50,60 ,"Contado");
  		$pdf->Text(50,65,"Consumidor Final");
  		$pdf->Text(157,60 , cambiaf_a_normal($row->periodo_desde). " al ". cambiaf_a_normal($row->periodo_hasta));
  		$pdf->Text(157,65 , cambiaf_a_normal($row->vto_pago));

  		$pdf->Rect(10, 70,190,217); 
  		$pdf->SetFont('Arial','B',10);
  		$pdf->Text(15,75,"Cantidad");
  		$pdf->Text(80,75,"Detalle");
  		$pdf->Text(130,75,"Precio Unit.");
  		$pdf->Text(160,75,"Bonif. ");
  		$pdf->Text(180,75,"Importe");

  		$pdf->SetFont('Arial','',10);
  		$pdf->Text(23,80,"1");
  		$pdf->Text(45,80, "Servicio Informatico");
  		$pdf->Text(135,80, $row->importe_pagado);
  		$pdf->Text(183,80, $row->importe_pagado);

  		$pdf->SetFont('Arial','B',10);
  		$pdf->Rect(10, 200,190,40); 
  		$pdf->Text(150,210,"Subtotal:");
  		$pdf->Text(150,220,"Total:");
  		$pdf->Text(12,230,"Observaciones:");
  		$pdf->SetFont('Arial','',10);
  		$pdf->Text(170,210,$row->importe_pagado);
  		$pdf->Text(170,220,$row->importe_pagado);
  		$pdf->SetY(228);
  		$pdf->SetX(12);
             //$pdf->Multicell(186,10, utf8_decode($pdf->SetFont('Arial','',8).$row->Observacion));
  		$pdf->SetFont('Arial','B',10);
  		$pdf->Text(30,250,"C.A.E. Nro:");
  		$pdf->Text(50,250,$row->cae);


  		$pdf->Text(105,250,"Fecha Vto C.A.E.:");
  		$pdf->Text(140,250, cambiaf_a_normal($row->vto_comprobante));

  		$y = 255;
  		$x = 25;
  		$data_code = $row->dni.'06'.'0003'.$row->cae.str_replace('/','',cambiaf_a_normal($row->vto_comprobante));


  		$dvFE = calculo_digito_verificador_FE($data_code);
  		$pdf->i25($x,$y,$data_code.$dvFE);
  	}
  }
//var_dump(ob_get_contents());
  $pdf->Output();
}

function reporte_ingresos_pdf($fecha_d, $fecha_h){

	$db = Conec_con_pass();
	require('../../fpdf.php');
	setlocale(LC_MONETARY, 'en_AR');
//require('Librerias/fpdf.php');

	$f_desde = cambiaf_a_bd($fecha_d);
	$f_hasta = cambiaf_a_bd($fecha_h);

	$pdf=new FPDF('P');
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',14);

	$pdf->Text(45,12,'DETALLE DE INGRESOS Y EGRESOS',1,0,'C');
	$pdf->SetFont('times','',10);
	$pdf->Text(15,20,'Fechas: '.$f_desde." hasta ".$f_hasta,1,0,'C');
	$pdf->SetFont('times','B',10);
	$pdf->Text(15,30,'DETALLES',1,0,'C');
	$pdf->Text(110,30,'IMPORTES',0,0,'C');
	$pdf->Line(15, 25, 180, 25);
	$pdf->SetX(20);

	$query_ingresos = pg_exec($db, "Select pg_lugar, sum(importe_pagado) as importe from pagos 
		inner join clientes on pagos.idclientes = clientes.idclientes 
		where fecha_ing between '$f_desde' and '$f_hasta' group by pg_lugar order by pg_lugar asc ");


	while($row_ingreso = pg_fetch_object($query_ingresos)){

		$medios = $row_ingreso->pg_lugar;
		$importe_ingreso = $row_ingreso->importe;
		$pdf->SetFont('times','',8);



		if($medios == 1){
			$pdf->Text(15,38,";RAPIPAGO TECHTRON",0,0,'L');
			$pdf->Text(110,38,";".money_format('%i', $importe_ingreso),0,0,'L');
		}

		if($medios == 1){
			$pdf->Text(15,46,";RAPIPAGO PROTEC",0,0,'L');
			$pdf->Text(110,46,";".money_format('%i', $importe_ingreso),0,0,'L');
		}

		if($medios == 2){
			$pdf->Text(15,54,";PAGOFACIL TECHTRON",0,0,'L');
			$pdf->Text(110,54,";".money_format('%i', $importe_ingreso),0,0,'L');   
		}

		if($medios == 2){
			$pdf->Text(15,62,";PAGOFACIL PROTEC",0,0,'L');
			$pdf->Text(110,62,";".money_format('%i', $importe_ingreso),0,0,'L');   
		}

		if($medios == 3){
			$pdf->Text(15,70,";OFICINA",0,0,'L');
			$pdf->Text(110,70,";".money_format('%i', $importe_ingreso),0,0,'L');
		}

		if($medios == 7){
			$pdf->Text(15,78,";PAGOMISCUENTAS",0,0,'L');
			$pdf->Text(110,78,";".money_format('%i', $importe_ingreso),0,0,'L');
		}

		if($medios == 5){
			$pdf->Text(15,86,";TARJETAS CREDITO",0,0,'L');
			$pdf->Text(110,86,";".money_format('%i', $importe_ingreso),0,0,'L');
		}

		if($medios == 6){
			$pdf->Text(15,94,";TARJETAS DEBITO",0,0,'L');
			$pdf->Text(110,94,";".money_format('%i', $importe_ingreso),0,0,'L');
		}


		if($medios == 1){
		}


	}

	
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',14);

	$pdf->Text(45,12,'DETALLE DE INGRESOS Y EGRESOS',1,0,'C');
	$pdf->SetFont('times','',10);
	$pdf->Text(15,20,'Fechas: '.$f_desde." hasta ".$f_hasta,1,0,'C');
	$pdf->SetFont('times','B',10);
	$pdf->Text(15,30,'DETALLES',1,0,'C');
	$pdf->Text(110,30,'IMPORTES',0,0,'C');
	$pdf->Line(15, 25, 180, 25);
	$pdf->SetX(20);
	$pdf->SetX(20);
	$pdf->SetY(10);
	$pdf->Cell(100,50,"DESCUENTOS",0,0,'L');	
	$pdf->SetY(20);
	$pdf->SetFont('Arial','',6);
	$st= pg_exec($db, "Select * from pagos inner join clientes on pagos.idclientes = clientes.idclientes where fecha_ing between '$f_desde' and '$f_hasta' and pg_lugar = 4 order by fecha_ing,pg_lugar asc ");
	while ($row21 = pg_fetch_object($st)){
		$clie = $row21->idclientes;
		$ayn = $row21->apellido.", ".$row21->nombre;
		$import = $row21->importe_pagado;
		$pdf->cell(20,50,";".$clie,0,0,'L');
		$pdf->cell(80,50,";".$ayn,0,0,'L');
		$pdf->cell(50,50,";".$import,0,0,'L');
		//$pdf->Cell(40,50,money_format('%i', $pag),0,0,'L');
		$pdf->Ln(8);
	}
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(40,50,"IMPORTE TOTAL:",0,0,'L');
	$pdf->Cell(20,50,";".money_format('%i', $pag),0,0,'L');

$pdf->AddPage();
	 $string3 = pg_exec($db, "Select * from caja_gerencia where ge_fecha between '$f_desde' and '$f_hasta'  order by ge_fecha ");
                
                    while($row2 = pg_fetch_object($string3))
                                {
                                        $ge_de = $row2->ge_debe + $ge_de;
                                        $ge_ha = $row2->ge_haber + $ge_ha;    
                                        $ge_total = $ge_de - $ge_ha;
                                }

                                $pdf->SetX(20);
                                $pdf->SetFont('Arial','B',8);
                                $pdf->Cell(100,40,"GERENCIA",0,0,'L');
                                    $pdf->Cell(100,40,";".money_format('%i', $ge_total),0,0,'L');
                                    $pdf->Ln(8);
                                
                                
$sql_cu_ga = pg_exec($db, "Select * from cuentas order by idcuentas") ;
$count_ga = pg_num_rows($sql_cu_ga);
                
            for ($i = 0 ; $i<$count_ga ; $i++)                
{    
                $sql_gas = pg_exec($db, "Select * from caja_cuentas inner join cuentas on caja_cuentas.cc_idcuenta = cuentas.idcuentas  where cc_idcuenta = '$i' and cc_fecha between '$f_desde' and '$f_hasta' order by idcuentas");
                $fd_ga = pg_num_rows($sql_gas);
                                
                if ($fd_ga > '0')
                {
                
                $cue_gas = "0";
                 
                while ($row_cc = pg_fetch_object($sql_gas))
                {
                    
                
                        
                        $pdf->SetX(35);
                        $pdf->SetFont('Arial','',6);
                        $pdf->Cell(120,50,";".$row_cc->cc_detalle,0,0,'L');
                        $pdf->Cell(120,50,";".$row_cc->cc_haber,0,0,'L');
                        $pdf->Ln(8);
                    
                    
                     $cue_gas = $cue_gas + $row_cc->cc_haber;
                     $cue_nom = $row_cc->cta_nombre;
                }
                $pdf->Ln(5);
                        $pdf->SetX(20);
                        $pdf->SetFont('Arial','B',8);
                        $pdf->Cell(100,40,";".$cue_nom,0,0,'L');
                        $pdf->Cell(100,40,";".money_format('%i', $cue_gas),0,0,'L');
                        $pdf->Ln(8);
                }
}


$sql_pro = pg_exec($db, "Select * from pagos_promesas where pgp_fecha between '$f_desde' and '$f_hasta' ");

while ($row_p = pg_fetch_object($sql_pro))
{
    $pagos = $pagos + $row_p->pgp_monto;

}

                        $pdf->Ln(5);
                        $pdf->SetX(20);
                        $pdf->SetFont('Arial','B',8);
                        $pdf->Cell(100,40,"IMPORTE PROMESAS: ",0,0,'L');
                        $pdf->Cell(100,40,";".money_format('%i', $pagos),0,0,'L');
                        $pdf->Ln(8);

                        
$sql_us = pg_exec($db, "select sum(cuenta_user.cu_debe) as total, us_apellido, us_nombre from usuario inner join cuenta_user on usuario.idusuario = cuenta_user.idusuario where cu_fecha between '$f_desde' and '$f_hasta'  group by us_apellido, us_nombre");


while ($row_us = pg_fetch_object($sql_us))
{
                        
    $sum_us = $row_us->total + $sum_us;
    
                        $pdf->Ln(5);
                        $pdf->SetX(20);
                        $pdf->SetFont('Arial','',8);
                        $pdf->Cell(100,40,";".$row_us->us_apellido.", ".$row_us->us_nombre ,0,0,'L');
                        $pdf->Cell(100,40,";".money_format('%i', $row_us->total),0,0,'L');
                        $pdf->Ln(8);
}
$pdf->SetFont('Arial','B',8);                        
$pdf->Cell(115,40,"Total Importe Usuario: ".";".money_format('%i', $sum_us),0,0,'L');
                        
                        
                $pdf->SetDrawColor(0,80,180);
                	




	$pdf->Output();
}

?>