<?php

	function set_insertar_descuento_servicio($datos){
		$db= Conec_con_pass();

		$cliente_id = $datos['cliente_id'];
		$fecha = $datos['fecha'];
		$importe = $datos['importe'];
		$num_tarjeta = $datos['num_tarjeta'];
		$detalle = $datos['detalle'];

		pg_query("BEGIN");

		switch ($datos['item']) {
			case 'Descuento':
				$query = $query =pg_exec($db, "INSERT INTO pagos (idpago, idclientes, fecha_ing, importe_pagado,  idnum_tarjeta, detalle, pg_lugar, pg_num_fact, pg_num_tar) 
				values ((select nextval('seq_pagos')), '$cliente_id','$fecha', '$importe', '$num_tarjeta', '$detalle', '4', '0', '0')"); //pg_lugar es 4 por que es descuento
				break;
			case 'Servicio':
				$query = pg_exec($db, "INSERT INTO pagos (idpago, idclientes, fecha_ing, importe_deuda,  idnum_tarjeta, detalle, pagado)
				values ((select nextval('seq_pagos')), '$cliente_id','$fecha', '$importe', '$num_tarjeta', '$detalle', 'true')");
				break;

			case 'Nota Debito':
				$query = pg_exec($db, "INSERT INTO pagos (idpago, idclientes, fecha_ing, importe_deuda,  idnum_tarjeta, detalle, pagado, pg_lugar)
				values ((select nextval('seq_pagos')), '$cliente_id','$fecha', '$importe', '$num_tarjeta', '$detalle', 'true', 13)");
				break;
		}

		if (pg_affected_rows($query) == true)
		{
			pg_query("COMMIT");
			return true;			
		}else{
			pg_query("ROLLBACK");
			return false;
		}
	}

		function cuentas_morosos($page){
			$db= Conec_con_pass();
		
			$row_for_page = $_SESSION['row_for_page'];	
			$select_count = "select count(*) ";
			$select_cliente_morosos = "select clientes.*,  coalesce(sum(p.importe_deuda),0) - coalesce(sum(p.importe_pagado),0)  as deuda_total_cliente, pl.importe ";
			$sql_query= "from clientes inner join instalaciones on clientes.idclientes = instalaciones.idclientes 
															inner join pagos p on clientes.idclientes = p.idclientes
															inner join planes pl on pl.id = instalaciones.plan_id
											where elim_clie = 'f' and importe > 0  and 
											clientes.idclientes not in (select ps_idclientes from promesas where ps_idclientes = clientes.idclientes and ps_cerrada = 'f') and clientes.idclientes not in (select tra_idclientes from tratamientos where tra_cerrada = false)
											group by clientes.idclientes, pl.importe  
											having coalesce(sum(p.importe_deuda),0) - coalesce(sum(p.importe_pagado),0) >= (pl.importe)"	 ;

			$count = pg_exec($db, $select_count . $sql_query);
			$sql_query .= "order by deuda_total_cliente desc";
			$sql_query .=  " limit $row_for_page offset (($page - 1) * $row_for_page )";
			$clientes_morosos = pg_exec($db, $select_cliente_morosos .  $sql_query);

							return [$clientes_morosos, $count];
		}

function guardar_promesa($data){
	
	$cliente_id =  $data['cliente_id'];
	$comentario = $data['comentario'];
	$importe_pago = $data['importe_pago'];
	$fecha_actual = date_server_db();
	$hora_actual = time_completa();
	$usuario_id = $data['usuario_id'];
	$fecha_promesa = $data['fecha_pago'];
	$importe_promesa = $data['importe_pago'];
	$tipo_promesa = $data['tipo_promesa'];
	$db = Conec_con_pass();
	$promesa_activa = pg_exec($db,"Select * from promesas where ps_idclientes = $cliente_id");
	
	pg_query("BEGIN");
	
	if (pg_num_rows($promesa_activa) > '0'){
        //Se actuliza el estado de la promesa, para poder generar una nueva.
        $sql_ps = pg_exec($db, "Update promesas set  ps_cerrada = 'true' where ps_idclientes = $cliente_id");
        $fdev_ps_up = pg_affected_rows($sql_ps);
        if($fdev_ps_up == 0 ){
          	pg_query("ROLLBACK");
           	$mensaje = "Promesa activa no actualizada";
           	return ['false', $mensaje];
        }
    }

    $sql_ps= pg_exec($db, "Insert Into promesas (ps_idclientes, ps_observaciones, ps_fecha, ps_time, ps_user, ps_fec_prome, ps_pago, ps_tratamientos) values('$cliente_id','$comentario','$fecha_actual', '$hora_actual', '$usuario_id', '$fecha_promesa', '$importe_promesa', '0')");
	$fdev_ps = pg_affected_rows($sql_ps); 

	if($fdev_ps == 0){
		pg_query("ROLLBACK");
		$mensaje = "Promesa no Insertada";
        return ['false', $mensaje];
	}

	if($tipo_promesa == 'tratamientos'){
		$comentario_novedad = "Tratamientos - Se compromete a pagar $ " . $importe_promesa ." el dia: ". $fecha_promesa." - ". $comentario ; 
	}else{
		$comentario_novedad = "Se compromete a pagar $ " . $importe_promesa ." el dia: ". $fecha_promesa." - ". $comentario ; 
}
	 $insertar_novedad_diaria = pg_affected_rows(pg_exec($db, "insert into novedades_diarias (nd_fecha, nd_time, idcliente, nd_observacion, nd_solucion, nd_recibido, nd_referido, nd_fechasol, nd_timesol, nd_usersol, nd_esta ) VALUES ('$fecha_actual', '$hora_actual','$cliente_id','$comentario_novedad','COMENTARIO', '$usuario_id', '0','$fecha_actual','$hora_actual','$usuario_id', '1')"));

	 if($insertar_novedad_diaria != 1){
	 	pg_query("ROLLBACK");
			$mensaje = "No se inserto novedad en el usuario";
        	return ['false', $mensaje];
	 }


		$consulta_cliente_tratamientos = consulta_cliente_en_tratamiento($cliente_id);

		if ($consulta_cliente_tratamientos == 'false'){

		$actualizar_estado = pg_exec($db, "update clientes set elim_ser = '2' where idclientes = $cliente_id ");
		$estado_cliente = pg_affected_rows($actualizar_estado);
		if ($estado_cliente != 1 ){
			pg_query("ROLLBACK");
			$mensaje = "No se actualizo el estado del cliente";
        	return ['false', $mensaje];
		}

		$query_server = pg_exec($db, "select idclientes from server where idclientes = $cliente_id");

		if (pg_num_rows($query_server) > 0){
		$actualizar_estado_server = pg_exec($db, "update server set se_esta = '2' where idclientes = $cliente_id ");
		$estado_cliente_server = pg_affected_rows($actualizar_estado_server);
			if ($estado_cliente_server != 1 ){
				pg_query("ROLLBACK");
				$mensaje = "No se actualizo el estado en el servidor";
        		return ['false', $mensaje];
			}
		}
	}
	
	pg_query("COMMIT");
	$mensaje = "Promesa Insertada";
	return['true', $mensaje];
}

function insertar_cliente_tratar($cliente_id, $comentario, $user_id){
	$db = Conec_con_pass();
	$fecha_actual = date_server_db();
	$hora_actual = time_completa();

	$insertar_tratamiento = pg_exec($db, "insert into tratamientos (tra_idclientes, tra_observaciones, tra_time, tra_user, tra_fecha ) values ( '$cliente_id', '$comentario', '$hora_actual', '$user_id',  '$fecha_actual')");
	//pg_query("BEGIN");

	if(pg_affected_rows($insertar_tratamiento) == 0){
		//pg_query("ROLLBACK");
         $mensaje = "tratamientos no insertado";
           	return ['false', $mensaje];
	}else{
		//pg_query("COMMIT");
         $mensaje = "Tratamientos insertado";
           	return ['true', $mensaje];
	}

	return;
}

function cerrar_promesas($cliente_id){
$db = Conec_con_pass();
$promesa_activa = pg_exec($db,"Select * from promesas where ps_idclientes = $cliente_id");
	
	//pg_query("BEGIN");
	
	if (pg_num_rows($promesa_activa) > '0'){
        //Se actuliza el estado de la promesa, para poder generar una nueva.
        $sql_ps = pg_exec($db, "Update promesas set  ps_cerrada = 'true' where ps_idclientes = $cliente_id");
        $fdev_ps_up = pg_affected_rows($sql_ps);
        if($fdev_ps_up == 0 ){
          	///pg_query("ROLLBACK");
           	$mensaje = "Promesa activa no actualizada";
           	return ['false', $mensaje];
        }
        //pg_query("COMMIT");
        $mensaje="Actualizado";
        return['true', $mensaje];
    }

}

function existe_promesa_normal($cliente_id){

	$db = Conec_con_pass();
	$promesa_activa = pg_exec($db,"Select * from promesas p inner join clientes c on p.ps_idclientes = c.idclientes  
									inner join instalaciones i on c.idclientes = i.idclientes
									inner join planes pl on pl.id = i.plan_id
									where p.ps_idclientes = $cliente_id and ps_cerrada = 'false'");

	$datos_clientes = pg_fetch_object($promesa_activa);
	if(pg_num_rows($promesa_activa) > 0){

		$existe_tratamiento = pg_num_rows(pg_exec($db, "select * from tratamientos where tra_idclientes = $cliente_id"));
		if($existe_tratamiento < 1){
			$importe_plan = $datos_clientes->importe;
			$deuda_actual = calcular_deuda($cliente_id);
			if($deuda_actual < ($importe_plan + ($importe_plan / 4) )){
				//pg_query("BEGIN");
				$cambiar_estado = pasar_estado_habilitado($cliente_id);
					if($cambiar_estado[0] == 'false'){
				//	pg_query("ROLLBACK");
					$mensaje = $cambiar_estado[1];
					return['false', $mensaje];
				}
				$cerrar_promesas = cerrar_promesas($cliente_id);
				
				if($cerrar_promesas[0] == 'true'){
				//	pg_query("COMMIT");
					$mensaje = $cerrar_promesas[1];
					return['true', $mensaje];

				}else{
				//	pg_query("ROLLBACK");
					$mensaje = $cerrar_promesas[1];
					return['false', $mensaje];					
				}
			}

		}
	}
	$mensaje = "No existe promesa";
			return['true', $mensaje];
}


?>