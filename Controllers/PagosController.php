<?php

function listar_medios_pagos(){
	$db= Conec_con_pass();
	$medios_pagos  = pg_exec($db, "SELECT * FROM medios_pagos order by nombre asc");
	return $medios_pagos;
}

function listar_archivo_por_tipo($id){
	$db= Conec_con_pass();
	$files = pg_exec($db, "select * from files_upload where medio_pago_id = '$id' order by id desc");
	return [$files];
}

function get_medio_pago($id){
	$db= Conec_con_pass();
	$query = pg_exec($db, "SELECT * FROM medios_pagos where id = $id");
	$medio_pago = pg_fetch_object($query);
	return $medio_pago;	
}

function listar_archivos($page){
	$db= Conec_con_pass();
	$row_for_page = $_SESSION['row_for_page'];
	$sql = "SELECT * FROM files_upload order by id desc " ;
	$count = pg_exec($db, $sql);
	$sql .= " limit $row_for_page offset (($page - 1) * $row_for_page )";
	$archivos = pg_exec($db, $sql);
	return [$archivos, $count];
}

function get_listar_archivo($id){
	$archivo = pg_exec($db, "SELECT * FROM files_upload where id = '$id' order by nombre asc ") ;
//$archivo = pg_fetch_object($sql);
	return $archivo;
}

function get_listar_pagos_cliente($cliente_id){
	$db= Conec_con_pass();

	$total = pg_exec($db, "select sum(case when importe_pagado is null then 0::numeric else importe_pagado end )  - sum(importe_deuda) as total from pagos where idclientes = $cliente_id");

	$sql = "select pagos.*, clientes.nombre as nombre_cliente,clientes.apellido, clientes.idnum_tarjeta, planes.* 
	from pagos inner join clientes on pagos.idclientes = clientes.idclientes 
	inner join instalaciones on instalaciones.idclientes = clientes.idclientes 
	inner join planes on instalaciones.plan_id = planes.id
	where clientes.idclientes = '$cliente_id' order by pagos.fecha_ing desc, idpago desc limit 25"; 
	$pagos = pg_exec($db, $sql);

	return [$total, $pagos];
}

function get_listar_pagos($nombre_archivo){
	#Lista todo los pagos de un archivo de pago electronico
	$db= Conec_con_pass();
	$row_pagos = pg_exec($db, "Select p.idpago, p.importe_pagado, 
		p.fecha_ing, c.estado, c.observacion, c.cae, p.idclientes, c.id 
		from pagos p  left join comprobantes c on p.idpago = c.pago_id  where file_name = '$nombre_archivo'");

	return $row_pagos;
}
function get_comprobante_cliente($pago_id){
	$db= Conec_con_pass();
	$row_pagos = pg_exec($db, "Select p.*, c.estado, c.observacion, c.cae, p.idclientes, c.id, c.organizacion_id as organizacion_comprobante, cl.*
		from pagos p  
		left join comprobantes c on p.idpago = c.pago_id 
		inner join clientes cl on p.idclientes = cl.idclientes
		where idpago = '$pago_id'");

	return $row_pagos;
}

function get_listar_comprobantes($desde = null, $hasta=null, $organizacion_id=null ){
	$db= Conec_con_pass();
	require_once('../../Controllers/AplicacionController.php');	
	$mes_actual = mes_actual();
	$ano_actual = ano_actual();
	$select = "Select c.*, p.idclientes, cl.* 
											from comprobantes c 
											inner join pagos p on c.pago_id = p.idpago
											inner join clientes cl on p.idclientes = cl.idclientes  ";

	$condicion = " where extract(month from c.fecha_comprobante) = $mes_actual and extract(year from c.fecha_comprobante) = $ano_actual" ;							

	if (!empty($desde) && !empty($hasta)){
		$desde = cambiaf_a_bd($desde);
		$hasta = cambiaf_a_bd($hasta);
		$condicion 	= " and c.fecha_comprobante between '$desde' and '$hasta' ";
    }

    if(!empty($organizacion_id)){
    	$condicion += " and $organizacion_id = $organizacion_id";
    }

	$order = "order by c.id desc";
	
	$comprobantes = pg_exec($db, $select . $condicion );
	return $comprobantes;
}

function suma_comprobantes_A_B($desde=null, $hasta=null, $organizacion_id=null){
	$db= Conec_con_pass();
	require_once('../../Controllers/AplicacionController.php');	
	$mes_actual = mes_actual();
	$ano_actual = ano_actual();

	$select = "Select sum(importe) from comprobantes c ";
	$condicion = " where tipo_comprobante in ('A', 'B') ";
	
	
	if (!empty($desde) && !empty($hasta)){
		$desde = cambiaf_a_bd($desde);
		$hasta = cambiaf_a_bd($hasta);
		$condicion .= " and c.fecha_comprobante between '$desde' and '$hasta' ";
    }else{
    	$condicion .= " and  extract(month from c.fecha_comprobante) = $mes_actual and extract(year from c.fecha_comprobante) = $ano_actual "; 
    }

	if(!empty($organizacion_id)){
    	$condicion .= " and organizacion_id = $organizacion_id";
    }

	$comprobantes = pg_exec($db, $select . $condicion);
	return $comprobantes;
}

function suma_NotaCredito_A_B($desde=null, $hasta=null, $organizacion_id=null){
	$db= Conec_con_pass();
	require_once('../../Controllers/AplicacionController.php');	
	$mes_actual = mes_actual();
	$ano_actual = ano_actual();

	$select = "Select sum(importe) from comprobantes c ";
	$condicion = " where tipo_comprobante in ('NotaCreditoA', 'NotaCreditoB')";
	
	if (!empty($desde) && !empty($hasta)){
		$desde = cambiaf_a_bd($desde);
		$hasta = cambiaf_a_bd($hasta);
		$condicion .= " and c.fecha_comprobante between '$desde' and '$hasta' ";
    }else{
    	$condicion .= " and  extract(month from c.fecha_comprobante) = $mes_actual and extract(year from c.fecha_comprobante) = $ano_actual"; 
    }
    if(!empty($organizacion_id)){
    	$condicion .= " and organizacion_id = $organizacion_id";
    }

	$comprobantes = pg_exec($db, $select . $condicion);
	return $comprobantes;

}

function eliminar_pago($pago_id){

	$db= Conec_con_pass();
	$query = pg_exec($db, "delete from pagos where idpago = '$pago_id'");

	if (pg_affected_rows($query)){
		return true;
	}
	return false;
}

function insertar_lotes_pagos($codigo_entidad, $path_file, $prefijo, $nombre_archivo, $medio_pago_id) {
	$db= Conec_con_pass();
	require_once('../../Controllers/AplicacionController.php');

	$estado = '';
	$facae = '';
	$observacion ='';
	$obscode = '';
	$vto_comprobante = '';
	$numero_comprobante =0;
	$resultado = null;
	$lines= file($path_file);
	$total_registros=count($lines);
	$fec_hoy = cambiaf_a_bd(fecha_servidor_total());
	$time =  trim(time_completa());
	$user_sess =trim($_SESSION['iduser']);
//Insertarmos el nombre del archivo que se va a subir

	$query_rapi = pg_exec($db, "INSERT INTO files_upload(nombre, fecha_creacion, medio_pago_id) VALUES ('$nombre_archivo', '$fec_hoy', '$medio_pago_id')");

	for ($i=0 ; $i<$total_registros ; $i++)
	{
		$num_tarj = '';

		if($prefijo != 'co'){
			$prefijo_codigo = $prefijo.'_'.$codigo_entidad;
		}else{
			$prefijo_codigo = $prefijo;
		}

		switch ($prefijo_codigo) {

			case 'tr_txt':
			$datos = explode('|', $lines[$i]);

			$fecha = $datos[12];
			$imp_paga = ($datos[6]/100);
			$num_tarj = $datos[3];
			$detalle = 'Abonado en PagoFacil';
			$medio_pago = 10;
			$total_registros_insertar = $total_registros;
			break;
			case 'RP_3974':
			if($lines[$i] == 0){
				continue;
			}
			$fecha = substr($lines[$i], 0,8);
			$imp_paga = substr($lines[$i], 17,6)/100;
			$num_tarj = substr($lines[$i], 26,10);
			$detalle = 'Abonado en Rapipago';
			$medio_pago = 11;
			if (substr($lines[$i],0,1) == '9'){
				$total_registros_insertar = (int)(substr($lines[$i],8,8));
				
			}	
		
			break;
			case 'RP_646':
			$fecha = substr($lines[$i],0,8) ;
			$imp_paga = substr($lines[$i],17,6)/100;
			$num_tarj =  substr($lines[$i],26,10);
			$detalle = 'Abonado en Rapipago';
			$medio_pago = 1;

			if (substr($lines[$i],0,1) == '9'){
				$total_registros_insertar = (int)(substr($lines[$i],8,8));
			}	
			break;
			case 'PF_900':

			if (substr($lines[$i],0,1) == '5'){
				$fecha = substr($lines[$i],8,8) ;
				$imp_paga = substr($lines[$i],52,6)/100;
				$num_tarj = trim(substr($lines[$i],24,10)) ;
				$detalle = 'Abonado en PagoFacil';
				$medio_pago = 2;
			}
			if (substr($lines[$i],0,1) == '9'){
				$total_registros_insertar  = (int)(substr($lines[$i],16,6));
			}

			break;
			case 'co':
			$fecha = substr($lines[$i],49,8);
			$imp_paga = (string)(int) substr($lines[$i],58,10)/100;
			$num_tarj =  trim(substr($lines[$i],1,10));
			$detalle = 'Abonado en PagoMisCuentas';
			$medio_pago = 7;
			if (substr($lines[$i],0,1) == '9'){
				$total_registros_insertar = (int)(substr($lines[$i],17,6));
			}
			break;
			default:
			break;
		}

		if (is_numeric($num_tarj)){

			if ($fecha != 00000000 && $fecha != 99999999)
			{	
				$sql = pg_exec($db, "select * from clientes where idnum_tarjeta ilike '%$num_tarj' ");

				if (pg_num_rows($sql) > 0){	
					$row =  pg_fetch_object($sql);
					$num_clie = $row->idclientes;
					$num_tarj = $row->idnum_tarjeta;
					$query = pg_exec($db, "INSERT INTO pagos (idclientes, fecha_ing, importe_pagado,  idnum_tarjeta, detalle, pagado, pg_lugar, file_name )values ('$num_clie','$fecha', '$imp_paga', '$num_tarj', '$detalle', 'true','$medio_pago', '$nombre_archivo')");
				//$quit = quitaraviso($num_clie);
					pg_affected_rows($query);
					$sql_prome = pg_exec($db, "select * from promesas where ps_idclientes = '$num_clie' ");
					if (pg_num_rows($sql_prome) > 0){
						$sql_pgp = pg_exec($db, "Insert into pagos_promesas (idpag_prom, pgp_fecha, pgp_time, pgp_monto, pgp_user, pgp_idclientes) values ((select nextval('seq_pag_prom')), '$fec_hoy', '$time', '$imp_paga', '$user_sess', '$num_clie')");
						$fd = pg_affected_rows($sql_pgp);
					}  
				}	
			}
		}
	}
	$update_total_registros = pg_exec($db, "update files_upload set cantidad_registros = '$total_registros_insertar' where nombre = '$nombre_archivo'");
	$pagos = pg_exec($db, "select c.*,p.*, o.nombre as organizacion_nombre, o.id as organizacion_id from pagos p inner join clientes c on p.idclientes = c.idclientes 
		inner join organizaciones o on o.id = c.organizacion_id
		where file_name ='$nombre_archivo'");
	generar_comprobantes($pagos, $codigo_entidad);
	return;
}

function generar_comprobantes($pagos, $codigo_entidad=null){
	$db= Conec_con_pass();
	require_once('../../clases/fe.php');

//if (!function_exists('cambiaf_a_bd')){
	require_once'../../Controllers/AplicacionController.php';
//}
	$count = 0 ;

	while($row = pg_fetch_object($pagos)){

		$estado = '';
		$facae = '';
		$observacion ='';
		$obscode = '';
		$vto_comprobante = '';
		$numero_comprobante =0;
		$resultado = null;
		$fecha_hoy = cambiaf_a_bd(fecha_servidor_total());
		$time =  trim(time_completa());
		$user_id =trim($_SESSION['iduser']);
		$pago_id = $row->idpago;
		$importe_abonado = $row->importe_pagado;
		$detalle = $row->detalle;
		$nombre_organizacion = $row->organizacion_nombre;
		$organizacion_id = $row->organizacion_id;
		$dni = $row->dni;
		
		
		if ($user_id == ''){$user_id = 1;}
		if ($dni != ''){
			if (!existe_comprobante($pago_id)){
				$count + 1; 
				$invoices = [];
				$comprobante = $row->tipo_factura;

				if ($comprobante != 'Prefactura A' && $comprobante != 'Prefactura B' ){

					switch ($comprobante) {
						case 'A':
						$tipo_factura = FEComprobante::FacturaA;
						$invoices = opciones_invoices($comprobante, $row->cuit, $importe_abonado );
						$fa = initial($invoices, $tipo_factura, $nombre_organizacion, $codigo_entidad);	
						$resultado = resultado_FE($fa);
						break;
						case 'B':
						$tipo_factura = FEComprobante::FacturaB;
						$invoices = opciones_invoices($comprobante, $row->dni, $importe_abonado );
						$fa = initial($invoices, $tipo_factura, $nombre_organizacion, $codigo_entidad);	
						$resultado = resultado_FE($fa);
						break;
						case 'C':
						$tipo_factura = FEComprobante::FacturaC;
						$invoices = opciones_invoices($comprobante, $row->dni, $importe_abonado );
						$fa = initial($invoices, $tipo_factura, $nombre_organizacion, $codigo_entidad);	
						$resultado = resultado_FE($fa);
						break;
						default:
						continue;
					}

					if ($resultado != null){
						$estado = $resultado['estado'];
						$facae = $resultado['cae'];
						$observacion =$resultado['observacion'];
						$obscode = $resultado['obscode'];
						$vto_comprobante = $resultado['vto_comprobante'];
						$numero_comprobante =$resultado['numero_comprobante'];
					}
					$periodo_desde = primerDiaMesWithoutParametes();
					$periodo_hasta = ultimoDiaMesWithoutParametes();
					$vto_pago = $fecha_hoy;

					/* Agregar Tipo de comprobantes si es factura A B C y el user_id*/
					$insert_comprobante = pg_exec($db, "INSERT INTO comprobantes (
						pago_id, 
						importe,
						Detalle,
						cae,
						vto_comprobante,
						numero_comprobante,
						estado,
						obscode,
						observacion,
						periodo_desde,
						periodo_hasta,
						vto_pago,
						fecha_comprobante,
						user_id, 
						tipo_comprobante,
						organizacion_id) 
						values(
						'$pago_id',
						'$importe_abonado',
						'$detalle',
						'$facae',
						'$vto_comprobante',
						'$numero_comprobante',
						'$estado',
						'$obscode',
						'$observacion',
						'$periodo_desde',
						'$periodo_hasta',
						'$vto_pago',
						'$fecha_hoy',
						'$user_id',
						'$comprobante',
						'$organizacion_id'
					)" 
				);
				}
			}
		}

	}
	return true;
/*if (pg_affected_rows($insert_comprobante)){
return true;
}else{
return false;
}*/
}

function insertar_nota_credito($pago_id){

	$db= Conec_con_pass();
	require_once('../../clases/fe.php');
	require_once('../../Controllers/AplicacionController.php');
	echo "sss";

	$query_pago = pg_exec($db, "Select co.importe, c.idclientes, co.tipo_comprobante,
		c.cuit, c.dni, co.numero_comprobante, o.nombre as organizacion_nombre, o.id as organizacion_id from pagos p 
		inner join clientes c  on p.idclientes = c.idclientes
		inner join comprobantes co on p.idpago = co.pago_id
		inner join organizaciones o on o.id = c.organizacion_id
		where co.pago_id = '$pago_id'");

	 $fdev1 = pg_num_rows($query_pago);

	if ($fdev1 > 0){

		$row = pg_fetch_object($query_pago);

		$num_clie = $row->idclientes;
		$estado = '';
		$facae = '';
		$observacion ='';
		$obscode = '';
		$vto_comprobante = '';
		$numero_comprobante =0;
		$resultado = null;
		$imp_paga = $row->importe;
		$cliente_id= $row->idclientes;
		$user_id =trim($_SESSION['iduser']);
		$organizacion_nombre = $row->organizacion_nombre;
		$fecha_hoy = cambiaf_a_bd(fecha_servidor_total());
		if ($user_id == ''){$user_id = 1;}

		$invoices = [];
		$comprobante = $row->tipo_comprobante;
		 $organizacion_id = $row->organizacion_id;
		switch ($comprobante) {
			case 'A':
			$tipo_nota = FEComprobante::NotaCreditoA;
			$tipo_nota_texto = 'NotaCreditoA';
			$tipo_comprobante = FEComprobante::FacturaA;
			$invoices = opciones_invoices($comprobante, $row->cuit, $imp_paga );
			$comprobanteAsociado= $row->numero_comprobante;
			$fa = nota_credito($invoices, $tipo_nota, $comprobanteAsociado, $tipo_comprobante, $organizacion_nombre);	
			$resultado = resultado_FE($fa);
			break;
			case 'B':
			$tipo_nota = FEComprobante::NotaCreditoB;
			$tipo_nota_texto = 'NotaCreditoB';
			$tipo_comprobante = FEComprobante::FacturaB;
			$invoices = opciones_invoices($comprobante, $row->dni, $imp_paga );
			$comprobanteAsociado= $row->numero_comprobante;

			$fa = nota_credito($invoices, $tipo_nota, $comprobanteAsociado, $tipo_comprobante, $organizacion_nombre);	
			$resultado = resultado_FE($fa);
			break;
			case 'C':
			$tipo_nota = FEComprobante::NotaCreditoC;
			$tipo_nota_texto = 'NotaCreditoC';
			$tipo_comprobante = FEComprobante::FacturaC;
			$invoices = opciones_invoices($comprobante, $row->dni, $imp_paga );
			$comprobanteAsociado= $row->numero_comprobante;
			$fa = nota_credito($invoices, $tipo_nota, $comprobanteAsociado, $tipo_comprobante, $organizacion_nombre);		
			$resultado = resultado_FE($fa);
			break;
		}

		if ($resultado != null){
			$estado = $resultado['estado'];
			$facae = $resultado['cae'];
			$observacion =$resultado['observacion'];
			$obscode = $resultado['obscode'];
			$vto_comprobante = $resultado['vto_comprobante'];
			$numero_comprobante =$resultado['numero_comprobante'];
			$detalle = "Nota Credito";

			$query = pg_exec($db, "INSERT INTO nota_creditos (pago_id, 
				cliente_id, 
				cae_nota_credito, 
				numero_comprobante, 
				observacion,
				obscode,
				resultado) 
				VALUES($pago_id, $cliente_id, 
				'$facae', $numero_comprobante, '$observacion', '$obscode', '$estado' )");

								$insert_comprobante = pg_exec($db, "INSERT INTO comprobantes (
						pago_id, 
						importe,
						Detalle,
						cae,
						vto_comprobante,
						numero_comprobante,
						estado,
						obscode,
						observacion,
						fecha_comprobante,
						user_id, 
						tipo_comprobante,
						organizacion_id) 
						values(
						'$pago_id',
							'$imp_paga',
						'$detalle',
						'$facae',
						'$vto_comprobante',
						'$numero_comprobante',
						'$estado',
						'$obscode',
						'$observacion',
						'$fecha_hoy',
						'$user_id',
						'$tipo_nota_texto',
						'$organizacion_id'
					)" 
				);
		}
	}	
}

function resultado_FE($fa)	{
	$estado = $fa[0]->resultado;
	$facae = $fa[0]->cae;

	$observacion = $fa[0]->observacion;
	$obscode = $fa[0]->obsCode;
	$numero_comprobante = $fa[0]->numero;
	if ($fa[0]->caeFechaVto != ''){
		$vto_comprobante =$fa[0]->caeFechaVto->format('Y-m-d');
	}else{
		$vto_comprobante = '';
	}
	return["estado"=>$estado, 
	"cae"=>$facae, 
	"observacion" =>$observacion, 
	"obscode"=>$obscode, 
	"numero_comprobante"=>$numero_comprobante, 
	"vto_comprobante"=>$vto_comprobante];
}

function opciones_invoices($comprobante, $cuit_dni, $importe ){
	$invoices = [];

	switch ($comprobante) {
		case 'A':
		$invoices[] = ['cuit_dni' => $cuit_dni,
		'doc_tipo' => FEDocumento::CUIT,
		'importe_total' => $importe];
		break;
		case 'B':
		$invoices[] = ['cuit_dni' => $cuit_dni,
		'doc_tipo' => FEDocumento::DNI,
		'importe_total' => $importe];
		break;
		case 'C':
		$invoices[] = ['cuit_dni' => $cuit_dni,
		'doc_tipo' => FEDocumento::DNI,
		'importe_total' => $importe];
		break;
	}
	return $invoices;
}

function existe_comprobante($pago_id){
	$db= Conec_con_pass();

	$query = pg_exec($db, "Select * from comprobantes where pago_id = '$pago_id'");
	if ( pg_num_rows($query) > 0 ){
		return true;
	}
	return false;
}

function existe_nota_credito_comprobante($pago_id){
	$db= Conec_con_pass();

	$query = pg_exec($db, "Select * from comprobantes where pago_id = '$pago_id' and tipo_comprobante ilike 'NotaCredito%'");
	if ( pg_num_rows($query) > 0 ){
		return true;
	}
	return false;
}


function insertar_items_deuda($fecha, $importe, $detalle, $cliente_id, $num_tarjeta, $costo_instalacion='false'){
	$db= Conec_con_pass();
	$query3 =pg_exec($db, "INSERT INTO pagos (
		fecha_ing, 
		importe_deuda, 
		idclientes,
		idnum_tarjeta, 
		detalle,
		costo_instalacion) 
		VALUES ('$fecha',
		'$importe',
		'$cliente_id',
		'$num_tarjeta',
		'$detalle',
		'$costo_instalacion')");

	if (pg_affected_rows($query3) == 0) {
		return false;
	}
	return true;
}

function insertar_items_bonificacion($fecha, $importe, $detalle, $cliente_id, $num_tarjeta, $costo_instalacion='false'){
	$db= Conec_con_pass();
	$query3 =pg_exec($db, "INSERT INTO pagos (
		fecha_ing, 
		importe_pagado, 
		idclientes,
		idnum_tarjeta, 
		detalle,
		costo_instalacion) 
		VALUES ('$fecha',
		'$importe',
		'$cliente_id',
		'$num_tarjeta',
		'$detalle',
		'$costo_instalacion')");
	if (pg_affected_rows($query3) == 0) {
		return false;
	}
	return true;
}

function insertar_abonos(){

	$usuario = $_SESSION["usuario"];
	$count = 0;
	$conclie = 0;
	$db = Conec_con_pass();
	$dia_actual = dia_actual(); //trae el numero del dia
	$mes_actual = mes_actual();
	$ano_actual = ano_actual();
	$fe_servidor = cambiaf_a_bd(fecha_servidor_total());
	$iduser = $_SESSION['iduser'];
	$tim_user = time_completa();
	$ip_actual = $_SESSION["dir_ip"];

	if ($dia_actual >= '01' and $dia_actual <='10'){
	$fecha_insertar_abono = $ano_actual.'-'.$mes_actual.'-01'; // forma una fecha
	$sql_registro = pg_exec($db,"select * from registros_abono where fecha = '$fecha_insertar_abono' "); //busca si existe un registro con esa fecha
	$count = pg_num_rows($sql_registro);
	if ($count > 0){
		$mensaje =  "El abono ya fue ingresado";
		return [false, $mensaje];
	}
	$ins_pago = pg_exec($db, "INSERT INTO registros_abono (idreg, fecha, detalle, usuario) Values ((select nextval('seq_regabo')), '$fecha_insertar_abono', 'Recargo abono mensual', '$usuario')");

	//consulta todo los clientes
	$sql_clie = @pg_exec($db,"select c.idclientes, p.importe, c.idnum_tarjeta, cu.*, cu.id as cuota_id , pr.porcentaje, pr.cantidad_meses as cantidad_meses_promocion, pr.meses_insertados, pr.finalizado as finalizado_promocion, pr.id as promocion_id
		from clientes c  
		inner join tipo_clientes tc on c.tipo_cliente_id = tc.id 
		inner join instalaciones i on i.idclientes = c.idclientes
		inner join planes p on i.plan_id = p.id
		left join (select * from cuotas where finalizado = 'false') as cu on c.idclientes = cu.cliente_id
		left join (select * from promociones where finalizado = false) as pr on c.idclientes = pr.cliente_id
		where tc.nombre = 'Cliente_activo' and elim_clie = 'false' and elim_ser > '0' and p.importe > 0 
		order by c.idclientes  "); 

	$sfdev = pg_num_rows($sql_clie); // trae la cantidad de clientes para ingresar el abono

	while ($row = pg_fetch_object($sql_clie)) {

		$idcliente = $row->idclientes; //trae el idclientes
		$abono = $row->importe; 
		$num_tarj = $row->idnum_tarjeta; // trae el numero de tarjeta
		$cantidad_cuotas = $row->cantidad_cuota;
		$cantidad_meses = $row->cantidad_meses_promocion;

		$ins_pago = insertar_items_deuda($fecha_insertar_abono, $abono, 'Abono Mensual', $idcliente, $num_tarj, $costo_instalacion='false');
		
		if(!empty($cantidad_cuotas)){
				$importe_cuota = $row->importe_cuota;
				$cuota_abonadas = $row->cuota_abonada;
				$finalizado_cuotas = $row->finalizado;
				$cuota_id = $row->cuota_id;
			
			if($cuota_abonadas < $cantidad_cuotas  && $finalizado_cuotas == 'f' ){
				$numero_cuota_abonar = $cuota_abonadas + 1;
				$detalle_cuota = "Cuota Instalacion ". $numero_cuota_abonar."/".$cantidad_cuotas;
				$insertar_cuotas = insertar_items_deuda($fecha_insertar_abono, $importe_cuota, $detalle_cuota, $idcliente, $num_tarj, $costo_instalacion='true');
				
				if($numero_cuota_abonar == $cantidad_cuotas){
					$finalizado_cuotas = 'true';
				}
				$update_cuotas = pg_exec($db, "update cuotas set cuota_abonada = '$numero_cuota_abonar' , finalizado = '$finalizado_cuotas'where id = $cuota_id ");
			}
		}	

		if(!empty($cantidad_meses)){
			$porcente_promocion = $row->porcentaje;
			$cantidad_meses_insertados = $row->meses_insertados;
			$finalizado_promocion = $row->finalizado_promocion;
			$promocion_id = $row->promocion_id;

			if($cantidad_meses_insertados < $cantidad_meses && $finalizado_promocion == 'f'){

			$importe_promocion = floor(($abono * $porcente_promocion) / 100);
			$detalle_promocion = "Promocion ". $cantidad_meses_insertados."/".$cantidad_meses;
			$numero_promocion_a_insertar = $cantidad_meses_insertados + 1;

			$query_prmocion = insertar_items_bonificacion($fecha_insertar_abono, $importe_promocion, $detalle_promocion, $idcliente, $num_tarj);
			
			if($cantidad_meses == $cantidad_meses_insertados){
				$finalizado_promocion = 'true';
			}
			$update_promocion = pg_exec($db, "update promociones set meses_insertados = '$numero_promocion_a_insertar', finalizado = '$finalizado_promocion' where id = $promocion_id");
		}
		}
	}
}
	else{
		$mensaje = "No corresponde ingresar abono";
		return[false, $mensaje];
	}
}

function calcular_deuda($cliente_id){

		$db= Conec_con_pass();
		$query = pg_exec($db, "select sum(importe_deuda) - sum(importe_pagado) as deuda from pagos where idclientes = $cliente_id");
		$deuda = pg_fetch_object($query)->deuda;

		return $deuda;
}

function calcular_nivel_deuda($cliente_id, $importe_plan, $importe_deuda){

	switch ($importe_deuda) {
		case $importe_deuda > $importe_plan:
			$color = '#FF3F33';
			$mensaje = '(Impago)';
			break;
		case $importe_deuda < $importe_plan && $importe_deuda > 0 :
			$color = '#D4AC0D';
			$mensaje = '(Resto)';
			break;
		case $importe_deuda <= 0:
			$color = '#229954';
			$mensaje = '(S/D)';
			break;
	}
	return[$color, $mensaje];
}


?>