<?php
function mikrotik_gateway($ip_navegacion)
{
	$db = Conec_con_pass();
	$ip_m = explode(".", $ip_navegacion);
	$ip_mkt = $ip_m[0].".". $ip_m[1].".".$ip_m[2]."."."0";
	$sql_mkt = pg_exec($db, "Select * from mikrotiks where ip = '$ip_mkt'");
	$row_server = pg_fetch_object($sql_mkt);
	$mikrotik_id = $row_server->mikrotik_id;

	if ($mikrotik_id != "")
	{ 
		return $mikrotik_id; 
	}else
	{
		return "false";
	}
}


function add_user_mikrotik_navegacion($mikrotik_id,  $graficar = false) 
{
	$db = Conec_con_pass();
//Insertar en mikrotiks
	if ($mikrotik_id == "")
	{
		return;
	}

	$mkt = pg_exec($db, "Select * from mikrotiks where mikrotik_id = '$mikrotik_id'");
	$row = pg_fetch_object($mkt);
	$ipRouteros = trim($row->parent_mkt);
	$API = new routeros_api();

	$debug = $row->debug;
	if ($debug == 'f'){
		$debug = 0;
	}else{
		$debug = 1;
	}

	$API->debug = $debug;
	$api_connect = $API->connect($ipRouteros , $row->users, $row->pass, "8728");

	if ($api_connect){	
		$API->write("/system/package/getall",true);
		$READ = $API->read(false);
		$ARRAY = $API->parse_response($READ);

		$version = $ARRAY[0]["version"];

		$API->write('/queue/simple/print', false);
		$version = (float) $version;
		$API->write('=.proplist=.id');
		$ARRAYS = $API->read();

		$a = count($ARRAYS);

		for ($i = $a  ; $i > -1 ; $i --)
		{
			$API->write("/queue/simple/remove", false);
			$API->write('=.id=' . $ARRAYS[$i]['.id']);
			$READ = $API->read();
		}
		$mikrotik_queue = pg_exec($db , "select se_ip_pc, idnum_tarjeta, velocidad_subida, velocidad_bajada,
			burst_limit_up, burst_limit_down, burst_threshold_up, burst_threshold_down, burst_time_up, burst_time_down
			from 
			mikrotiks m 
			inner join server s on mikrotik_id = s.parent_mkt 
			left join preclientes pr on pr.idpreclientes = s.idclientes
			left join instalaciones i on i.idclientes = s.idclientes
			inner join planes p on p.id IN ( i.plan_id, pr.idplan)
			left join clientes c on c.idclientes = i.idclientes
			where m.parent_mkt = '$row->parent_mkt' and (se_esta > '0' or elim_ser > '0')  and (elim_clie = false or pr_elim = false) 
			order by se_ip_pc asc "
				);

		while($row_mkt = pg_fetch_object($mikrotik_queue)){

			$ip_addr = $row_mkt->se_ip_pc;
			$velocity = $row_mkt->velocidad_subida."k"."/".$row_mkt->velocidad_bajada."k";
			$name = $row_mkt->idnum_tarjeta;
			$graficar = $row_mkt->graficar;

			$burst_limit = $row_mkt->burst_limit_up.'k'."/".$row_mkt->burst_limit_down."k";
			$burst_threshold = $row_mkt->burst_threshold_up.'k'."/".$row_mkt->burst_threshold_down."k";
			$burst_time = $row_mkt->burst_time_up."/".$row_mkt->burst_time_down;

			$type = 'bypassed';
 
			if ($version >= 6 ){
				$API->write("/queue/simple/add", false);
				$API->write('=target='. $ip_addr, false);
							$API->write('=max-limit='. $velocity, false); // 2M/2M [TX/RX]
							$API->write('=burst-limit='. $burst_limit, false); // 2M/2M [TX/RX]
							$API->write('=burst-threshold='. $burst_threshold, false); // 2M/2M [TX/RX]
							$API->write('=burst-time='. $burst_time, false); // 2M/2M [TX/RX]
							$API->write('=name='. $name, true); 
							$READ = $API->read(false);
							$ARRAY = $API->parse_response($READ);
						}else{
							$API->write("/queue/simple/add", false);
							$API->write('=target-addresses='. $ip_addr, false);
							$API->write('=burst-limit='. $burst_limit, false); // 2M/2M [TX/RX]
							$API->write('=burst-threshold='. $burst_threshold, false); // 2M/2M [TX/RX]
							$API->write('=burst-time='. $burst_time, false); // 2M/2M [TX/RX]
							$API->write('=max-limit='. $velocity, false); // 2M/2M [TX/RX]
							$API->write('=name='. $name, true); 
							$READ = $API->read(false);
							$ARRAY = $API->parse_response($READ);
						}
						if ($graficar == 't'){
							$API_1 = new routeros_api();
							$API_1->debug = true;
							if ($API_1->connect($mkt_principal , $Username , $Pass, $api_puerto)){
								$API_1->write("/queue/simple/add", false);
								$API_1->write('=target='. $ip_addr, false);
								$API_1->write('=max-limit='. $velocity, false); // 2M/2M [TX/RX]
								$API_1->write('=burst-limit='. $burst_limit, false); // 2M/2M [TX/RX]
								$API_1->write('=burst-threshold='. $burst_threshold, false); // 2M/2M [TX/RX]
								$API_1->write('=burst-time='. $burst_time, false); // 2M/2M [TX/RX]
								$API_1->write('=name='. $name, true); 
								$READ_1 = $API_1->read(false);
								$ARRAY_1 = $API_1->parse_response($READ_1);
								$API_1->disconnect();
							}
						}
					}
					$mikrotik_customer = pg_exec($db, "Select mikrotik_customers.ip, velocity  from mikrotik_customers inner join mikrotiks on mikrotik_customers.mikrotik_id = mikrotiks.mikrotik_id where mikrotiks.parent_mkt = '$row->parent_mkt'  ");
					while ($row_customer_mkt = pg_fetch_object($mikrotik_customer)){
						$ip_mkt_customer = $row_customer_mkt->ip;
						$velocity_1 = $row_customer_mkt->velocity;
						$name_1 = "Mikrotik - ".$row_customer_mkt->ip;
						if ($version >= 6.00){
							$API->write("/queue/simple/add", false);
							$API->write('=target='. $ip_mkt_customer, false);
						$API->write('=max-limit='. $velocity_1, false); // 2M/2M [TX/RX]
						$API->write('=name='. $name_1, true); 
						$READ = $API->read(false);
						$ARRAY = $API->parse_response($READ);
					}else{
						$API->write("/queue/simple/add", false);
						$API->write('=target-addresses='. $ip_mkt_customer, false);
						$API->write('=max-limit='. $velocity_1, false); // 2M/2M [TX/RX]
						$API->write('=name='. $name_1, true); 
						$READ = $API->read(false);
						$ARRAY = $API->parse_response($READ);
					}	
				}
				$queue_resto = pg_exec($db, "select * from mikrotiks where parent_mkt = '$row->parent_mkt'");
				while ($row_queue = pg_fetch_object($queue_resto)){
					$ip_addr_resto = $row_queue->ip."/24";
					$velocity_resto = "10k/10k";
					$name_resto = "resto"." -".$row_queue->ip;

					if ($version >= 6.00){
						$API->write("/queue/simple/add", false);
						$API->write('=target='. $ip_addr_resto, false);
						$API->write('=max-limit='. $velocity_resto, false); // 2M/2M [TX/RX]
						$API->write('=name='. $name_resto, true); 
						$READ = $API->read(false);
						$ARRAY = $API->parse_response($READ);
			}else {
				$API->write("/queue/simple/add", false);
				$API->write('=target-addresses='. $ip_addr_resto, false);
				$API->write('=max-limit='. $velocity_resto, false); // 2M/2M [TX/RX]
				$API->write('=name='. $name_resto, true); 
				$READ = $API->read(false);
				$ARRAY = $API->parse_response($READ);
			}
		}
		$API->disconnect();
		return true; 	
	}else{
		return false;
	}
}


function cargar_queue_mikrotik_principal(){
	
	//require_once '../Librerias/conexion.php';
	$db = Conectar_db();

	$mikrotik_bordes = pg_exec($db,"Select * from mikrotik_bordes ");
	while ($mikrotik = pg_fetch_object($mikrotik_bordes)) {
		
		$Username="Sistema";
		$Pass="rtl8184";
		$api_puerto = "8728";
		$mkt_principal = $mikrotik->ip;

		$API = new routeros_api();
		//$API->debug = true;
		if ($API->connect($mkt_principal , $Username , $Pass, $api_puerto)) {

			$API->write('/queue/simple/print', false);
			$API->write('=.proplist=.id');
			$ARRAYS = $API->read();
			$a = count($ARRAYS);
			for ($i = 0 ; $i < $a ; $i ++){	
				$API->write("/queue/simple/remove", false);
				$API->write('=.id=' . $ARRAYS[$i]['.id']);
				$READ = $API->read();
			}
			$mikrotik_queue = pg_exec($db , "select se_ip_pc, idnum_tar, velocidad_subida, velocidad_bajada from 
				mikrotiks m 
				inner join server s on mikrotik_id = s.parent_mkt 
				inner join instalaciones i on i.idclientes = s.idclientes
				inner join planes p on p.id = i.plan_id
				inner join clientes c on c.idclientes = i.idclientes
				where m.mikrotik_borde_id = '$mikrotik->id' and se_esta > '0' and elim_clie = false
				");

			while($row_mkt = pg_fetch_object($mikrotik_queue)){
				$ip_addr = $row_mkt->se_ip_pc;
				$velocity = $row_mkt->velocidad_subida."k"."/".$row_mkt->velocidad_bajada."k";
				$name = $row_mkt->idnum_tar;
				$comment = $row_mkt->ip;
				if ($ip_addr != "" and $name !="")
				{
		//echo "$version 6";i
					$API->write("/queue/simple/add", false);
					$API->write('=target='. $ip_addr, false);
			$API->write('=max-limit='. $velocity, false); // 2M/2M [TX/RX]
			$API->write('=name='. $name, false); 
			$API->write('=comment='. $comment, true);	 
			$READ = $API->read(false);
			$ARRAY = $API->parse_response($READ);
		}
	}

	$API->disconnect();
}}}

function listar_mikrotiks_borde(){
	$db = Conec_con_pass();
	$mikrotiks_borde = pg_exec($db, "select * from mikrotik_bordes");

	return $mikrotiks_borde;

}



?>