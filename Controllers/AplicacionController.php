<?php
function curPageName() {
 return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}

function ultimoDiaMes($fecha_instalacion){

	$month = fechaAnoMes($fecha_instalacion);
	$aux = date('Y-m-d', strtotime("{$month} + 1 month"));
	$last_day = date('Y-m-d', strtotime("{$aux} - 1 day"));

return $last_day;

}

function ultimoDia($fecha){
	date_default_timezone_set('America/Argentina/Mendoza');
	$month = fechaAnoMes($fecha);
	$aux = date('Y-m-d', strtotime("{$month} + 1 month"));
	$last_day = date('d',  strtotime("{$aux} - 1 day"));

return $last_day;

}

function primerDiaMes($fecha){
	date_default_timezone_set('America/Argentina/Mendoza');
	$month = fechaAnoMes($fecha);
	$aux = date('Y-m-d', strtotime("{$month} "));
	$first_day = date('Y-m-d', strtotime("{$aux}"));
return $first_day;

}

function dia_actual(){ 

	date_default_timezone_set('America/Argentina/Mendoza');
	return date ("d");
}

function mes_actual(){ 

	date_default_timezone_set('America/Argentina/Mendoza');
	return date ("m");
}

function obtener_mes_fecha($fecha){

	$nuevafecha = strtotime (  $fecha ) ;
	$nuevafecha = date ( 'm' , $nuevafecha );
	return $nuevafecha;
}


function ano_actual(){ 

	date_default_timezone_set('America/Argentina/Mendoza');
	return date ("Y");
}

function ultimoDiaMesWithoutParametes(){

$date = new DateTime('now');
$date->modify('last day of this month');
$last_day = $date->format('Y/m/d');
return $last_day;

}

function primerDiaMesWithoutParametes(){
	date_default_timezone_set('America/Argentina/Mendoza');
	$date = new DateTime('now');
	$date->modify('first day of this month');
	$first_day = $date->format('Y/m/d');
	return $first_day;
}

  function fechaAnoMes($fecha){
  	
	ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
		 $fechaAnoMes=$mifecha[1]."-".$mifecha[2];
		
	return $fechaAnoMes;
} 

function restarFechas($dFecIni, $dFecFin)
{
	
$dFecIni = str_replace("/","-",$dFecIni);

$dFecFin = str_replace("/","-",$dFecFin);

ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $dFecIni, $aFecIni);
ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $dFecFin, $aFecFin);

$date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[3], $aFecIni[1]);
$date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[3], $aFecFin[1]);


return round(($date2 - $date1) / (60 * 60 * 24));
	
}

function diasTranscurridos($inicio, $fin)
{
    $inicio = strtotime($inicio);
    $fin = strtotime($fin);
    $dif = $fin - $inicio;
    $diasFalt = (( ( $dif / 60 ) / 60 ) / 24);
    return ceil($diasFalt);
}

function sumar_array($a, $b){
    return $a *  $b;
}

function primerDiaMesSiguiente($fecha){

$nuevafecha = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-01' , $nuevafecha );
return $nuevafecha;
}

function MesSiguiente($fecha){

$nuevafecha = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'm' , $nuevafecha );
return $nuevafecha;
}

function datetime(){
	date_default_timezone_set('America/Argentina/Mendoza');
	//putenv("TZ=America/Argentina/Mendoza");
	$fecha = new DateTime();
 	$fecha = $fecha->getTimestamp();
	return $fecha;
}

function cambiaFechaYYYYMMDD($fecha){ 

    $fecha = str_replace("/","-",$fecha);
	ereg( "([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 

if (checkdate($mifecha[2], $mifecha[1], $mifecha[3] )){    

	return $lafecha; 
}else{
	echo "La fecha ingresada es incorrecta ";
	return;
}
}

function date_server()
{

	putenv("TZ=America/Argentina/Mendoza");
	$sdate=date("d")."-".date("m")."-".date("Y"); 
	$stime=date("h").":".date("i"); 
	return $sdate;
}

function url(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'] . "/sistema_tracert/";
}

function calculo_digito_verificador_FE($code){

$data = str_split($code);
$datos_impar='';
$datos_par='';

for ($i=0; $i < count($data) ; $i++) { 
 
	if (($i % 2) == 0){
		
		$datos_par = $datos_par + $data[$i];
	}else{
		$datos_impar = $datos_impar + $data[$i];
	}
}

$etapa_3 = $datos_par * 3;
	$etapa_4 = $etapa_3 + $datos_impar;

 (ceil($etapa_4 / 10) * 10 ) ;

	 $etapa_5 = (ceil($etapa_4 / 10) * 10) - $etapa_4;
	return $etapa_5;

}