<?php
include "menu.php";
require_once "Librerias/conn.php";

?>


<html>
    <head>

 <script type="text/javascript">

 function rellenaCombo(formulario)
      {

      	with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci�n.
        {
          var centro = idtipo[idtipo.selectedIndex].text; // Valor seleccionado en el primer combo.
          var n = idsub.length;  // Numero de l�neas del segundo combo.

          idsub.disabled = false;  // Activamos el segundo combo.

          for (var i = 0; i < n; ++i)
            idsub.remove(idsub.options[i]); // Eliminamos todas las l�neas del segundo combo.

          idsub[0] = new Option("Seleccione Subtipo", ''); // Creamos la primera l�nea del segundo combo.

          if (centro != '')  // Si el valor del primer combo es distinto de 'null'.
          {
   <?php
   // CODIGO PHP
    // Para cada centro, construimos el segundo combo con los empleados del mismo.
$db = Conec_con_pass();

    $cons_cen = @pg_exec($db, "SELECT * FROM marcas;");

    for ($l = 0; $l < pg_numrows($cons_cen); ++$l)
    {
     $cen = @pg_fetch_object($cons_cen, $l);
   ?>
            if (centro == '<?php echo $cen->marcas;?>')
            {

    <?php
    // CODIGO PHP
      // Construimos los valores del segundo combo con los empleados del centro.

      $cons_emp = @pg_exec($db, "SELECT  * FROM modelos WHERE idmarcas = ".$cen->idmarcas);

      for ($m = 0; $m < pg_numrows($cons_emp); ++$m)
      {
       $emp = @pg_fetch_object($cons_emp, $m);
    ?>
              idsub[idsub.length] = new Option('<?php echo $emp->modelos;?>','<?php echo $emp->idmodelos;?>' );
    <?php
    // CODIGO PHP
      }
    ?>
            }
   <?php
   // CODIGO PHP
    }
   ?>
            idsub.focus();  // Enviamos el foco al segundo combo.
          }
          else  // El valor del primer combo es 'null'.
          {
            idsub.disabled = true;  // Desactivamos el segundo combo (que estar� vac�o).
            idtipo.focus();  // Enviamos el foco al primer combo.
          }

          idsub.selectedIndex = 0;  // Seleccionamos el primer valor del segundo combo ('null').
        }

      }

</script>

<meta http-equiv="content-type" content="text/xml; charset=utf-8" />

<link rel="stylesheet" type="text/css" media="all" href="skins/aqua/theme.css" title="Aqua" />


<!-- import the calendar script -->
<script type="text/javascript" src="js/calendar.js"></script>

<!-- import the language module -->
<script type="text/javascript" src="lang/calendar-es.js"></script>

<!-- other languages might be available in the lang directory; please check
your distribution archive. -->

<!-- helper script that uses the calendar -->
<script type="text/javascript">

var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}





    </script>
    <script type="text/javascript" src="js/funciones.js"></script>
<link rel="stylesheet" type="text/css" href="CSS/estilo.css">
    </head>
    <body>

        <div class="titulo">

<h1> CONSULTA DE EQUIPOS </h1>
</div>
<hr>

<form name="frmdatos" method="post" action="consultas_equipos.php">
        <table>
                <tr>
                    <td class="nombre_campos">DESDE:</td>
                    <td><input type="text" name="txtf_desde" id="txtf_desde" maxlength="10"  onKeyUp="mascara(this,'/',patron,true);" onBlur="esFechaValida(this);"/><input type="reset" value=" ... " onclick="return showCalendar('txtf_desde', '%d/%m/%Y');" tabindex="1"/> </td>
                        <td class="nombre_campos">HASTA:</td>
			<td><input type="text" name="txtf_hasta" id="txtf_hasta" maxlength="10"  onKeyUp="mascara(this,'/',patron,true);" onBlur="esFechaValida(this);"/><input type="reset" value=" ..." onclick="return showCalendar('txtf_hasta', '%d/%m/%Y');" tabindex="2"/> </td>
		</tr>

                <tr>
                    <td class="nombre_campos" >MAC</td>
                    <td><input type="text" name="txtmac" id="txtmac" maxlength="15" onblur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" tabindex="3" ></td>

            <tr>
                 <td class="nombre_campos">Marcas</td>

                    <td>
		 <select name="idtipo" id="idtipo" onchange=" rellenaCombo('frmdatos');"   tabindex="4" onkeypress="return handleEnter(this, event);" >
              <option value="null" selected>Seleccione Marcas

			<?php
			// CODIGO PHP
			 // Contruimos el primer combo con los valores de la tabla 'centros'.
			 $db = Conec_con_pass();
			 $cons_centros = @pg_exec($db, "SELECT * FROM marcas;");

			 for ($k = 0; $k < pg_numrows($cons_centros); ++$k)
			 {
			  $centro = @pg_fetch_object($cons_centros, $k);


			  echo "               <option value=\"".$centro->idmarcas."\"  >".$centro->marcas."\n";

			  }
			?>
          </select>
           </td>
            </tr>
            <tr>
                <td class="nombre_campos">Modelos</td>

        <td>

            <select name="idsub" id="idsub" tabindex="5" onkeypress="return handleEnter(this, event);" >
            <option value="" disabled >Seleccione Modelos
          </select>

    	 </td>
                </tr>
<tr>
                     <td class="nombre_campos" >Estado:</td>
                     <td><select id="eq_est" name="eq_est" tabindex="6" onkeypress="return handleEnter(this, event);" >
                            <option value="">Seleccione Estado</option>
                             <option value="1">Stock</option>
                            <option value="2">Baja</option>
                            <option value="3">Instalados</option>
                            <option value="4">A Retirar</option>
                            <option value="5">Asignado</option>
                 </select></td>
   
            </tr>

        </table>

            <table>
                <tr>
                    <td><input type="submit" class="boton" value="Consultar" ></td>
                </tr>

            </table>



        </form>
    </body>

</html>