<?php
include("menu.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="content-type" content="text/xml; charset=utf-8" />

<link rel="stylesheet" type="text/css" media="all" href="skins/aqua/theme.css" title="Aqua" />


<!-- import the calendar script -->
<script type="text/javascript" src="js/calendar.js"></script>

<!-- import the language module -->
<script type="text/javascript" src="lang/calendar-es.js"></script>

<!-- other languages might be available in the lang directory; please check
your distribution archive. -->

<!-- helper script that uses the calendar -->
<script type="text/javascript">

var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}

var patron = new Array(2,2,4)
function mascara(d,sep,pat,nums){
if(d.valant != d.value){
val = d.value
largo = val.length
val = val.split(sep)
val2 = ''
for(r=0;r<val.length;r++){
val2 += val[r] 
}
if(nums){
for(z=0;z<val2.length;z++){
if(isNaN(val2.charAt(z))){
letra = new RegExp(val2.charAt(z),"g")
val2 = val2.replace(letra,"")
}
}
}
val = ''
val3 = new Array()
for(s=0; s<pat.length; s++){
val3[s] = val2.substring(0,pat[s])
val2 = val2.substr(pat[s])
}
for(q=0;q<val3.length; q++){
if(q ==0){
val = val3[q]
}
else{
if(val3[q] != ""){
val += sep + val3[q]
}
}
}
d.value = val
d.valant = val
}
}


function esFechaValida(fecha){
    if (fecha != undefined && fecha.value != "" ){
        if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha.value)){
            alert("formato de fecha no v�lido (dd/mm/aaaa)");
            return false;
        }
        var dia  =  parseInt(fecha.value.substring(0,2),10);
        var mes  =  parseInt(fecha.value.substring(3,5),10);
        var anio =  parseInt(fecha.value.substring(6),10);
 
    switch(mes){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8: 
        case 10:
        case 12:
            numDias=31;
            break;
        case 4: case 6: case 9: case 11:
            numDias=30;
            break;
        case 2:
            if (comprobarSiBisisesto(anio)){ numDias=29 }else{ numDias=28};
            break;
        default:
            alert("Fecha introducida err�nea");
            return false;
    }
 
        if (dia>numDias || dia==0){
            alert("Fecha introducida err�nea");
            return false;
        }
        return true;
    }
}
 

function comprobarSiBisisesto(anio){
if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) {
    return true;
    }
else {
    return false;
    }
}

function handleEnter (field, event) {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if (keyCode == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                    break;
            i = field.form.elements[i].tabIndex + 1;
            for( j = 0 ; j < field.form.elements.length; j++){
                if( field.form.elements[j].tabIndex == i){
                    break;
                }
            }
            field.form.elements[j].focus();
            return false;
        }
        else
        return true;
    }    


function consultar ()
{
var f_desde;
var f_hasta;
var zon;
var mes;
var num_mes;
var coape;
var ordenar;
var manual;
var emp;

f_desde = document.all.txtf_desde.value;
f_hasta = document.all.txtf_hasta.value;
ano = document.all.txtano.options[document.all.txtano.selectedIndex].value;
mes = document.all.txtmes.options[document.all.txtmes.selectedIndex].text;
num_mes = document.all.txtmes.options[document.all.txtmes.selectedIndex].value;
coape = document.all.txtcoape.value;
//ordenar = document.all.sel.options[document.all.sel.selectedIndex].value;
manual = document.all.manual.checked;


if (manual == true)
{
	document.location.href = "consulta_cliente_cupones_manu.php?f_desde=" + f_desde + "&f_hasta=" + f_hasta  + "&mes="+ mes  + "&coape=" + coape + "&num_mes="+ num_mes + "&ano=" + ano ;
}
else
{
	document.location.href = "consulta_cliente_cupones.php?f_desde=" + f_desde + "&f_hasta=" + f_hasta  + "&mes="+ mes  + "&coape=" + coape + "&num_mes="+ num_mes + "&ano=" + ano;

}
}

</script>

<style type="text/css">
.ex { font-weight: bold; background: #fed; color: #080 }
.help { color: #080; font-style: italic; }
body { background: #fea; font: 10pt tahoma,verdana,sans-serif; }
table { font: 13px verdana,tahoma,sans-serif; }
a { color: #00f; }
a:visited { color: #00f; }
a:hover { color: #f00; background: #fefaf0; }
a:active { color: #08f; }
.key { border: 1px solid #000; background: #fff; color: #008;
padding: 0px 5px; cursor: default; font-size: 80%; }
<!--
.Estilo3 {font-size: 24px}
-->
</style>
<link rel="stylesheet" type="text/css" href="CSS/estilo.css">
</head>

<body class="fondo">
<h1 class="titulo">IMPRIMIR CUPONES GENERALES</h1> <hr>

<div class="recuadro_consul">
<form name="consul" >
	<table align="center"  width="50%">
		<tr>
			<td class="nombre_campos">DESDE:</td>
			<td><input type="text" size="12" name="txtf_desde" id="txtf_desde" maxlength="10"  onKeyUp="mascara(this,'/',patron,true);" onBlur="esFechaValida(this);"><input type="reset" value=" ... "
			onclick="return showCalendar('txtf_desde', '%d/%m/%Y');">  </td>
			<td class="nombre_campos">HASTA:</td>
			<td><input type="text" size="12" name="txtf_hasta" id="txtf_hasta" maxlength="10" onKeyUp="mascara(this,'/',patron,true);" onBlur="esFechaValida(this);"><input type="reset" value=" ... "
			onclick="return showCalendar('txtf_hasta', '%d/%m/%Y');"></td>
		</tr>
		<tr>
		
   
    <td width="10%" class="nombre_campos">*MESES </td>
    <td>
    		<select   id="txtmes" name="txtmes">
    		<option>Seleccione el mes</option>
    		<option value="01">ENERO</option>
    		<option value="02">FEBRERO</option>
    		<option value="03">MARZO</option>
    		<option value="04">ABRIL</option>
    		<option value="05">MAYO</option>
    		<option value="06">JUNIO</option>
			<option value="07">JULIO</option>
			<option value="08">AGOSTO</option>
			<option value="09">SETIEMBRE</option>
			<option value="10">OCTUBRE</option>
			<option value="11">NOVIEMBRE</option>
			<option value="12">DICIEMBRE</option>	
    		
    		</select>
   </td>
   			
		
    <td width="10%" class="nombre_campos">A&Ntilde;O </td>
    <td>
    		<select   id="txtano" name="txtano">
    		<option>Seleccione el A&Ntilde;O</option>
    		<option value="2010">2010</option>
    		<option value="2011">2011</option>
    		<option value="2012">2012</option>
    		<option value="2013">2013</option>
    		<option value="2014">2014</option>
    		<option value="2015">2015</option>
			<option value="2016">2016</option>
			<option value="2017">2017</option>
			<option value="2018">2018</option>
			<option value="2019">2019</option>
			<option value="2020">2020</option>   		
    		</select>
    </td>
   			</tr>
		
   			
           
		
		<tr>
   			<td class="nombre_campos">Ingrese  Apellido:</td>
      		<td><input type="text" name="txtcoape" id="txtcoape" maxlength="30" onBlur="this.value=this.value.toUpperCase();"  onkeypress="return handleEnter(this, event)"/></td>
   
      		<td>manual:</td>
      		<td><input type="checkbox" value="manual" name="manual" id="manual"></td>
   			</tr>

   			<tr>
			<td height="28" colspan="6" align="center"><input class="boton" type="button" name="consulta" id="consulta" value="consultar" onClick="consultar()">
			</td>
		</tr>
	</table>
</form>
</div>
</body>

</html>