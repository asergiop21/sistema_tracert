<?php
include("menu.php");
require "Librerias/conn.php";

$db = Conec_con_pass();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


 <!-- CHECKTREE v1.0RC by Angus Turnbull http://www.twinhelix.com -->
 <link rel="stylesheet" type="text/css" href="js/checktree/checktree.css" />
 <script type="text/javascript" src="js/checktree/checktree.js"></script>
 <script type="text/javascript"><!--

 // USAGE NOTES: Create a new CheckTree() object like so, and pass it its own name.
 var checkmenu = new CheckTree('checkmenu');
 // You can create several such tree objects, just give each a unique name.

 // One optional property: whether to count all checkboxes beneath the current level,
 // or just to count the checkboxes immediately beneath the current level (the default).
 //checkmenu.countAllLevels = true;

 //--></script>

</head>
<body style="font: 0.8em/1.6 sans-serif; background-color: #FFF">

<form action="javascript:void(0)">

<!--

Once you've created a named tree object, all you have to do is have an unordered list
on the page like this, with ID="..." attributes on some elements. The rules are:

1) Outer <ul> tag must be labelled: ID="tree-OBJECTNAME" where OBJECTNAME is from the script.
2) A <ul> tag with ID="tree-LEVELNAME" will be shown and hidden onclick of...
3) A LI tag with ID="show-LEVELNAME" will toggle the next level's visibility when clicked.
4) Checkboxes that respond to lower levels must be labelled ID="check-LEVELNAME".
5) Any tag with ID="count-LEVELNAME" will show the number of boxes ticked in that level.
6) The last LI tag in each list should have CLASS="last" applied, so it looks good.

Have a look at the example below; it's not too hard I hope :).

-->

<ul id="tree-checkmenu" class="checktree">
 <li id="show-explorer">
  <input id="check-explorer" type="checkbox" />
  Internet Explorer
  <span id="count-explorer" class="count"></span>
  <ul id="tree-explorer">
   <li id="show-iemac">
    <input id="check-iemac" type="checkbox" />
    Macintosh
    <span id="count-iemac" class="count"></span>
    <ul id="tree-iemac">
     <li><input type="checkbox" />v4.0</li>
     <li class="last"><input type="checkbox" />v5.0</li>
    </ul>
   </li>
   <li id="show-iewin" class="last">
    <input id="check-iewin" type="checkbox" />
    Windows
    <span id="count-iewin" class="count"></span>
    <ul id="tree-iewin">
     <li><input type="checkbox" />v4.0</li>
     <li><input type="checkbox" />v5.0</li>
     <li><input type="checkbox" />v5.5</li>
     <li class="last"><input type="checkbox" />v6.0</li>
    </ul>
   </li>
  </ul>
 </li>
 <li id="show-netscape">
  <input id="check-netscape" type="checkbox" />
  Netscape
  <span id="count-netscape" class="count"></span>
  <ul id="tree-netscape">
   <li><input type="checkbox" />v4.0x</li>
   <li><input type="checkbox" />v4.5-v4.7</li>
   <li><input type="checkbox" />v6.x</li>
   <li><input type="checkbox" />v7.x</li>
   <li id="show-mozilla" class="last">
    <input id="check-mozilla" type="checkbox" />
    Mozilla
    <span id="count-mozilla" class="count"></span>
    <ul id="tree-mozilla">
     <li><input type="checkbox" />pre-v1.0</li>
     <li><input type="checkbox" />v1.0</li>
     <li><input type="checkbox" />v1.1+</li>
     <li class="last"><input type="checkbox" />Firebird</li>
    </ul>
   </li>
  </ul>
 </li>
 <li id="show-opera">
  <input id="check-opera" type="checkbox" />
  Opera
  <span id="count-opera" class="count"></span>
  <ul id="tree-opera">
   <li><input type="checkbox" />v5.x</li>
   <li><input type="checkbox" />v6.x</li>
   <li class="last"><input type="checkbox" />v7.x</li>
  </ul>
 </li>
 <li id="show-khtml">
  <input id="check-khtml" type="checkbox" />
  KHTML
  <span id="count-khtml" class="count"></span>
  <ul id="tree-khtml">
   <li><input type="checkbox" />Safari</li>
   <li class="last"><input type="checkbox" />Konqueror</li>
  </ul>
 </li>
 <li class="last"><input type="checkbox" />Omniweb</li>
</ul>

</form>


</body>
</html>