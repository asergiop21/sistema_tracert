<?php
include_once("../../menu.php");
require("../../Librerias/conn.php");
require_once("../../Librerias/Librerias_nuevas.php");
require_once("../../Controllers/LocalidadesController.php");
$db = Conec_con_pass();

$submit = $_POST['submit'];


if ($submit == 'Guardar') {

    $no_nom = $_POST['tr_nom'];
    $no_dir = $_POST['tr_dir'];
    $no_res = $_POST['tr_res'];
    $no_ip =  $_POST['tr_ip'];
    $no_frec = $_POST['tr_frec'];
    $no_loc = $_POST['localidad_id'];
    $no_tel = $_POST['tr_tel'];
    $no_cel = $_POST['tr_cel'];
    $no_cel2 = $_POST['tr_cel2'];
    $user_id = $_SESSION['iduser'];

if ($no_nom <> "" || $no_dir <> "" || $no_tel <> "" )
{
    $sql_no = pg_exec($db, "Insert into 
        nodos(
            nombre,
            ip,
            telefono,
            celular,
            celular2,
            localidad_id,
            responsable,
            frecuencia,
            direccion,
            user_id
            )
    values(
        '$no_nom',
        '$no_ip',
        '$no_tel',
        '$no_cel',
        '$no_cel2',
        '$no_loc',
        '$no_res',
        '$no_frec',
        '$no_dir',
        '$user_id'
        )");

    $f_no = pg_affected_rows($sql_no);

    if ($f_no > 0)
    {
       $mensaje_correcto = 'El registro se dio de alta';
   }
   else
   {
    $mensaje_error = 'El registro no se dio de alta';
}

}else{
  $mensaje_error='Complete los campos con *';
}
}


$localidades  = listar_localidades();
?>

<body>
    <form action="nuevo.php" method="post" name="alt_nod">
<?php if ($mensaje_error != ""){ ?>
  <div class="alert alert-danger">
    <h4><?php echo $mensaje_error; ?></h4>
  </div>
<?php } ?>

<?php if ($mensaje_correcto != ""){ ?>
  <div class="alert alert-success">
    <h4><?php echo $mensaje_correcto; ?></h4>
  </div>
<?php
}
?>
        <h1>NODOS</h1>
        <div class="span12">
            
                <div>
                    <label>Nombre:</label>
                    <input type="text" id="tr_nom" name="tr_nom" >
                </div>    
                <div>
                    <label>Responsable</label>
                    <input type="text" id="tr_res" name="tr_res" >
                </div>
                <div>
                    <label>Direccion</label>
                    <input type="text" id="tr_dir" name="tr_dir" > 
                </div>
                <div>
                  LOCALIDAD:<br>
                  <?php
                  $localidad_id = $row->idlocalidad;
                  echo "<select name='localidad_id' id='localidad_id'>";
                  while ($localidad = pg_fetch_object($localidades)) {
                   if ($localidad_id == $localidad->idlocalidad )
                   {
                    echo "<option value='$localidad->idlocalidad' selected > $localidad->num_loc - $localidad->dpto </option>";
                }
                else 
                {
                   echo "<option value='$localidad->idlocalidad'> $localidad->num_loc - $localidad->dpto </option>";
               }
           }
           echo "</select>";
           ?>
       </div>
   
   
    <div>
        <label>Frecuencia</label>
        <input type="text" id="tr_frec" name="tr_frec" >
    </div>

    <div>
        <label>Ip</label>
        <input type="text" id="tr_ip" name="tr_ip" >
    </div>

    <div>
        <label>Telefono</label></td>
        <input type="text" id="tr_tel" name="tr_tel" >
    </div>

    <div>
        <label>Celular</label></td>
        <input type="text" id="tr_cel" name="tr_cel">
    </div>
    <div>
        <label>Celular 2</label></td>
        <input type="text" id="tr_cel2" name="tr_cel2" >
    </div>

    <input type="submit" value="Guardar" name="submit" class="btn btn-primary" > 

</div>
</form>   
</body>
