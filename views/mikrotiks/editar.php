<?php
include("../../menu.php");
require_once("../../Librerias/conn.php");
$db = Conec_con_pass();

$mkt_id = $_REQUEST['mkt_id'];

if (isset($_POST["editar"])){

	$ip = $_POST["ip"];
	$ip_original = $_POST["ip_original"];
	$parent = $_POST["parent_mkt"];
	$search_mkt = pg_exec($db, "select * from mikrotiks where ip = '$ip'");
	$mikrotik_borde_id = $_POST["mikrotik_borde_id"];
	$user_mkt = $_POST["user_mkt"];
	$password = $_POST['password'];
	$confirmar_password = $_POST['confirmar_password'];

	if ($password == "" ){

		$sql_check_pass = pg_exec($db, "Select pass from mikrotiks where mikrotik_id= '$mkt_id'");
		$password = pg_fetch_object($sql_check_pass)->pass;

		if ($password == "" ){
			$mensaje_error = "Ingrese password";
			goto end;
		}
	}else{
		if ($password != $confirmar_password){
			$mensaje_error = "La confirmacion del password no coincide con el password";
			goto end;
		}
	}

	if (pg_num_rows($search_mkt) > 0 && $ip_original != $ip ){
		$mensaje_error = "No Actualizado, El rango de ip ya existe";
	}else{
		pg_query("BEGIN");

		$mkt_edit = pg_exec($db, "update mikrotiks set ip= '$ip', parent_mkt= '$parent', mikrotik_borde_id = $mikrotik_borde_id, users = '$user_mkt', pass = '$password'  where mikrotik_id= '$mkt_id'");

		if (pg_affected_rows($mkt_edit) > '0'){
			pg_query("COMMIT");
			$mensaje_correcto = "Actualizado";
		}
		else{
			pg_query("ROLLBACK");
			$mensaje_error = "No Actualizado";
		}
	}
}
end:	
$mikrotiks = pg_exec($db, "Select * from mikrotiks where mikrotik_id = '$mkt_id'");
$mikrotik_bordes = pg_exec($db, "Select * from mikrotik_bordes");

?>

<form action="editar.php" method="post">
	<?php if ($mensaje_error != ""){ ?>
	<div class="alert alert-danger">
		<h4><?php echo $mensaje_error; ?></h4>
	</div>
	<?php } ?>

	<?php if ($mensaje_correcto != ""){ ?>
	<div class="alert alert-success">
		<h4><?php echo $mensaje_correcto; ?></h4>
	</div>
	<?php
}
?>


<div class="span8" >
	<div class="row-fluid">
		<h3>Editar Rango de Mikrotik</h3>

		<div class=""></div>

		<?php

		while($row_mkt = pg_fetch_object($mikrotiks))
		{
			?>
			<div>
				Rango de ip: <br>
				<input type ="text" value="<?php echo $row_mkt->ip?>" id="ip" name="ip" >
				<input type ="hidden" value="<?php echo $row_mkt->ip?>" id="ip_original" name="ip_original" >
				<input type ="hidden" value="<?php echo $row_mkt->mikrotik_id?>" id="mkt_id" name="mkt_id" >
			</div>

			<div>
				Mikrotik Parent: <br>
				<input type ="text" value="<?php echo $row_mkt->parent_mkt?>" id="parent_mkt" name="parent_mkt" >
			</div>
			<div>
				Usuario: <br>
				<input type ="text" value="<?php echo $row_mkt->users?>" id="user_mkt" name="user_mkt" >
			</div>
			<div>
				Contraseña: <br>
				<input type ="password" value="" id="password" name="password" >
			</div>
			<div>
				Confirmar Contraseña <br>
				<input type ="password" value="" id="confirmar_password" name="confirmar_password" >

			</div>
			<div>
				Mikrotik Borde: <br>

				<?php
				$mikrotik_borde_id = $row_mkt->mikrotik_borde_id;
				echo "<select name='mikrotik_borde_id' id='mikrotik_borde_id'>";

				while ($mikrotik_borde = pg_fetch_object($mikrotik_bordes)) {
					if ($mikrotik_borde_id == $mikrotik_borde->id){
						echo "<option value='$mikrotik_borde->id' selected> $mikrotik_borde->nombre </option>";
					}else
					{
						echo "<option value='$mikrotik_borde->id'> $mikrotik_borde->nombre </option>";
					}
				}
				echo "</select>";
				?>
			</div>

			<?php
		}
		?>
		<input type="submit" value="Editar" class="btn btn-primary" name="editar" value="editar"> 

	</div>
</div>
</form>