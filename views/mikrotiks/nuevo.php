<?php
include("../../menu.php");
require_once("../../Librerias/conn.php");
$db = Conec_con_pass();

$mkt_id = $_REQUEST['mkt_id'];

if (isset($_POST['nuevo'])){
	$ip = $_POST["ip"];
	$parent = $_POST["parent_mkt"];
	$mikrotik_borde_id = $_POST["mikrotik_borde_id"];
	$user_mkt = $_POST["user_mkt"];
	$password = $_POST['password'];
	$confirmar_password = $_POST['confirmar_password'];
	
	$search_mkt = pg_exec($db, "select * from mikrotiks where ip = '$ip'");
	if ($password == "" ){
			$mensaje_error = "Ingrese password";
			goto end;
		}
	else{
		if ($password != $confirmar_password){
			$mensaje_error = "La confirmacion del password no coincide con el password";
			goto end;
		}
	}

	if (pg_num_rows($search_mkt) > 0){
		echo "<div class ='alert-danger'>No Insertado, El rango de ip ya existe </div>";
	}else
	{
		pg_query("BEGIN");
		$mkt_new = pg_exec($db, "Insert Into mikrotiks (ip, parent_mkt, mikrotIk_borde_id, users, pass) values ('$ip', '$parent', '$mikrotik_borde_id', '$user_mkt', '$password')");

		if (pg_affected_rows($mkt_new) > '0')
		{
			pg_query("COMMIT");
			$msg = urlencode("Registro Insertado..");
			$class = "alert-success";
			echo "<meta http-equiv=refresh content=0;URL=".BASE_URL ."views/mikrotiks/index.php?msg=$msg&class=$class>"; 

		}else
		{
			pg_query("ROLLBACK");
			$msg = urlencode("El registro no se pudo dar de alta");
			$class = "alert-danger";
			echo "<meta http-equiv=refresh content=0;URL=".BASE_URL ."views/mikrotiks/index.php?msg=$msg&class=$class>"; 
			
		}}
	}
end:
	$mikrotik_bordes = pg_exec($db, "Select * from mikrotik_bordes");
	?>

	<form action="nuevo.php" method="post">
	<?php if ($mensaje_error != ""){ ?>
	<div class="alert alert-danger">
		<h4><?php echo $mensaje_error; ?></h4>
	</div>
	<?php } ?>

	<?php if ($mensaje_correcto != ""){ ?>
	<div class="alert alert-success">
		<h4><?php echo $mensaje_correcto; ?></h4>
	</div>
	<?php
}

?>

		<div class="span8" >
			<div class="row-fluid">
				<h3>Nuevo Rango de Mikrotik</h3>
				<div>
					Rango de ip: <br>
					<input type ="text" value="<?php if (isset($ip)){echo $ip; }?>" id="ip" name="ip" >
				</div>

				<div>
					Mikrotik Parent: <br>
					<input type ="text" value="<?php if (isset($parent)){ echo $parent;} ?>" id="parent_mkt" name="parent_mkt" >
				</div>
				<div>
					Usuario: <br>
					<input type ="text" value="<?php if (isset($user_mkt)){echo $user_mkt; } ?>" id="user_mkt" name="user_mkt" >
				</div>
				<div>
					Contraseña: <br>
					<input type ="password" value="" id="password" name="password" >
				</div>
				<div>
					Confirmar Contraseña <br>
					<input type ="password" value="" id="confirmar_password" name="confirmar_password" >

				</div>
				<div>
					Mikrotik Borde: <br>
					<?php
					echo "<select name='mikrotik_borde_id' id='mikrotik_borde_id'>";
					while ($mikrotik_borde = pg_fetch_object($mikrotik_bordes)) {

						echo "<option value='$mikrotik_borde->id'> $mikrotik_borde->nombre </option>";
					}
					echo "</select>";
					?>
				</div>
				<input type="submit" value="nuevo" class="btn btn-primary" name="nuevo" id="nuevo"> 

			</div>
		</div>
	</form>
	