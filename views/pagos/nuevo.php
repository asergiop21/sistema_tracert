<?php
include("../../menu.php");
require_once("../../Librerias/conn.php");
require("../../Controllers/PagosController.php");

$db = Conec_con_pass();

if (isset($_POST['guardar'] ))
{

  $medio_pago_id = $_POST["medio_pago_id"];
  $nombre_archivo = $_FILES["file"]["name"];
  $prefijo_file = substr($nombre_archivo, 0,2);
  $codigo_entidad = explode('.',$nombre_archivo)[1];

  if ($prefijo_file != get_medio_pago($medio_pago_id)->prefijo){
      $mensaje_error = 'El medio de pago no coincide con el archivo seleccionado';
  }else
  {

  if ($_FILES["file"]["error"] > 0)
  {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
  }
  else
  {
    move_uploaded_file($_FILES["file"]["tmp_name"],
      "../../files_upload/" . $_FILES["file"]["name"]);
  }

  $query = pg_exec($db,"select * from files_upload where nombre = '$nombre_archivo'");
  $archivo = pg_num_rows($query);

  if ($archivo > 0) {
    $mensaje_error = 'El archivo ya esta dado de alta';
  }
  else 
  {
    $path_file = BASE_URL.'files_upload/'.$nombre_archivo;
    insertar_lotes_pagos($codigo_entidad, $path_file, $prefijo_file, $nombre_archivo,$medio_pago_id);
  }
}
}
$medios_pagos = listar_medios_pagos();  
?>
<script>

function valida(F)
{   
 if((F.file.value) != "" ) 
 {   
  return true;
}
else
{
 alert("Seleccione un archivo.");
 return false;
}
} 

</script>
<body >
  <h2 class="titulo">Pagos Electronicos </h2>
  <hr>

  <?php if ($mensaje_error != ""){ ?>
  <div class="alert alert-danger">
    <h4><?php echo $mensaje_error; ?></h4>
  </div>
<?php } ?>

<?php if ($mensaje_correcto != ""){ ?>
  <div class="alert alert-success">
    <h4><?php echo $mensaje_correcto; ?></h4>
  </div>
<?php
}
?>
  <div class="recuadro_consul" align="center">
    <form id="formulario"  method="post" action="#" onSubmit="return valida(this);" enctype="multipart/form-data">
      <select name='medio_pago_id' id='medio_pago_id' tabindex='6' class = 'required span6'>
        <?php
        echo " <option value=''> Seleccion un Medio de Pago </option>";
        while ($medio_pago = pg_fetch_object($medios_pagos)) {
          echo " <option value='$medio_pago->id' id='$medio_pago->id' > $medio_pago->nombre </option>";
        }
        ?>
      </select>
      <label for="file" >Nombre de Archivo:</label>
      <input  type="file" name="file" id="file"   > 
      <br><br>
      <input type="submit"  name="guardar" class="btn btn-primary"  />
    </form>
  </div>
