<?php
require ('../../Librerias/conn.php');
require_once('../../Librerias/pdf.php');
require_once("../../Controllers/LocalidadesController.php");
require_once("../../Controllers/AplicacionController.php");

require('../../i25.php');


$db = Conec_con_pass();
$pago_id = $_REQUEST['pago_id'];//373436;

$count = 0;

      $query_validar = pg_exec($db, "Select * from comprobantes where pago_id = '$pago_id'");

            if ( pg_num_rows($query_validar) < 1 ){
                 $sql_pago=  pg_exec($db, "Select o.nombre as organizacion_nombre, o.id as organizacion_id, * 
                                          from pagos p inner join clientes c on p.idclientes = c.idclientes 
                                                      inner join organizaciones o on c.organizacion_id = o.id
                                                      where p.idpago = '$pago_id'");
                 $validacion_comprobante = pg_num_rows($pago);
                 if ($validacion_comprobante < 1){
                  echo "algo Salio mal";
                  die;
                 }
            }
         
      $sql_pagos  = pg_exec($db, "Select p.importe_pagado,c.*, p.idclientes, c.id, cl.*
                   from pagos p  left join comprobantes c on p.idpago = c.pago_id 
                   inner join clientes cl on p.idclientes = cl.idclientes
                   where pago_id = '$pago_id'");

      $cantidad_clientes = pg_num_rows($sql_pagos);

$comprobante  = ['Original', 'Duplicado'];

$pdf = new PDF_i25();
$pdf->AddPage('P', 'A4');
$pdf->SetFont('Arial','B',10);
$idcd = "";


foreach ($comprobante as $key => $value) {
 
  if ($value == 'Duplicado') 

            {
                  $pdf->AddPage('P', 'A4'); 
                  pg_result_seek($sql_pagos, 0);
            }


while ($row = pg_fetch_object($sql_pagos))
{


            if ($idcd != "")
            {
                   $monto = $row->cd_debe;
            }
            else
            {
                  $monto = $row->importe_pagado;
            
            }
          
           $count = $count + 1;
            /*********Parte superior Izquierda ********/
            $pdf->Rect(10, 10,190,30);
            $pdf->SetFont('Arial','B',18);
   	      $pdf->Text(20,18, "Techtron Argentina S.A.");
            $pdf->SetFont('Arial','B',10);
            $pdf->Text(15,25, "25 de mayo 485"); 
            $pdf->Text(15,30, "San Martin - Mendoza"); 
            $pdf->Text(15,35, "IVA RESPONSABLE INSCRIPTO"); 

            /******************************************/
            /*********Parte superior Derecha********/
            
            $pdf->SetFont('Arial','',9);
            $pdf->Text(155,14, "FACTURA");
            $pdf->Text(130,18, $value . "         Pagina 1 de 1");
            $pdf->SetFont('Arial','',12);
            $pdf->Text(125,24, "N: 0003-" . str_pad($row->numero_comprobante, 8, "0", STR_PAD_LEFT));
            $pdf->SetFont('Arial','',9);
            $pdf->Text(125,28, "Fecha:  " . cambiaf_a_normal($row->fecha_comprobante));
            $pdf->Text(110,33, "CUIT: 30-71118816-5"); 
            $pdf->Text(150,33, "IIBB: 615012"); 
            $pdf->Text(175,33, "Sede Timb: 08"); 
            $pdf->Text(110,38, "Inicio de actividad: 18/09/2009"); 
            $pdf->Text(158,38, "Nro Estab: 08-0615012-00"); 
		

            /**************Parte supercior Centro **********/

            $pdf->Rect(97.5, 10,15,14);
            $pdf->SetFont('Arial','B',20);
            $pdf->Text(103,18, "B"); /*esto debe ser Variable */
            $pdf->Line(105, 24,105,40);
            $pdf->SetFont('Arial','',8);
            $pdf->Text(100, 22, "COD.06"); /*esto debe ser Variable */
            
            $pdf->Rect(10, 40,190,30); 
            $pdf->SetFont('Arial','B',10);
            $pdf->Text(15,50, "Nombre y Apellido: "); 
            $pdf->Text(120,50, "CUIT/CUIL/DNI: "); 
            $pdf->Text(15,55, "Direccion:"); 
            $pdf->Text(120,55, "Localidad:"); 
            $pdf->Text(15,60,"Condicion de Venta:");
            $pdf->Text(15,65,"Cond. IVA:");
            $pdf->Text(120,60,"Periodo Facturado:");
            $pdf->Text(120,65,"Vto Pago:");

            $pdf->SetFont('Arial','',10);
            $pdf->Text(50,50 ,substr(($row->apellido . " ".  $row->nombre), 0, 30));
            $pdf->Text(157,50 ,$row->dni);
            $pdf->Text(50,55 ,substr(($row->domicilio),0,30));
            $pdf->Text(157,55 , get_localidad($row->localid)->dpto);
            $pdf->Text(50,60 ,"Contado");
            $pdf->Text(50,65,"Consumidor Final");
            $pdf->Text(157,60 , cambiaf_a_normal($row->periodo_desde). " al ". cambiaf_a_normal($row->periodo_hasta));
            $pdf->Text(157,65 , cambiaf_a_normal($row->vto_pago));

            $pdf->Rect(10, 70,190,217); 
            $pdf->SetFont('Arial','B',10);
            $pdf->Text(15,75,"Cantidad");
            $pdf->Text(80,75,"Detalle");
            $pdf->Text(130,75,"Precio Unit.");
            $pdf->Text(160,75,"Bonif. ");
            $pdf->Text(180,75,"Importe");

            $pdf->SetFont('Arial','',10);
             $pdf->Text(23,80,"1");
             $pdf->Text(45,80, "Servicio Informatico");
             $pdf->Text(135,80, $row->importe_pagado);
             $pdf->Text(183,80, $row->importe_pagado);

             $pdf->SetFont('Arial','B',10);
             $pdf->Rect(10, 200,190,40); 
             $pdf->Text(150,210,"Subtotal:");
             $pdf->Text(150,220,"Total:");
             $pdf->Text(12,230,"Observaciones:");
              $pdf->SetFont('Arial','',10);
             $pdf->Text(170,210,$row->importe_pagado);
             $pdf->Text(170,220,$row->importe_pagado);
             $pdf->SetY(228);
             $pdf->SetX(12);
             //$pdf->Multicell(186,10, utf8_decode($pdf->SetFont('Arial','',8).$row->Observacion));
                          $pdf->SetFont('Arial','B',10);
             $pdf->Text(30,250,"C.A.E. Nro:");
             $pdf->Text(50,250,$row->cae);


             $pdf->Text(105,250,"Fecha Vto C.A.E.:");
             $pdf->Text(140,250, cambiaf_a_normal($row->vto_comprobante));

            $y = 255;
            $x = 25;
            $data_code = $row->dni.'06'.'0003'.$row->cae.str_replace('/','',cambiaf_a_normal($row->vto_comprobante));


            $dvFE = calculo_digito_verificador_FE($data_code);
            $pdf->i25($x,$y,$data_code.$dvFE);
          
}
}

$pdf->Output();

?>
