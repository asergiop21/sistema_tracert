<?php
require_once("../../Librerias/conn.php");
require("../../Controllers/ClientesController.php");
require_once('../../Controllers/ReportesController.php');

    $mensaje_error='';
    $mensaje_correcto='';

    if (isset($_REQUEST["imprimir"])){
        $mes = date('m');
        $ano = date('Y');
        $a = generar_comprobantes_pdf($_REQUEST['cliente_id'],$mes, $ano);
    }

if (isset($_POST['habilitar'])){
  $cliente_id = $_POST['habilitar'];
    $cliente_habilitar = habilitar_precliente($cliente_id);
    if ($cliente_habilitar[0] == 'true'){
      $mensaje_correcto =$cliente_habilitar[1];
    }else{
      $mensaje_error =$cliente_habilitar[1];
    }
}

if (isset($_POST['eliminar'])){
  $cliente_id = $_POST['eliminar'];
  $eliminarClientePre = eliminar_cliente_pre($cliente_id);
  if ($eliminarClientePre[0] == 'true'){
    $mensaje_correcto =$eliminarClientePre[1];
  }else{
    $mensaje_error =$eliminarClientePre[1];
  }
}

include_once("../../menu.php");
$db = Conec_con_pass();
$clientes = pg_exec($db, "Select  c.idclientes as cliente_id_ , * from clientes c inner join 
							instalaciones i on c.idclientes = i.idclientes 
							inner join localidad l on c.localid = l.idlocalidad
							left join server s on c.idclientes = s.idclientes
							left join (select  cliente_id, equipo_id from clientes_equipos limit 1) ce on c.idclientes = ce.cliente_id
							left join equipos e on ce.equipo_id = e.idequipos
							where instalado = false and tipo_cliente_id = 2 and elim_clie = 'false' ");
 $cont = pg_num_rows($clientes);
?>

<form name="cliente_instalar" id="cliente_instalar" method="POST" '>

        <?php if ($mensaje_error != ""){ ?>
  <div class="alert alert-danger">
    <h4><?php echo $mensaje_error; ?></h4>
  </div>
<?php } ?>

<?php if ($mensaje_correcto != ""){ ?>
  <div class="alert alert-success">
    <h4><?php echo $mensaje_correcto; ?></h4>
  </div>
<?php
}
?>
<table id="tlbnroins" align="center" name = "tlbnroins" class="table" >
    <tr>
				<th>Selec</th>
                <th>Equipos</th>
                <th>Imprimir</th>
                <th>N° Clientes </th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Localidad</th>
                <th>Ip Antena</th>
                <th>Ip Pc</th>
                <th>Habilitar</th>
                <th>Eliminar</th>
	  </tr>
<?php

        while ($row = pg_fetch_object($clientes))
            {
               ?>
               <td></td>
               <td><?php if ($row->equipo_id != ''){ ?><a href="../clientes/asignar_equipos.php?cliente_id=<?php echo $row->cliente_id_; ?>"> Equipos Asignado </a>   <?php } ?></td>
               <td><a href="?cliente_id=<?php echo $row->idclientes?>&imprimir=true;">Cupon</a></td>
               <td><a href="orden_instalacion.php?cliente_id=<?php echo $row->cliente_id_ ?>">Orden</a></td>
                <td><?php echo $row->cliente_id_; ?></td>
                <td><?php   date_default_timezone_set('America/Argentina/Mendoza');          echo date('d/m/Y', strtotime($row->created_at));?></td>
                <td><a href="../clientes/editar.php?cliente_id=<?php echo $row->cliente_id_; ?>"><?php echo $row->apellido.", " .$row->nombre; ?></a></td>
                <td><?php echo $row->num_loc." / ". $row->dpto; ?></td>
                <td><?php echo $row->se_ip_ant; ?></td>
                <td><?php echo $row->se_ip_pc; ?></td>
                <td><button type="submit" value="<?php echo $row->cliente_id_ ?>" id="<?php echo $row->cliente_id_ ?>" name="habilitar" onclick="return confirm('Desea Habilitarlo?')">Habilitar</button></td>
                <td><button type="submit" value="<?php echo $row->cliente_id_ ?>" id="<?php echo $row->cliente_id_ ?>" name="eliminar" class="btn btn-danger"onclick="return confirm('Desea Eliminarlo?')">Eliminar</button></td>
	     </tr>
<?php
 		}
 		?>	  
  </table>
 <h3>Cantidad de Clientes Encontrados: <?php echo $cont; ?></h3>
</form>