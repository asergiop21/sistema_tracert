<?php

include("../../menu.php");
require("../../Librerias/conn.php");
require("../../Controllers/LocalidadesController.php");
require("../../Controllers/PlanesController.php");
require("../../Controllers/MikrotiksController.php");
$db = Conec_con_pass();

$cliente_id = $_REQUEST["cliente_id"];

 if (isset($_POST['guardar'] ))
    {

      $nombre = $_POST["txtnom"];
      $apellido = $_POST["txtape"];
      $domicilio = $_POST["txtdom"];
      $telefono = $_POST["txttel"];
      $celular = $_POST["txtcel"];
      $celular1 = $_POST["txtcel1"];
      $email = $_POST["txtmail"];
      $localidad_id = $_POST["txtlocal"];
      $observacion = $_POST["txtobs"];
      $barrio = $_POST["txtbar"];
      $referencia = $_POST["txtent"];
      $plan_id = $_POST["txtplan"];

      $iduser = $_SESSION['iduser'];
      $ip_actual = $_SESSION["dir_ip"];
      $ip_equipo = $_POST['ip_equipo'];
      $ip_navegacion = $_POST['ip_navegacion'];
  
      if (!empty($nombre) && !empty($apellido) && !empty($domicilio) && !empty($localidad_id) && !empty($plan_id))
      {
        pg_query("BEGIN");
        $query = pg_exec($db, "UPDATE preclientes  SET nombre = '$nombre', apellido= '$apellido', domicilio = '$domicilio', tel_fijo = '$telefono', celular = '$celular', cel1 = '$celular1', email = '$email', observacion='$observacion', localid = '$localidad_id', barrio = '$barrio', entre='$referencia', idplan = '$plan_id' where idpreclientes = $cliente_id");  
       
          if (!empty($ip_equipo) && !empty($ip_navegacion))
          {
              $query_cliente_server = pg_exec($db, "select * from server where idclientes = $cliente_id "); 
              $query_ip = pg_exec($db,"select * from server where se_ip_pc IN ('$ip_equipo', '$ip_navegacion') and idclientes != $cliente_id ");
              $mikrotik_gateway = mikrotik_gateway($ip_navegacion);

              if ($mikrotik_gateway == 'false')
              {
                $mensaje_suberror = "El Rango de ip no existe en sistema";
              }else{
                 $mikrotik_gateway;
              if (pg_num_rows($query_ip) == 0  ){
                if (pg_num_rows($query_cliente_server) == 0){
                  $data_server = pg_exec($db, "Insert into server (idclientes, se_ip_pc, se_ip_ant, parent_mkt, idnum_tar ) values ('$cliente_id', '$ip_navegacion', '$ip_equipo', '$mikrotik_gateway', '$cliente_id')" );
                }else{
                  $data_server = pg_exec($db, "update server set se_ip_pc = '$ip_navegacion', se_ip_ant ='$ip_equipo', parent_mkt = '$mikrotik_gateway', idnum_tar = '$cliente_id' where idclientes = '$cliente_id'");
                }
                $affected_rows = pg_affected_rows($data_server);
            }else{
              $mensaje_suberror = 'La Ip ya existe'; 
            }
          }}
          else{
            $data_server = 1 ;

          }

    if (pg_affected_rows($query) < 1 || pg_affected_rows($data_server)< 1)
      {
        pg_query("ROLLBACK");
        if ($mensaje_suberror != "")
        {
            $mensaje_error = $mensaje_suberror;
        }else{
        $mensaje_error = 'El Registro No  Se Inserto';
      }
      }
      else 
      {
        pg_query("COMMIT");

        $mensaje_correcto = 'El Registro Se Inserto';
        if ($ip_navegacion != ""){
            add_user_mikrotik_navegacion($mikrotik_gateway);
          }
        

        }
      }else
      {
         $mensaje_error = 'Complete los campos con * '; 
      }
}

$sql =pg_exec($db, "select p.*, s.se_ip_ant, s.se_ip_pc from preclientes p left join server s on p.idpreclientes = s.idclientes where idpreclientes = $cliente_id");
$localidades = listar_localidades();
$planes = listar_planes();

?>
<div class="titulo">
  <h3> MODIFICAR CLIENTE A INSTALAR </h3>
</div>
<?php
$row = pg_fetch_object($sql);
$idpre_cliente =  $row->idpreclientes;
?>

<body>

  <div class="container">
    <div class="row">
      <div class="span12">

        <form  action="" method="post">

          <?php if ($mensaje_error != ""){ ?>
          <div class="alert alert-danger">
            <h4><?php echo $mensaje_error; ?></h4>
          </div>
          <?php } ?>

          <?php if ($mensaje_correcto != ""){ ?>
          <div class="alert alert-success">
            <h4><?php echo $mensaje_correcto; ?></h4>
          </div>
          <?php
        }
        ?>
        <div>
          N° CLIENTE 
          <input type="text" name="icliente_id" id="cliente_id" size="9" value="<?php echo $row->idpreclientes; ?>" readonly>
        </div>
        <div >
          <label>* Apellido</label>
          <input  name= "txtape" type ="text" id="txtape" value ="<?php echo $row->apellido;  ?>" tabindex="1" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" class="required span6">
        </div>
        <div>
          <label>* Nombre</label>
          <input name= "txtnom" type ="text" id= "txtnom" value ="<?php echo $row->nombre;?>"  tabindex="2" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);"  class="required span6">
        </div>
        <div>
          <label>* Domicilio </label>
          <input name= "txtdom" type ="text" id= "txtdom" value ="<?php echo $row->domicilio; ?>"  tabindex="3" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);"  class="required span6">    
        </div>
        <div>
          <label>Barrio </label>
          <input name= "txtbar" type ="text" id= "txtbar"  value ="<?php echo $row->barrio; ?>"  tabindex="4" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" class="span6" >   
        </div>
        <div>
          <label>Referencia </label>
          <input name= "txtent" type ="text" id= "txtent" value ="<?php echo $row->entre; ?>"  tabindex="5" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" class="span6" />     
        </div>

        <div>
          <label>  * Localidad </label>
          <select name='txtlocal' id='txtlocal' tabindex='6' onkeypress='return handleEnter(this, event)' class = 'required span6'>
            <?php
            echo " <option value=''> SELECCIONE UNA LOCALIDAD </option>";
            while ($localidad = pg_fetch_object($localidades)) {
              if ((isset($row->localid) && $row->localid == $localidad->idlocalidad)) 
              {
                echo " <option value='$localidad->idlocalidad' id='$localidad->dpto' selected > $localidad->num_loc - $localidad->cp - $localidad->dpto </option>";
              }else
              {
                echo " <option value='$localidad->idlocalidad' id='$localidad->dpto' > $localidad->num_loc - $localidad->cp - $localidad->dpto </option>";
              }

            }?>

          </select>
        </div>
        <div>    
          <label>Telefono </label>
          <input name= "txttel" type ="text" id= "txttel"  value ="<?php echo $row->tel_fijo; ?>"  tabindex="7" onblur="validarEntero(this)" onkeypress="return handleEnter(this, event)" maxlength="12"  class="span6">    
        </div>
        <div>
         <label>Celular</label>
         <input name= "txtcel" type = "text" id= "txtcel"  value ="<?php echo $row->celular; ?>"   tabindex="8" onkeypress="return handleEnter(this, event)" maxlength="12" class="span6"/>    
       </div>
       <label>Celular 1</label>
       <input name= "txtcel1" type = "text" id= "txtcel1" value ="<?php if (isset($celular1)){ echo $celular1 ;} ?>"   tabindex="8" onkeypress="return handleEnter(this, event)" maxlength="12" class="span6"/>                
       <div>
        <label>Email</label>
        <input name= "txtmail" type = "text"  id= "txtmail" value ="<?php if (isset($email)){ echo $email ;} ?>"    tabindex="9" onkeypress="return handleEnter(this, event)" class="email span6" >    
      </div>

      <div>
        <label>Observación </label>
        <textarea name="txtobs"  id="txtobs" rows="3" cols="50"  class="span6" tabindex="11" onkeypress="return handleEnter(this, event);" onKeyDown="if(this.value.length >= 999){ alert('Has superado el tamaño máximo permitido'); return false;  }"><?php echo $row->observacion; ?>"</textarea>            
      </div>
      <div>
       <label>* Plan</label> 

       <select name='txtplan' id='txtplan' tabindex='12'  class='required span6'>

        <?php
        echo " <option value='' > SELECCIONE UN PLAN </option>";
        $plan_id = $row->idplan;
        while ($plan = pg_fetch_object($planes)) {
              //foreach ($linea as $valor_col) {
          if ((isset($plan_id) && $plan_id == $plan->id)) 
          {
            echo " <option value='$plan->id' id='$plan->id' selected> $plan->nombre - $plan->importe </option>";                
          }else
          {
            echo " <option value='$plan->id' id='$plan->id'> $plan->nombre - $plan->importe </option>";
          }
        }
        ?>
      </select>  
    </div>

<div class="span6">
    Ip Equipo:
    <input type="text" name="ip_equipo" value="<?php echo $row->se_ip_ant; ?>" ></td>
    Ip Navegación</td>
    <input type="text" name="ip_navegacion" value="<?php echo $row->se_ip_pc; ?>" ></td>
</div>
<div class="span6">
    <a href="../../asignar_eq.php?clie=<?php echo $cliente_id ?>" class="btn btn-success">Asignar Equipo</a>
    <input type="submit" value="Guardar" id="guardar" class="btn btn-primary" name="guardar" >
    </div>

  </form>
</div>
</div>
</div>
</body>