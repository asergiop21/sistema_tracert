  <?php
  include("../../menu.php");
  require_once("../../Librerias/conn.php");
  require("../../Controllers/LocalidadesController.php");
  require("../../Controllers/PlanesController.php");
  $db = Conec_con_pass();

  if (isset($_POST['guardar'] ))
    {

      $nombre = $_POST["txtnom"];
      $apellido = $_POST["txtape"];
      $domicilio = $_POST["txtdom"];
      $telefono = $_POST["txttel"];
      $celular = $_POST["txtcel"];
      $celular1 = $_POST["txtcel1"];
      $email = $_POST["txtmail"];
      $localidad_id = $_POST["txtlocal"];
      $observacion = $_POST["txtobs"];
      $barrio = $_POST["txtbar"];
      $referencia = $_POST["txtent"];
      $plan_id = $_POST["txtplan"];

      $iduser = $_SESSION['iduser'];
      $ip_actual = $_SESSION["dir_ip"];
  

      if (!empty($nombre) && !empty($apellido) && !empty($domicilio) && !empty($localidad_id) && !empty($plan_id))
      {

      pg_query("BEGIN");
      $query = pg_exec($db, "INSERT INTO preclientes (nombre, apellido, domicilio, tel_fijo, celular, cel1, email, observacion, localid, barrio, entre, idplan)  
                              VALUES ('$nombre','$apellido','$domicilio','$telefono','$celular','$celular1', '$email', '$observacion', '$localidad_id', '$barrio', '$referencia', 
                               '$plan_id')");
      $cliente_nuevo= pg_affected_rows($query);
       
    if ($cliente_nuevo < 1 )
      {
        pg_query("ROLLBACK");
        
          $mensaje_error = 'El Registro No  Se Inserto';
      }
      else 
      {
        pg_query("COMMIT");
        
        $mensaje_correcto = 'El Registro Se Inserto';
        }
      }else
      {
         $mensaje_error = 'Complete los campos con * '; 
      }
}
  
  $localidades = listar_localidades();
  $planes = listar_planes();
  ?>

  <script src="js/jsvalidate/jquery-latest.js"></script>
  <script type="text/javascript" src="js/jsvalidate/jquery.validate.js"></script>

    <script>
    $(document).ready(function(){

      $("#commentForm").validate();
    });
      jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
      phone_number = phone_number.replace(/\s+/g, ""); 
  	return this.optional(element) || phone_number.length > 9 &&
  		phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
  }, "Please specify a valid phone number");
    
    </script>

  <body>

  <div class="container">
    <div class="row">
      <div class="span12">

      <form  action="" method="post">
          
<?php if ($mensaje_error != ""){ ?>
  <div class="alert alert-danger">
    <h4><?php echo $mensaje_error; ?></h4>
  </div>
<?php } ?>

<?php if ($mensaje_correcto != ""){ ?>
  <div class="alert alert-success">
    <h4><?php echo $mensaje_correcto; ?></h4>
  </div>
<?php
}
?>
          <div class="titulo">
  	       	<h1>PRE ALTA DE CLIENTES</h1>
    	     </div>

  <div >
     <label>* Apellido</label>
    <input  name= "txtape" type ="text" id="txtape" value ="<?php if (isset($apellido)){ echo $apellido ;} ?>" tabindex="1" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" class="required span6">
  </div>
  <div>
  <label>* Nombre</label>
    <input name= "txtnom" type ="text" id= "txtnom" value ="<?php if (isset($nombre)){ echo $nombre ;} ?>"  tabindex="2" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);"  class="required span6">
  </div>
  <div>
    <label>* Domicilio </label>
    <input name= "txtdom" type ="text" id= "txtdom" value ="<?php if (isset($domicilio)){ echo $domicilio ;} ?>"  tabindex="3" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);"  class="required span6">    
  </div>
  <div>
      <label>Barrio </label>
  <input name= "txtbar" type ="text" id= "txtbar"  value ="<?php if (isset($barrio)){ echo $barrio ;} ?>"  tabindex="4" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" class="span6" >   
  </div>
    <div>
  <label>Referencia </label>
   <input name= "txtent" type ="text" id= "txtent" value ="<?php if (isset($referencia)){ echo $referencia ;} ?>"  tabindex="5" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event);" class="span6" />     
    </div>

         <div>
            <label>  * Localidad </label>
              <select name='txtlocal' id='txtlocal' tabindex='6' onkeypress='return handleEnter(this, event)' class = 'required span6'>
          <?php
          echo " <option value=''> SELECCIONE UNA LOCALIDAD </option>";
              while ($localidad = pg_fetch_object($localidades)) {
              if ((isset($localidad_id) && $localidad_id == $localidad->idlocalidad)) 
              {
              echo " <option value='$localidad->idlocalidad' id='$localidad->dpto' selected > $localidad->num_loc - $localidad->cp - $localidad->dpto </option>";
            }else
            {
              echo " <option value='$localidad->idlocalidad' id='$localidad->dpto' > $localidad->num_loc - $localidad->cp - $localidad->dpto </option>";
            }

           }?>

         </select>
         </div>
  <div>    
              <label>Telefono </label>
              <input name= "txttel" type ="text" id= "txttel"  value ="<?php if (isset($telefono)){ echo $telefono ;} ?>"  tabindex="7" onblur="validarEntero(this)" onkeypress="return handleEnter(this, event)" maxlength="12"  class="span6">    
      </div>
      <div>
               <label>Celular</label>
              <input name= "txtcel" type = "text" id= "txtcel"  value ="<?php if (isset($celular)){ echo $celular ;} ?>"   tabindex="8" onkeypress="return handleEnter(this, event)" maxlength="12" class="span6"/>    
  </div>
               <label>Celular 1</label>
              <input name= "txtcel1" type = "text" id= "txtcel1" value ="<?php if (isset($celular1)){ echo $celular1 ;} ?>"   tabindex="8" onkeypress="return handleEnter(this, event)" maxlength="12" class="span6"/>                
        <div>
              <label>Email</label>
               <input name= "txtmail" type = "text"  id= "txtmail" value ="<?php if (isset($email)){ echo $email ;} ?>"    tabindex="9" onkeypress="return handleEnter(this, event)" class="email span6" >    
        </div>
        
        <div>
              <label>Observación </label>
              <textarea name="txtobs"  id="txtobs" rows="3" cols="50"  class="span6" tabindex="11" onkeypress="return handleEnter(this, event);" onKeyDown="if(this.value.length >= 999){ alert('Has superado el tamaño máximo permitido'); return false;  }"><?php if (isset($observacion)){ echo $observacion ;} ?></textarea>            
      </div>
            <div>
               <label>* Plan</label> 

                <select name='txtplan' id='txtplan' tabindex='12'  class='required span6'>

      	  <?php
  			echo " <option value='' > SELECCIONE UN PLAN </option>";
     
  					while ($plan = pg_fetch_object($planes)) {
     					//foreach ($linea as $valor_col) {
              if ((isset($plan_id) && $plan_id == $plan->id)) 
              {
                echo " <option value='$plan->id' id='$plan->id' selected> $plan->nombre - $plan->importe </option>";                
              }else
              {
         			echo " <option value='$plan->id' id='$plan->id'> $plan->nombre - $plan->importe </option>";
            }
  				 }
      		?>
           </select>  
            </div>
                  <td ><input type="submit" value="Guardar" id="guardar" class="btn btn-primary" name="guardar" >
  </form>
      </div>
          </div>
    </div>
  </body>
