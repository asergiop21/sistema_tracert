<?php
		require_once "../../Librerias/conn.php";
	require_once "../../Controllers/PagosController.php";
	$db= Conec_con_pass();

	if(isset($_POST['descargar'])) {
		$desde = $_POST['desde'];
		$hasta = $_POST['hasta'];
		$organizacion_id = $_POST['organizacion_id'];
		$com= get_listar_comprobantes($desde, $hasta, $organizacion_id);
		$nombre_fichero = "comprobantes";
		
$csv_header = "Fecha|";
$csv_header .= "Nombre|";
$csv_header .= "Apellido|";
$csv_header .= "DNI|";
$csv_header .= "Cuit|";
$csv_header .= "Cae|";
$csv_header .= "Monto|";
$csv_header .= "Estado|";
$csv_header .= "Tipo|\r\n";

 $csv_datos = $csv_header;
 while($row_d = pg_fetch_object($com)){

$csv_datos .= $row_d->fecha_comprobante."|";
$csv_datos .= $row_d->nombre."|";
$csv_datos .= $row_d->apellido."|";
$csv_datos .= $row_d->dni."|";
$csv_datos .= $row_d->cuit."|";
$csv_datos .= $row_d->cae."|";
$csv_datos .= $row_d->importe."|";
$csv_datos .= $row_d->estado."|";
$csv_datos .=$row_d->tipo_comprobante."\r\n";
 	
 }
 //$csv_datos = trim(preg_replace('/\t+/', '', $csv_datos));
header('Content-disposition: attachment; filename='.$nombre_fichero);
		header('Content-type: text/plain');
print_r($csv_datos);
exit;
}
	include "../../menu.php";

	if(isset($_POST['enviar'])){

		$desde = $_POST['desde'];
		$hasta = $_POST['hasta'];
		$organizacion_id = $_POST['organizacion_id'];
		$comprobantes= get_listar_comprobantes($desde, $hasta, $organizacion_id);
		$suma_comprobantes_A_B = suma_comprobantes_A_B($desde, $hasta, $organizacion_id);
		$suma_NotaCredito_A_B = suma_NotaCredito_A_B($desde, $hasta, $organizacion_id);
	}else{
		$comprobantes= get_listar_comprobantes();
		$suma_comprobantes_A_B = suma_comprobantes_A_B();
		$suma_NotaCredito_A_B = suma_NotaCredito_A_B();
	}
	$organizaciones = pg_exec($db, "select * from organizaciones order by id");
	?>

	<form method="post">
		<div class="span12">
			<div class="span6">
				<div class="span3">
					<label>Desde</label>
					<input type="text" name="desde" id="desde" class="datepicker" value="<?php if($desde !=''){echo $desde;}?>">
					
				</div>
				<div class="span2">
					<label>Hasta</label>
					<input type="text" name="hasta" id="hasta" class="datepicker" value="<?php if($hasta !=''){echo $hasta;}?>">
				</div>
			</div>
			<div class="span3">
				<label></label>
				<div>
					<?php
					echo "<select name='organizacion_id' id='organizacion_id' tabindex='9' class='span2'>";
					echo "<option></option>";
					while ($organizacion = pg_fetch_object($organizaciones)) {          
						echo " <option value='$organizacion->id'> $organizacion->id</option>";
					}
					echo "</select>";
					pg_close($db);  
					?>
				</div>
			</div>
			<div>
				<div class="row"> 
					<div class="span12">
						<input type="submit" name="enviar" id="enviar" value="Filtrar" class="btn btn-info">
						<input type="submit" name="descargar" id="descargar" value="Descargar" class="btn btn-success">
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="span12">
			<div class="span4 ">
				<span class="label label-warning"><h4>Total Comprobantes: $<?php echo number_format(pg_fetch_object($suma_comprobantes_A_B)->sum, 2 , ',','.'); ?> </h4></span>	
			</div>
			<div class="span4">
				<span class="label label-warning"><h4>Total Notas de Credito: <?php echo number_format(pg_fetch_object($suma_NotaCredito_A_B)->sum, 2 , ',','.'); ?></h4></span>
				
			</div>
			<div class="span3">
				<span class="label label-warning"><h4>Cantidad Reg.: <?php echo pg_num_rows($comprobantes); ?></h4></span>
				
			</div>
		</div>
	</div>
	<table class="table">
		<tr>
			<th>#</th>
			<th>Fecha</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>DNI</th>
			<th>Cuit</th>
			<th>Cae</th>
			<th>Monto</th>
			<th>Estado</th>
			<th>Tipo </th>

		</tr>
		<?php
		while ($row = pg_fetch_object($comprobantes)){
			?>
			<tr>
				<td><?php echo $row->id?></td>
				<td><?php echo $row->fecha_comprobante; ?></td>
				<td><?php echo $row->nombre; ?></td>
				<td><?php echo $row->apellido; ?></td>
				<td><?php echo $row->dni; ?></td>
				<td><?php echo $row->cuit; ?></td>
				<td><?php echo $row->cae; ?></td>
				<td><?php echo  $row->importe; ?></td>
				<td><?php echo  $row->estado; ?></td>
				<td><?php echo  $row->tipo_comprobante; ?></td>
			</tr>
			<?php
		}

		?>
	</table>

