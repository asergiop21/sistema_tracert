<div  id="cd_abono"  class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
       <h3> Pagos de Abonos</h3>
   </div>  

   <div class="modal-body">
       <div class="span5">
        <div class="span1">
            Fecha <br>
            <input name="txtf_venc" id="txtf_venc" type="text" class="input-small"  size="3" onKeyUp="mascara(this,'/',patron,true);" onBlur=" esFechaValida(this);" value="<?php echo fecha_servidor_total() ; ?>" onkeypress="return handleEnter(this, event)" tabindex="0">
        </div>
        * Usuario <br>
        <div class="span2">
            <div id="clientes_div">
                <input type="text" id="cliente" size="40"  tabindex="1" onkeypress="return handleEnter(this, event)" class="required">
                <input type="hidden" id="idclientes_abono" name="clie">
                <input type="hidden" id="num_tar" name="num_tar" >
                <input type="hidden" id="cl_nya" name="cl_nya" >
            </div>
        </div>  
    </div>  
    <div class="span5">
    <div class="span2" >
        * Cond. Venta<br>
        <select id="cond_vta" name="cond_vta" tabindex="4" onkeypress="return handleEnter(this, event)" class="input-small">
            <option></option>
            <option value ="3">Contado</option>
            <option Value="5">Tarjeta Credito</option>
            <option Value="6">Tarjeta Debito</option>
            <option Value="1">Rapipago</option>
            <option Value="2">PagoFacil</option>
            
        </select>
    </div>
    <div class="span2">
        <br>
        <select id="tipo_tarjeta" name="tipo_tarjeta" tabindex="5"  class="input-small">
            <option></option>
            <option id='3' value ="Efectivo">Efectivo</option>
            <option id = '5' value ="Visa">Visa</option>
            <option id = '5' Value="Naranja">Naranja</option>
            <option id = '5' Value="Mastercard">Nativa</option>
            <option id = '5' Value="Nativa">Mastercard</option>
            <option id = '1' Value="Rapipago">Rapipago</option>
            <option id = '2' Value="PagoFacil">PagoFacil</option>
        </select>
    </div>
</div>

<div class="span5">
        <div class="span3" >
            *        Descripcion <br>
            <input name="txt_detalle_abono" id="txt_detalle_abono" type="text" maxlength="100" size="50" onkeypress="return handleEnter(this, event)" tabindex="2" onBlur="this.value=this.value.toUpperCase();">
        </div>

        <div class="span1">
            *        Importe<br>
            <input name="txt_imp_pag" class="input-small"  id="txt_imp_pag" type="text"size="8" onkeypress="javascript:return solonumeros(event)" tabindex="3" maxlength="7"  >
        </div>  
</div>
</div>

<div class="modal-footer">

    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    <input type="button" id="guardar_pagos" value="Guardar" onclick="agregar_pagos()" onkeypress="return handleEnter(this, event)" tabindex="7" class="btn btn-primary"> 
</div>  

</div>
