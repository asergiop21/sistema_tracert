<?php
include("../../menu.php");
require("../../Librerias/conn.php");
require("../../Controllers/ClientesController.php");
require("../../Controllers/LocalidadesController.php");
require_once("../../Controllers/AplicacionController.php");

$db= Conec_con_pass();

//var_dump($_POST);
echo $_POST['estado'];

if (isset($_POST['enviar']))
{
	if(isset($_POST['chkmarcar']) == true && $_POST['estado'] != ""){
		cambio_estado($_POST['chkmarcar'], $_POST['estado']);
	}else
	{	$mensaje_error = "Debe seleccionar algun cliente o el estado";
}

}

$query =pg_exec($db, "select clientes.idclientes, clientes.nombre, apellido, localid, tel_fijo, celular, planes.importe, barrio, (sum(importe_deuda) - sum(importe_pagado) ) as deuda, elim_ser 
									from clientes 
									inner join instalaciones on clientes.idclientes = instalaciones.idclientes 
									inner join planes on instalaciones.plan_id = planes.id
									inner join pagos on pagos.idclientes = clientes.idclientes
									where elim_clie = false and planes.importe > 0 
									group by clientes.idclientes, clientes.nombre, clientes.apellido, clientes.localid, clientes.tel_fijo,
									clientes.celular, planes.importe, clientes.barrio, elim_ser
									having (clientes.elim_ser = '1' and (sum(importe_deuda) - sum(importe_pagado)) > (planes.importe + 50 )) or 
									clientes.elim_ser = '2'
									order by elim_ser
	");

$count = pg_num_rows($query) ;
		?>
<h1 class="titulo"> AVISOS Y CORTES</h1> <hr>

<form name="frmdatos"  method="post">
  <?php if ($mensaje_error != ""){ ?>
          <div class="alert alert-danger">
            <h4><?php echo $mensaje_error; ?></h4>
          </div>
          <?php } ?>

          <?php if ($mensaje_correcto != ""){ ?>
          <div class="alert alert-success">
            <h4><?php echo $mensaje_correcto; ?></h4>
          </div>
<?php }?>
	<div>
		<select name="estado" id="estado">
			<option></option>
			<option value='aviso'>Aviso</option> 	
			<option value='corte'>Corte</option> 	
			<option value='sacar_aviso'>Sacar Aviso</option> 	
		</select>
		<input type="submit" name="enviar"></input>
	</div>

    <table id="tlbnroins" class="table table-striped" >
<tr>
             <td class="datos4">Marcar <input type="checkbox" name="todo" ></td>
			<td class="datos4">Idclie</td>
			<td class="datos4">Nombre y Apellido</td>
			<td class="datos4">Domicilio</td>
			<td class="datos4">Barrio</td>
			<td class="datos4">Localidad</td>
			<td class="datos4">Abono</td>
			<td class="datos4">Deuda</td>
			<td class="datos4">Tel.</td>
			<td class="datos4">Cel.</td>
            <td class="datos4">Estado</td>
	</tr>
<?php
		while ($row = pg_fetch_object($query)) {
			?>
	<tr>
     
                <td align="center"  class="datos"><input type="checkbox" name="chkmarcar[]" id="chkmarcar" value ="<?php  echo $row->idclientes ?>"></td>
                <td class="datos"><?php echo $row->idclientes ?></td>
                <td class="datos"><?php echo $row->apellido." ".$row->nombre?></td>
                <td class="datos"><?php echo $row->domicilio;?></td>
                <td class="datos"><?php echo $row->barrio;?></td>
                <td class="datos"><?php echo get_localidad($row->localidad)->num_loc?></td>
                <td class="datos"><?php echo $row->importe?></td>
                <td class="datos"><?php echo $row->deuda?></td>
                <td class="datos"><?php echo $row->tel_fijo?></td>
                <td class="datos"><?php echo $row->celular?></td>
                <td class="datos"><?php echo get_estado_cliente($row->elim_ser)->nombre;?></td>
	</tr>
		<?php
			}

?>

</table>
<th><?php echo "cantidad de clientes $count"?></th>
 </form>

</body>
</html>