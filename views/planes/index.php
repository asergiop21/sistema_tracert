<?php
	include_once("../../menu.php");
	require_once("../../Librerias/conn.php");

	$db = Conec_con_pass();

	$planes = pg_exec($db, "select id, nombre, velocidad_subida, velocidad_bajada, importe, importe_recargo,(select count(plan_id)  
						from instalaciones inner join clientes on instalaciones.idclientes = clientes.idclientes 
						where instalaciones.plan_id = p.id and elim_clie = false and elim_ser > '0' and tipo_cliente_id = 1  ) as cantidad from planes p order by nombre ");
$contar = 0;

?>

<div class="container">
	<div class="row">
		<div class="span12">
<h2>Planes</h2>
<table class="table table-striped">
	<tr>
	<th>id</th>
	<th>Nombre</th>
	<th>Cantidad</th>
	<th>Velocidad Subida</th>
	<th>Velocidad Bajada</th>
	<th>Precio</th>
	<th>Recargo</th>
	<th></th>

	</tr>

<?php
	while($row_planes = pg_fetch_object($planes))
	{
	$contar =  $row_planes->cantidad + $contar
?>
	<tr>
		<td><?php echo $row_planes->id; ?></td>
		<td><?php echo $row_planes->nombre; ?></td>
		<td><?php echo $row_planes->cantidad ?></td>
		<td><?php echo $row_planes->velocidad_subida ?></td>
		<td><?php echo $row_planes->velocidad_bajada ?></td>
		<td>$ <?php echo number_format($row_planes->importe,2 ) ?></td>
		<td>$ <?php echo number_format($row_planes->importe_recargo,2 ) ?></td>
		<td><a href="editar.php?plan_id=<?php echo $row_planes->id?>">Editar</a></td>
		<td><a href="unificar.php?plan_id=<?php echo $row_planes->id?>">Unificar</a></td>


	</tr>
<?php
}
?>
	</table>
<div>
<h3>Cantidad de usuarios : <?php echo $contar; ?></h3>
</div>


	<div>
	<a href="nuevo.php" > Agregar Plan </a>
	</div>
	</div>
	</div>
	</div>