<?php

require_once("../../Librerias/conn.php");
require_once("../../Controllers/ClientesController.php");
require_once("../../Controllers/AplicacionController.php");
require_once("../../Controllers/CuentasController.php");

$cliente_id=$_REQUEST['cliente_id'];
echo $tipo_promesa = $_REQUEST['tipo_promesa'];
if(isset($_POST['enviar'])){

    $insertar_promesa = guardar_promesa($_POST);
    if ($insertar_promesa[0] == 'true'){
        echo $url = $_POST['url_referencia'];
        $mensaje_promesa =  $_SESSION['mensaje_promesa'] =  $insertar_promesa[1];
        header('Location:'. $url.'?mensaje_promesa='. $mensaje_promesa);
        exit;
    }else{
        echo $insertar_promesa[1];

    }
}
include_once("../../menu.php");
$datos_clientes = get_cliente($cliente_id);
?>
    
<script>
    $(function() {
    $( "#fecha_pago").datepicker({
                        format: 'dd-mm-yyyy',
                        minDate: '0',
                        onSelect: function(dateText, inst) {
                                var lockDate = new Date($('#fecha_pago').datepicker('getDate'));
                                lockDate.setDate(lockDate.getDate() + 1);      
                        }
    });

     $("#commentForm").validate({
         rules:{
                importe_pago: {number: true  }
        }});
    });
    function cancelar(){
         window.parent.location.reload(); window.parent.tb_remove(true);
        }

</script>
<form action="#" method="post" name="commentForm" id="commentForm">
<div id="container">
        <h3>Promesas de Pagos</h3>
        <h4>Clientes = <?php echo $datos_clientes->apellido . ", " .     $datos_clientes->nombre; ?></h4>
    <div class="container" >
            <div class="row">
                <div class="form-group ">
                    <label for="ingreso"><h4>¿Para cuando Paga?</h4></label>
                    <input type="text" id="fecha_pago" name="fecha_pago" class="form-control span6" tabindex="9" class="required"   />
                    <input type="hidden" id="cliente_id" name="cliente_id" value="<?php echo $cliente_id ?>"   />
                    <input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo $_SESSION['iduser']; ?>"   />
                    <input type="hidden" id="url_referencia" name="url_referencia" value="<?php echo $_SERVER['HTTP_REFERER']; ?>"/>
                    <input type="hidden" id="tipo_promesa" name="tipo_promesa" value="<?php echo $tipo_promesa?>"   />
                
                    <label for="ingreso"><h4>¿Cuánto va a Pagar?</h4></label>
                    <input type="text" id="importe_pago" name="importe_pago"  tabindex="9" class="required form-control span6"  />
                    
                    <label for="ingreso"><h4>¿Quedáron en algo más?</h4></label>
                    <textarea cols="50" rows="8" id="comentario" name="comentario" class="form-control span6"></textarea>
                    <div>
                    <input type="submit" id="enviar" name="enviar" value="Guardar" class="btn btn-primary"  />
                </div>
                </div>
            </div>
    </div>
</div>
    </form>           
