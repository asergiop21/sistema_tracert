<?php
require_once("../../Librerias/conn.php");
require_once("../../Controllers/ReportesController.php");
require_once("../../Controllers/PagosController.php");
$db = Conec_con_pass();


if (isset($_GET['imprimir'])) {
  $pago_id = $_GET['pago_id_imprimir'];
   
   if (existe_comprobante($pago_id)){
        imprimir_factura($pago_id);
   }else{
         $pagos = pg_exec($db, "select o.nombre as organizacion_nombre, o.id as organizacion_id, c.cuit, c.dni, p.importe_pagado, p.token, p.idpago, p.detalle, c.tipo_factura from pagos p inner join clientes c on p.idclientes = c.idclientes 
           inner join organizaciones o on o.id = c.organizacion_id
      where p.idpago = '$pago_id'");
      generar_comprobantes($pagos);
      imprimir_factura($pago_id);
   }
}

include("../../menu.php");
if (isset($_GET['pago_id'] )){

  $pago_id = $_GET['pago_id'];

  $sql_eliminar_pago = eliminar_pago($pago_id);

  if ($sql_eliminar_pago == true){

        $mensaje_correcto = "Registro Eliminado";
  }else
  {
      $mensaje_error = "Error al eliminar el registro";
  }
}

if (isset($_GET['cliente_id'] )){
 $cliente_id = $_GET['cliente_id'];
        $sql_pago = get_listar_pagos_cliente($cliente_id);

      }
      ?>
      <div class="container-fluid">
        <h1 class="titulo"> RESUMEN DE PAGOS</h1> <hr width="100%" align="left">

        <form >
           <?php if ($mensaje_error != ""){ ?>
          <div class="alert alert-danger">
            <h4><?php echo $mensaje_error; ?></h4>
          </div>
          <?php } ?>

          <?php if ($mensaje_correcto != ""){ ?>
          <div class="alert alert-success">
            <h4><?php echo $mensaje_correcto; ?></h4>
          </div>
<?php }?>

          <div class="span12">
            <div class="span4"> 
              <input type="text" id="clientes" class="span4" >
              </div>
            <div class="span3"> 
              <input type="submit" class="btn btn-primary span2" name="consulta" id="consulta" value="consultar" >
            </div>
            <div id="datos_cliente" class="span4"> 
              <p><?php $datos = pg_fetch_object($sql_pago[1]);
                echo 'Nombre: ' . $datos->apellido . ', ' . $datos->nombre_cliente. '<br>';
                echo 'Plan: '. $datos->nombre. '<br>';
                
            ?></p>
          </div>

          <input type="hidden" id="cliente_id" name="cliente_id" value=<?php if(isset($cliente_id)){echo $cliente_id;} ?>>

          <div class="span12">
            <ul class="nav nav-tabs">
              <li class="active"><a href="<?php echo BASE_URL ?>views/cuentas?>>">Cuenta</a></li>
              <li><a href="<?php echo BASE_URL ?>views/cuentas/nuevo.php?cliente_id=<?php echo $cliente_id; ?>">Ingresar Servicio o Descuento</a></li>
            </ul> 
          </div>
          <?php
          if($sql_pago){ ?>
          <table class="table table-striped table-bordered table-hover table-condensed"  >
            <tr>
              <th>Nº pagos</th>
              <th>Fecha</th>
              <th>Detalle</th>
              <th>Debe</th>
              <th>Haber</th>
              <th>Deuda</th>
              <th>Eliminar</th>
              <th>imprimir</th>
            </tr>

            <?php
          pg_result_seek($sql_pago[1], 0);
            echo $deuda = pg_fetch_object($sql_pago[0])->total;
            
            while ($row = pg_fetch_object($sql_pago[1])) {
              setlocale(LC_MONETARY, 'es_AR');

                $importe_abonado = $row->importe_pagado;
              ?>
              <tr>
                <td ><?php echo $row->idpago;?></td>

                <td><?php echo cambiaf_a_normal($row->fecha_ing);?></td>

                <td ><?php echo $row->detalle;?></td>

                <td><?php echo $row->importe_deuda; ?></td>

                <td><?php echo $importe_abonado ?></td>

                <td><?php echo money_format('%i', $deuda); ?></td>
                <?php if ($row->importe_deuda > 0 and $row->cae == '' ){ ?>
                <td><a href="index.php?cliente_id=<?php echo $row->idclientes ?>&pago_id=<?php echo $row->idpago; ?>" class="btn btn-danger btn-mini">Eliminar</a> </td>
                <?php } 
                if ($importe_abonado !=''){
                ?>
                <td><form>
        <input type="hidden" name="pago_id_imprimir" value="<?php echo $row->idpago;?>">
        <input type="hidden" name="cliente_id" id="cliente_id" value ="<?php echo $cliente_id;?>">
        <input type="submit" name="imprimir" value="Imprimir">
      </form></td>

                <?php } ?>
              </tr>
              <?php 
              $deuda = $deuda + $row->importe_deuda ;
              $deuda = $deuda - $row->importe_pagado ;

            }  ?>
            </table>

            <?php  } ?>    
          </form>
        </div>

