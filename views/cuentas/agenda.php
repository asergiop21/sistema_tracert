<?php
require_once '../../Librerias/conn.php';
require_once("../../Controllers/ClientesController.php");
require_once("../../Controllers/CuentasController.php");

$db = Conec_con_pass();
if (empty($pro)) { $pro = 0;} 

if(isset($_POST['cliente_moroso_id'])){

  $cliente_id = $_POST['cliente_moroso_id'];
  $user_id = $_POST['user_id'];
  $comentario = $_POST['comentario'];

  $fn_deshabilitar = deshabilitar_cliente($cliente_id, $user_id, $comentario);
 
  if ($fn_deshabilitar[0] == 'true'){
    $mensaje = $fn_deshabilitar[1];
  }else{
    $mensaje = $fn_deshabilitar[1];
  }
  session_start();
  $_SESSION['mensaje'] = $mensaje;  
  session_write_close();
   $base_url = url();
   header("Location:" . $base_url . "views/cuentas/agenda.php");
    exit ();        
}

include_once '../../menu.php';

$pro = $_REQUEST['pro']; //si es 0 es promesa normal! si es 1 promesa de Tratamientos
if (isset($_SESSION['mensaje'])){
  $mensaje = $_SESSION['mensaje'];
  unset($_SESSION['mensaje']);
}

$sql_ps = pg_exec($db, "Select clientes.nombre, clientes.apellido, clientes.idclientes, idpromesas, ps_fecha, 
  ps_observaciones, p.id, sum(importe_deuda)- sum(importe_pagado) as deuda_pagos, ps_fec_prome, usuario,
  p.importe
  from promesas
  inner join clientes on promesas.ps_idclientes = clientes.idclientes
  inner join instalaciones on clientes.idclientes = instalaciones.idclientes
  inner join usuario on promesas.ps_user = usuario.idusuario
  inner join server on clientes.idclientes = server.idclientes 
  inner join planes p on instalaciones.plan_id = p.id
  inner join pagos pa on pa.idclientes = clientes.idclientes
  where ps_cerrada = 'f' and ps_tratamientos = '0'  and clientes.elim_ser not in ('0')
  group by clientes.idclientes, clientes.nombre, clientes.apellido, idpromesas,  ps_fecha, ps_observaciones, p.id, ps_fec_prome, usuario
  having(sum(importe_deuda) - sum(importe_pagado) > (p.importe + (p.importe / 4 )))
  order by ps_fec_prome asc ");


?>
<body>
  <div >
    <form action="#" method="post">  
      <?php if ($mensaje != ""){ ?>
      <div class="alert alert-info">
        <h4><?php echo $mensaje; ?></h4>
      </div>
      <?php } ?>

     
    <?php

    if(isset($_REQUEST['mensaje_promesa'])){
      $mensaje = $_REQUEST['mensaje_promesa'];
      echo "<div class ='alert-success'>$mensaje</div>";
      unset($_REQUEST['mensaje_promesa']);

    } ?>

     <div>
            Cantidad de Clientes: <?php echo pg_num_rows($sql_ps); ?>  

        </div>


    <table class="table" >
      <tr>
        <th>Fecha</th>
        <th>Promesa Nueva</th>
        <th>Corte</th>
        <th>Cliente</th>
        <th>Deuda</th>
        <th>Observaciones</th>
        <th>Fecha Prom.</th>
        <th>Usuarios</th>  
      </tr>
      <?php
      while($row_ps = pg_fetch_object($sql_ps)){
        $abono = $row_ps->importe;
        $idclie = $row_ps->idclientes;
        $idpro = $row_ps->idpromesas;
        $ca_deu = 0;
        $ca_pag = 0;
        $deu_toc = 0;
        ?>
        <tr>
          <td><?php echo cambiaf_a_normal($row_ps->ps_fecha);  ?></td>
          <td><a href="<?php echo BASE_URL ?>views/cuentas/promesas.php?cliente_id=<?php echo $row_ps->idclientes?>" >Promesa</a></td>
          <td>

          <button type="button" data-toggle="modal" data-target="#myModal"  data-id="<?php echo $row_ps->idclientes ?>" value="<?php echo $row_ps->idclientes ?>" id="<?php echo $row_ps->idclientes ?>" name="deshabilitar" class="btn btn-danger agenda_deshabilitar">Deshabilitar</button></td>
          <td><p><a href="<?php echo BASE_URL ?>historial.php?clie=<?php echo $row_ps->idclientes; ?>" target="_blank" ><?php echo $ayp = $row_ps->apellido.", ".$row_ps->nombre;?></a> </p></td>
          <td><?php echo number_format($row_ps->deuda_pagos,2); ?></td>
          <td><p><?php echo $row_ps->ps_observaciones;  ?></p></td>
          <td><?php echo cambiaf_a_normal($row_ps->ps_fec_prome ); ?></td>
          <td><?php  echo $row_ps->usuario; $idclie;?></td>
          
          <td><input type="hidden" name="pro" id="pro" value="<?php echo $pro; ?>"></td>
          <td><input type="hidden" name="user_id" id="user_id" value="<?php echo  $_SESSION['iduser']; ?>"></td>
        </tr>
        <?php
      }
      ?>

    <input type="hidden" name="cliente_moroso_id" id="cliente_moroso_id"  placeholder="" class="form-control">
    <input type="hidden" name="comentario" id="comentario"  placeholder="" class="form-control">

    </table>
   

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Desea Deshabilitar el Cliente?</h4>
      </div>
      <div class="modal-body">
    <div class="row-fluid">
    
     <input type="text" name="comentario_cliente" id = "comentario_cliente" value="" placeholder="" class="form-control span12">


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger deshabilitar_cliente">Eliminar</button>
      </div>
    </div>
  </div>
</div>



  </form>
</div>
</body> 