<?php
include("../../menu.php");
require_once("../../Librerias/conn.php");
require_once("../../Controllers/CuentasController.php");
require_once("../../Controllers/AplicacionController.php");
require_once("../../Controllers/SoportesController.php");

$current_page = curPageName();
$db = Conec_con_pass();
$page = 1;
$row_for_page = $_SESSION['row_for_page'] ;
if(array_key_exists('pg', $_GET)){
          $page = $_GET['pg']; //si el valor pg existe en nuestra url, significa que estamos en una pagina en especifico.
          $page;
        }
        $clientes_morosos = cuentas_morosos($page);
        $max_number_page = (intval(pg_num_rows($clientes_morosos[1]) / $row_for_page) + 1 ) ;
?>

        <form action="#" method="POST">
          <?php

          if(isset($_REQUEST['mensaje_promesa'])){
            $mensaje = $_REQUEST['mensaje_promesa'];
            echo "<div class ='alert-success'>$mensaje</div>";
            unset($_REQUEST['mensaje_promesa']);
                      
      } ?>


          <div class="row-fluid">
            <?php include "../../Librerias/paginador.php" ?>
            <table class="table table-striped">
              <tr>
                <th></th>
                <th>Apellido y Nombre </th>
                <th>Abono </th>
                <th>Deuda</th>  
              
              </tr>
              <?php
              while($row_ca = pg_fetch_object($clientes_morosos[0]))
              {           
                ?>
                <tr>
                  <td><a href="<?php echo BASE_URL ?>views/cuentas/promesas.php?cliente_id=<?php echo $row_ca->idclientes?>" >Promesa</a></td>
                  <td><p><a href="<?php echo BASE_URL ?>historial.php?clie=<?php echo $row_ca->idclientes; ?>" target="_blank" ><?php echo $ayp = $row_ca->apellido.", ".$row_ca->nombre;?></a> 
                    <?php $sql_rec =soportes_pendientes($row_ca->idclientes); 
                    $fdev_rec = pg_num_rows($sql_rec); 
                    if ($fdev_rec > 0){ echo "<b> <font color='red'>(P)</font></b> "; }
                    ?></p></td>
                    <td><?php echo $row_ca->importe;?></td>
                    <td><?php echo $row_ca->deuda_total_cliente; ?></td>
                    
                  </tr>    
                  <?php
                }
                ?>
              </table>
            </div>
          </form>       
