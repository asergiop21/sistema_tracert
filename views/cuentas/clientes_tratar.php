<?php

include_once'../../menu.php';
require_once("../../Librerias/conn.php");
require_once("../../Librerias/pagos.php");
require_once("../../Controllers/PagosController.php");
require_once("../../Controllers/ClientesController.php");

$db= Conec_con_pass();

  if (isset($_POST['habilitar'])){
    $cliente_id = $_POST['habilitar'];
    $fn_restaurar = restaurar_cliente_tratamiento($cliente_id);
    if ($fn_restaurar[0] == 'true'){
           $mensaje_correcto = $fn_restaurar[1];
        }else{
          $mensaje_error = $fn_restaurar[1];
        }
  }

   /* if(isset($_POST['habilitar'])){
       $cliente_id = $_POST['habilitar'];
        $fn_habilitar = habilitar_cliente($cliente_id);
        echo "no paso";
        if ($fn_habilitar[0] == 'true'){
           $mensaje_correcto = $fn_habilitar[1];
        }else{
          $mensaje_error = $fn_habilitar[1];
        }
    }*/

$sql_tr = pg_exec($db, "Select clientes.*, tratamientos.*, pl.importe as importe, p.* from clientes 
    inner join tratamientos on clientes.idclientes = tratamientos.tra_idclientes
    inner join instalaciones i on clientes.idclientes = i.idclientes
    inner join planes pl on i.plan_id = pl.id
    left join (select * from promesas where ps_cerrada = 'false') p on tratamientos.tra_idclientes =  p.ps_idclientes  where tra_cerrada = 'false'
    order by tra_fecha asc");
$cant = pg_num_rows($sql_tr);

?>

<form method="POST">

   <?php if ($mensaje_error != ""){ ?>
      <div class="alert alert-danger">
        <h4><?php echo $mensaje_error; ?></h4>
      </div>
      <?php } ?>

      <?php if ($mensaje_correcto != ""){ ?>
      <div class="alert alert-success">
        <h4><?php echo $mensaje_correcto; ?></h4>
      </div>
      <?php
    }
    ?>
    
    <div class="<?php echo $_REQUEST['class']; ?>">
        <?php echo $_REQUEST['mensaje']; ?>           
    </div>
    <div >            
        <h1>Clientes en Tratamiento</h1>

        <table class="table">
            <tr>
                <th>Fecha</th>
                <th>Apellido y Nombre</th>
                <th>Deuda</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>

            <?php while ($row_tr = pg_fetch_object($sql_tr)){
                $cliente_id = $row_tr->idclientes; 
                $deuda = 0;
                $deuda = calcular_deuda($cliente_id);
                $nivel_deuda = calcular_nivel_deuda($cliente_id, $row_tr->importe, $deuda );
                ?>
                <tr>
                    <td><?php echo cambiaf_a_normal($row_tr->ps_fecha); ?></td>
                    <td><p><a href="<?php echo BASE_URL ?>historial.php?clie=<?php echo $cliente_id;?>" target="_blank" ><?php echo $ayp = $row_tr->apellido.", ".$row_tr->nombre; ?></a><b> <font color="<?php echo $nivel_deuda[0] ?>"><?php echo $nivel_deuda[1]?></font></b> </p></td>           
                  <td><?php 
                  echo $deuda;
                  $deuda_gral = $deuda_gral + $deuda;

                  ?></td>
                  
                  <td><a href="<?php echo BASE_URL ?>views/cuentas/promesas.php?cliente_id=<?php echo $row_tr->idclientes?>&tipo_promesa=tratamientos" >Promesa</a></td>
                   <td><button type="submit" value="<?php echo  $row_tr->idtratam;?>" id="<?php echo $row_tr->idtratam?>" name="habilitar" class="btn btn-success"onclick="return confirm('Desea restaurarlo?')">Habilitar</button></td>
                   
                  <td><a href="<?php echo BASE_URL; ?>views/clientes/eliminar_clienteht.php?num_clie=<?php echo $row_tr->idclientes?>&user_id=<?php echo $_SESSION['iduser'] ?>"  >Eliminar</a></span></td>
                  <td><a href="<?php echo BASE_URL; ?>views/clientes/imprimir_aviso.php?clie=<?php echo $row_tr->idclientes; ?>">Carta</a></td>
              </tr>
              <?php 
          }
          ?>
      </table>
      <p>
        <?php 
        $spro = pg_exec($db , "select * from promesas where ps_cerrada = false and ps_tratamientos = 1");
        $ca_pro = pg_num_rows($spro);

    echo "Cantidad de Registros = $cant </br> Total de Deuda: $deuda_gral <br> Cantidad Clientes en promesa: $ca_pro <br> Cantidad deuda promesa:". $_SESSION['deuda_prom']; ?>
</p>
</div>       

<!-- Trigger the modal with a button -->
<!-- Modal -->

</form>
