<?php

require("../../Librerias/conn.php");
//include("../../menu.php");
$db=Conec_con_pass();

$nombre_fichero = "FAC5868.";
$nombre_fichero .= date_ddmmyy();

$deuda_mensual = pg_exec($db, "Select * from 
							clientes c inner join instalaciones i on c.idclientes = i.idclientes 
							inner join planes p on i.plan_id = p.id
							where elim_clie = 'false' and p.importe > 0 ");
$csv_datos_header = "0";
$csv_datos_header .= "400";
$csv_datos_header .= "5868";
$csv_datos_header .= date_for_pay();
$csv_datos = str_pad($csv_datos_header, 280, "0");
$csv_datos .= "\r\n";
$count = 0;

while ($datos = pg_fetch_object($deuda_mensual))
{
	$deuda_acumulada = pg_exec($db,"Select (sum(importe_deuda) - sum(importe_pagado)) as sum from pagos where idclientes = '$datos->idclientes' ");
	$deuda = floatval(pg_fetch_object($deuda_acumulada)->sum);

	if ( $deuda <= 0 )
	{
		continue;
	}

	$first_due_date = fecha_servidor_anio_completo().fecha_servidor_mes()."10";
	$second_due_date = fecha_servidor_anio_completo().fecha_servidor_mes()."20";
	$third_due_date = fecha_servidor_anio_completo().fecha_servidor_mes()."28";
	$amount_first = str_replace(".", "", $deuda);
	$total_first_due_date += $amount_first ;
	$amount_second = str_replace(".", "", ($amount_first + 30));
	$amount_third= str_replace(".", "", ($amount_first + 60));
	$detail = "Abono por pagomiscuentas";
	$detail_customer = "Detalle de pago";
	$code_="";

			$csv_datos .= "5";
			$csv_datos .= str_pad($datos->idnum_tarjeta, 19, " ");
			$csv_datos .= str_pad($ceros, 20, " ");
			$csv_datos .= "0";
			$csv_datos .= $first_due_date;
			$csv_datos .= str_pad(number_format($amount_first, 2 , "", ""), 11, "0", STR_PAD_LEFT);
			$csv_datos .= $second_due_date;
			$csv_datos .= str_pad(number_format($amount_second, 2 ,"" , ""), 11, "0",STR_PAD_LEFT);
			$csv_datos .= $third_due_date;
			$csv_datos .= str_pad(number_format($amount_third, 2, "", ""), 11, "0", STR_PAD_LEFT);
			$csv_datos .= str_pad($ceros, 19,"0");
			$csv_datos .= str_pad($datos->idnum_tarjeta, 19, " ");
			$csv_datos .= str_pad("ABONO MENSUAL", 40, " ");
			$csv_datos .= str_pad("ABONO MENSUAL", 15, " ");
			$csv_datos .= str_pad($space, 60, " ");
			$csv_datos .= str_pad($ceros, 29,"0");
			$csv_datos .= "\r\n";

		$count = $count + 1 ;
}

$csv_datos .= "9";
$csv_datos .= "400";
$csv_datos .= "5868";
$csv_datos .= date_for_pay();
$csv_datos .= str_pad($count, 7, "0", STR_PAD_LEFT);
$csv_datos .= str_pad($ceros, 7,"0");
$csv_datos .= str_pad(number_format($total_first_due_date,2 , "", ""),  16, "0" , STR_PAD_LEFT);
$csv_datos .= str_pad($ceros, 234,"0");

header('Content-disposition: attachment; filename='.$nombre_fichero);
header('Content-type: text/plain');
//header("Content-type: application/vnd.ms-excel");
//header("Content-disposition: csv" . date("Y-m-d") . ".txt");
//header( "Content-disposition: filename=".$nombre_fichero.".txt");
print_r($csv_datos);
exit;


?>