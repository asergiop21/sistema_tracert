          <div class="panel panel-primary setup-content" id="step-1">
           <div class="panel-heading">
             <h3 class="panel-title">Alta Cliente</h3>
           </div>
           <div class="panel-body">
           
            <div class="span12">
              <div class="control-group span6 " >  
                <label>Apellido</label>
                <input  name= "apellido" type ="text" id= "apellido" tabindex="1"   class="form-control span5" required="required" >
              </div>
              <div class="control-group span5 "> 
               <label>Nombre</label> 
               <input name= "nombre" type ="text" id= "nombre"   tabindex="2"    class="form-control span5" required="required">
           </div>
        </div>
   
           <div class="span12">
            <div class="control-group span6">
              <label>  * Localidad </label>
              <select name='localidad_id' id='localidad_id' tabindex='6'  class="form-control span5" required="required">
                <?php
                echo " <option value=''> SELECCIONE UNA LOCALIDAD </option>";
                while ($localidad = pg_fetch_object($localidades)) {
                                      echo " <option value='$localidad->idlocalidad' id='$localidad->dpto' > $localidad->num_loc - $localidad->cp - $localidad->dpto </option>";
                  }?>
                </select>
              </div>
              <div class="control-group span5">
                <label>Domicilio</label>
                <input name= "domicilio" type ="text" id= "domicilio"  tabindex="3" class="form-control span5" required="required">
              </div>
            </div>
            <div class="span12">
              <div class="form-group span6"> 
                <label>Barrio </label>
                <input name= "barrio" type ="text" id= "barrio"  tabindex="4" class="form-control span5"   >
              </div>
              <div class="form-group span5"> 
                <label>Referencia</label>
                <input name= "referencia" type ="text" id= "referencia" class="form-control span5"  tabindex="5" >
              </div>
            </div>
          <div class="span12">
            <div class="control-group span3"> 
              <label>Fecha de Nacimiento</label> 
              <input name="fecha_nacimiento" type="text" id="fecha_nacimiento" tabindex="7"  pattern="\d{1,2}-\d{1,2}-\d{4}"  placeholder="DD-MM-AAAA" class="form-control" required="required" >
              
            </div>
            <div class="control-group span3">
              <label>Dni</label> 
              <input name="dni" type ="text" id="dni"  tabindex="11"  maxlength="8" class="form-control" required="required">
            </div>
            <div class="form-group span3">
              <label>Cuit</label> 
              <input name="cuit"  type ="text" id="cuit" tabindex="13"   maxlength="11"  />
            </div>
            <div class="form-group span2">

              <label>Email</label>
              <input name= "email" type = "text"  id= "email" class="span2" tabindex="10" >
            </div>
    </div>
    <div class="span12">
            <div class="form-group span4">
              <label>Telefono</label>
              <input name= "telefono" type ="text" id= "telefono" tabindex="8" class="form-control"  />
            </div>
            <div class="form-group span4">
              <label>Celular</label>
              <input name="celular" type = "text" id= "celular"  tabindex="9"  >
            </div>
            <div class="form-group span3">
              <label>Celular 2</label>
              <input name="celular2" type = "text" id= "celular2"  tabindex="9" >
            </div>
      </div>
            <div class="form-group "> 
              <label>Observaciones</label>         
              <textarea name="observaciones"  id="observaciones" rows="3" class="span10"  cols="50" tabindex="12"  onKeyDown="if(this.value.length >= 10000){ alert('Has superado el tamaño maximo permitido'); return false;  }"></textarea>
            </div>
            <div class="form-group "> 
             <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
             </div>
 </div>
</div>