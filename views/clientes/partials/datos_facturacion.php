          <div class="panel-heading">
           <h3 class="panel-title">Facturacion</h3>
         </div>
         <div class="panel-body">
          <div class="form-group input-prepend">
            <label><h4>Costo Instalacion</h4></label>
            <span class="add-on">$</span>
            <input name="costo_instalacion"  type ="text" id="costo_instalacion" tabindex="14" onkeypress="return handleEnter(this, event)" maxlength="11"  class="required" />
            <div>
             <h4>Cuotas</h4>
             <?php $cuotas = $row->cantidad; ?>
             <select name="cuota_id" id="cuota_id">
              <option value="1" <?php if ( $cuotas == 1 ){?>  selected <?php   }?> >1</option>
              <option value="2" <?php if ( $cuotas == 2 ){?>  selected <?php   }?> >2</option>
              <option value="3" <?php if ( $cuotas == 3 ){?>  selected <?php   }?> >3</option>
            </select>
          </div>
        </div>
        <div class="form-group">
         <label><h4>* Plan</h4></label> 
         <select name='plan_id' id='plan_id' tabindex='12'  class='required span6'>
          <?php
          echo " <option value='' > Seleccione un plan </option>";
          while ($plan = pg_fetch_object($planes)) {
                            //foreach ($linea as $valor_col) {
            if ((isset($plan_id) && $plan_id == $plan->id)){
              echo " <option value='$plan->id' id='$plan->id' selected> $plan->nombre - $plan->importe </option>";                
            }else{
              echo " <option value='$plan->id' id='$plan->id'> $plan->nombre - $plan->importe </option>";
            }
          }
          ?>
        </select>  
      </div>
      <div>
          <h4>Promociones</h4>
          Meses
           <select name="cantidad_mes" id="cantidad_mes">
            <option></option>
              <option value="1" <?php if ( $cuotas == 1 ){?>  selected <?php   }?> >1</option>
              <option value="2" <?php if ( $cuotas == 2 ){?>  selected <?php   }?> >2</option>
              <option value="3" <?php if ( $cuotas == 3 ){?>  selected <?php   }?> >3</option>
              <option value="4" <?php if ( $cuotas == 4 ){?>  selected <?php   }?> >4</option>
              <option value="5" <?php if ( $cuotas == 5 ){?>  selected <?php   }?> >5</option>
              <option value="6" <?php if ( $cuotas == 6 ){?>  selected <?php   }?> >6</option>
              <option value="7" <?php if ( $cuotas == 7 ){?>  selected <?php   }?> >7</option>
              <option value="8" <?php if ( $cuotas == 8 ){?>  selected <?php   }?> >8</option>
              <option value="9" <?php if ( $cuotas == 9 ){?>  selected <?php   }?> >9</option>
              <option value="10" <?php if ( $cuotas == 10 ){?>  selected <?php   }?> >10</option>
              <option value="11" <?php if ( $cuotas == 11 ){?>  selected <?php   }?> >11</option>
              <option value="12" <?php if ( $cuotas == 12 ){?>  selected <?php   }?> >12</option>
            </select>
            Porcentaje
            <input type="text" name="porcentaje_promocion" id="porcentaje_promocion">

      </div>
      <div  class="form-group">
        <label><h4>* Organizacion</h4></label> 
        <select name='organizacion_id' id='organizacion_id' tabindex='12'  class='required span6' required="required">
          <?php
          echo " <option value='' > Selecione una organizacion </option>";
          while ($organizacion = pg_fetch_object($organizaciones)) {
                            //foreach ($linea as $valor_col) {
            if ((isset($organizacion_id) && $organizacion_id == $organizacion->id)){
              echo " <option value='$organizacion->id' id='$organizacion->id' selected> $organizacion->nombre </option>";                
            }else{
              echo " <option value='$organizacion->id' id='$organizacion->id'> $organizacion->nombre </option>";
            }
          }
          ?>
        </select>  
      </div>
      <div class="form-group">
       <div><h4>Tipo Factura:</h4>
        <select id="tipo_factura" name="tipo_factura">
          <?php 
          while($opcion_comprobantes = pg_fetch_object($tipos_comprobantes)) {
            if($cliente->tipo_factura == $opcion_comprobantes->nombre){
              echo "<option selected>$opcion_comprobantes->nombre</option>";
            }else{
              echo "<option>$opcion_comprobantes->nombre</option>";
            }
          }
          ?>        
        </select> 
      </div>
    </div>
    <div class="form-group">
      <label><h4>Calle de Facturacion</h4></label>
      <input type="text" id="txtdomdes" name="txtdomdes"  maxlength="50" class="span12"  size="50" tabindex="18" onkeypress="return handleEnter(this, event)" class="required">
    </div>
    <div class="form-group"> 
      <label><h4>Barrio de Facturacion</h4> </label>
      <input name= "txtbardes" type ="text" id= "txtbardes" tabindex="19" class="span12" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event)" maxlength="50" size="50" > 
    </div>
    <div class="form-group">
      <label><h4>Referencia de Facturacion</h4></label>
      <input name= "txtentdes" type ="text" id= "txtentdes" tabindex="20" class="span12" onBlur="this.value=this.value.toUpperCase();" onkeypress="return handleEnter(this, event)" maxlength="50" size="50"  size="50" >
    </div>
    <div class="form-group">
     <input class="btn btn-success pull-right"  name="guardar" type="submit" id="guardar_nuevo">
   </div>