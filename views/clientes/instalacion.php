<?php
include_once("../../menu.php");
require_once("../../Librerias/conn.php");
require_once("../../Librerias/Librerias_nuevas.php");
require_once("../../Controllers/MikrotiksController.php");
require_once("../../Controllers/OrganizacionesController.php");
require_once("../../Controllers/PagosController.php");
require_once("../../Controllers/ClientesController.php");
require_once("../../Controllers/AplicacionController.php");

$db = Conec_con_pass();
$cliente_id = $_REQUEST["cliente_id"];

if (isset($_POST['guardar'])){

   $fecha_instalacion = cambiafecha_a_bd($_POST['fecha_instalacion']);

  $costo = $_POST['costo'];
  if ($costo == ''){$costo = 0;}
  $cantidad_cuotas = $_POST['cuota_id'];
  $plan_id  = $_POST['plan_id'];
  $instalacion_id_hidden = $_POST['instalacion_id'];
  $importe_cuota = $costo / $cantidad_cuotas;
  $estado_id = $_POST['estado_id'];
  $tipo_factura = $_POST['tipo_factura'];
 

  $plan_name_query = pg_exec($db, "select * from planes where id = '$plan_id'");
  $plan_name = pg_fetch_object($plan_name_query);
  $velocidad_bajada = $plan_name->velocidad_bajada;
  $velocidad_subida = $plan_name->velocidad_subida;
  $instalador1 = $_POST['instalador1'];
  $instalador2= $_POST['instalador2'];

  pg_query("BEGIN");

  if ($instalador1 ==""){ $instalador1='Null';}
  if ($instalador2 ==""){ $instalador2='Null';}

  if(is_null($fecha_instalacion)){ 
    $update_instalacion = pg_exec($db, "update instalaciones  set plan_id = '$plan_id',  cost_inst = '$costo', instalador1=$instalador1, instalador2 = $instalador2 where idclientes = '$cliente_id' ");
  }else{
    $update_instalacion = pg_exec($db, "update instalaciones  set plan_id = '$plan_id',  cost_inst = '$costo', fecha_inst = '$fecha_instalacion', instalador1= $instalador1, instalador2= $instalador2 where idclientes = '$cliente_id' ");
  }
	
  $update_server = pg_exec($db, "update server set se_mdr = '$velocidad_bajada', se_mur = $velocidad_subida, se_esta = '$estado_id' where idclientes = '$cliente_id'");
  $update_cliente = pg_exec($db, "update clientes set elim_ser = '$estado_id', tipo_factura= '$tipo_factura' where idclientes ='$cliente_id'");

    $validar_cuotas_abonadas = pg_fetch_object(pg_exec($db, "Select cuota_abonada from cuotas where cliente_id = $cliente_id"))->cuota_abonada;

  if(is_null($validar_cuotas_abonadas) && is_null($fecha_instalacion)){
     $add_cuota = insertar_cuotas_instalacion($costo, $cantidad_cuotas, $cliente_id );
     if(is_null($add_cuota)){
        pg_query("ROLLBACK");
    echo "<div class ='alert-warning'>Registro no Actualizado - error insertar cuotas</div>";
     }
  }

  if($validar_cuotas_abonadas == 0 ){
      $update_cuotas = pg_exec($db, "update cuotas set cantidad_cuota ='$cantidad_cuotas', importe_cuota = '$importe_cuota', importe_total = '$costo' where cliente_id = $cliente_id");
      if(is_null(pg_affected_rows($update_cuotas))) {
           pg_query("ROLLBACK");
    echo "<div class ='alert-warning'>Registro no Actualizado - error actualizar cuotas </div>";
      }
  };

  $cantidad_meses = $_POST['cantidad_meses_instalacion'];
  $porcentaje = $_POST['porcentaje_promocion_instalacion'];
  if ($cantidad_meses != ''){

  $validar_promocion = pg_exec($db, "select * from promociones where cliente_id = $cliente_id");

  if(pg_num_rows($validar_promocion) > 0 ){

          $meses_insertados = pg_fetch_object($validar_promocion)->meses_insertados;

        if($meses_insertados == 0 ){

      $update_promocion = pg_exec($db, "update promociones set cantidad_meses ='$cantidad_meses', porcentaje = '$porcentaje' where cliente_id = $cliente_id");
      if(is_null(pg_affected_rows($update_promocion))) {
           pg_query("ROLLBACK");
    echo "<div class ='alert-warning'>Registro no Actualizado - error actualizar cuotas </div>";
      }
  }
  }else{

    $insertar_promocion = insertar_promocion($cliente_id, $cantidad_meses, $porcentaje);
    if ($insertar_promocion == 0 || empty($insertar_promocion)) {
    pg_query("ROLLBACK");
    return false;
  }
  }
  }

 $ip_query = pg_exec($db, "Select se_ip_pc from server where idclientes = '$cliente_id'");
  $ip_navegacion = pg_fetch_object($ip_query);
  $mikrotik_gateway = mikrotik_gateway($ip_navegacion->se_ip_pc);

  if (pg_affected_rows($update_instalacion)){
    pg_query("COMMIT");
    if ($ip_navegacion->se_ip_pc != "" || $ip_navegacion != false){//add_user_mikrotik_navegacion($mikrotik_gateway);
    }
    echo "<div class ='alert-success'>Actualizado Exitosamente </div>";
  }else{
    pg_query("ROLLBACK");
    echo "<div class ='alert-warning'>Registro no Actualizado - error actualizacion instalacion</div>";
  }
}

$cliente =pg_exec($db, "select * from clientes  where idclientes = '$cliente_id'");
$cliente = pg_fetch_object($cliente);
$instalacion = pg_exec($db, "select * from instalaciones i left join cuotas c  on i.idclientes = c.cliente_id 
                              left join promociones p on i.idclientes = p.cliente_id
  where idclientes ='$cliente_id'");
$row= pg_fetch_object($instalacion);
$organizacion = get_organizacion_cliente($cliente_id);

$plan_id = $row->plan_id;
$planes = pg_exec($db, "SELECT * FROM planes order by nombre asc");
$estados = pg_exec($db, "SELECT * FROM estados order by nombre asc");
$tipos_comprobantes = pg_exec($db, "SELECT * FROM tipos_comprobantes order by nombre asc");

?>

  <h1><b> Instalacion </b></h1>
  <form name ="frmdatos_instalaciones" id="frmdatos_instalaciones" action="" method="POST">

    <ul class="nav nav-tabs" id="myTab">
      <li><a href="<?php echo BASE_URL; ?>views/clientes/editar.php?cliente_id=<?php echo $cliente_id; ?> ">Datos</a></li>
      <li class="active"><a href="">Instalacion</a></li>
      <li><a href="<?php echo BASE_URL; ?>views/clientes/asignar_ip.php?cliente_id=<?php echo $cliente_id;?>">IPs</a></li>
      <li><a href="<?php echo BASE_URL; ?>views/clientes/asignar_equipos.php?cliente_id=<?php echo $cliente_id;?>">Asignar Equipos</a></li>
      <li><a href="<?php echo BASE_URL; ?>historial.php?clie=<?php echo $cliente_id;?>">Historial</a></li>
    </ul>
    <div class="row">
      <div class="span12">
        <div class="span8">
          <input type="hidden" value="<?php echo $cliente_id ?>" name="cliente_id" id="cliente_id">
          <input type="hidden" value="<?php echo $row->idinstalaciones; ?>" name="instalacion_id" id="instalacion_id">
          <div>
            Fecha Instalacion: <br>
            <input type="text" name="fecha_instalacion" id="fecha_instalacion" value="<?php  if (!empty($row->fecha_inst)){echo cambiafecha_a_normal($row->fecha_inst);}  ?>">
          </div>

          <div class="form-group input-prepend"> 
           
           <?php if ($row->cuota_abonada > 0){ ?>
           <div>
             Costo Instalacion: <br>
            <span class="add-on">$</span><input type="text" name="costo" id="costo" readonly="true"  value="<?php if (!empty($row->cost_inst)){echo $row->cost_inst;} ?>">
            </div>
            <div>
              Cuotas <br>
              <input type="text" name="cantidad_cuotas" id="cantidad_cuotas" readonly="true"   value="<?php if (!empty($row->cantidad_cuota)){echo $row->cantidad_cuota;} ?>">
            </div>
            <?php
             }else{?>
              <div>
           <?php $cuotas = $row->cantidad_cuota; ?>
           Costo Instalacion: <br>
            <span class="add-on">$</span><input type="text" name="costo" id="costo"  value="<?php if (!empty($row->cost_inst)){echo $row->cost_inst;} ?>">
             </div>
             <div>
             Cuotas <br>
           <select name="cuota_id" id="cuota_id">
            <option value="1" <?php if ( $cuotas == 1 ){?>  selected <?php    }?> >1</option>
            <option value="2" <?php if ( $cuotas == 2 ){?>  selected <?php   }?> >2</option>
            <option value="3" <?php if ( $cuotas == 3 ){?>  selected <?php   }?> >3</option>
          </select>
        </div>

         <?php  }?>
          
         </div>
         <div>
          <h4>Promociones</h4>




          <?php $cant_meses = $row->cantidad_meses;
              $porcentaje = $row->porcentaje;
              $meses_insertados = $row->meses_insertados;
              if ($meses_insertados > 0){ ?>
                Meses
                <input type="meses" name="meses" readonly = "true" value="<?php if (!empty($cant_meses)){echo $cant_meses;} ?>">    

            Porcentaje
            <input type="porcentaje" name="porcentaje" readonly ="true" value="<?php if (!empty($porcentaje)){echo $porcentaje;} ?>">

              <?php
              }else{
         ?>
          Meses
           <select name="cantidad_meses_instalacion" id="cantidad_meses_instalacion">
              <option value=""></option>
              <option value="1" <?php if ( $cant_meses == 1 ){?>  selected <?php   }?> >1</option>
              <option value="2" <?php if ( $cant_meses == 2 ){?>  selected <?php   }?> >2</option>
              <option value="3" <?php if ( $cant_meses == 3 ){?>  selected <?php   }?> >3</option>
              <option value="4" <?php if ( $cant_meses == 4 ){?>  selected <?php   }?> >4</option>
              <option value="5" <?php if ( $cant_meses == 5 ){?>  selected <?php   }?> >5</option>
              <option value="6" <?php if ( $cant_meses == 6 ){?>  selected <?php   }?> >6</option>
              <option value="7" <?php if ( $cant_meses == 7 ){?>  selected <?php   }?> >7</option>
              <option value="8" <?php if ( $cant_meses == 8 ){?>  selected <?php   }?> >8</option>
              <option value="9" <?php if ( $cant_meses == 9 ){?>  selected <?php   }?> >9</option>
              <option value="10" <?php if ( $cant_meses == 10 ){?>  selected <?php   }?> >10</option>
              <option value="11" <?php if ( $cant_meses == 11 ){?>  selected <?php   }?> >11</option>
              <option value="12" <?php if ( $cant_meses == 12 ){?>  selected <?php   }?> >12</option>
            </select>
            Porcentaje
            <input type="text" id="porcentaje_promocion_instalacion" name="porcentaje_promocion_instalacion" value="<?php if (!empty($porcentaje)){echo $porcentaje;} ?>">
<?php } ?>
      </div>
         
        <div>
        	Plan:<br>

          <select name='plan_id' id='plan_id' class="span3">

           <?php
           while ($plan = pg_fetch_object($planes)) {
             if ($plan_id == $plan->id)
             {
              echo "<option value='$plan->id' selected > $plan->nombre - $plan->velocidad_bajada - $ $plan->importe </option>";
            }
            else 
            {
             echo "<option value='$plan->id'> $plan->nombre - $plan->velocidad_bajada - $ $plan->importe</option>";
           }
         }
         ?>
       </select>
     </div>
     <div  class="form-group">

      <label>* Organizacion</label> 
      <input type="text" value='<?php echo $organizacion->nombre?> ' readonly = true id='organizacion_id' name='organizacion_id' >               
         
      </select>  
    </div>

    <div class="form-group">
      <div>Tipo Factura: <br>
        <select id="tipo_factura" name="tipo_factura">
          <?php 
         while($opcion_comprobantes = pg_fetch_object($tipos_comprobantes)) {
            if($cliente->tipo_factura == $opcion_comprobantes->nombre){
              echo "<option selected>$opcion_comprobantes->nombre</option>";
            }else{
            echo "<option>$opcion_comprobantes->nombre</option>";
          }
        }
          ?>
          
        </select> 
      </div>
      <div>
        Estado:<br>
        <select name="estado_id" id="estado_id">
         <?php while ($estado = pg_fetch_object($estados)) { 
          if ($estado->id == $cliente->elim_ser){
           echo "<option value='$estado->id' selected > $estado->nombre </option>";
         }else{
           echo "<option value='$estado->id'  > $estado->nombre </option>";
         }
       }
       ?>
     </select> 

   </div> 
   <div id="form_instaladores">
    Instaladores 1:<br>
    <select name="instalador1" id="instalador1"  tabindex="38" onkeypress="return handleEnter(this, event)">
          <option value="" selected>Seleccione Instalador
              
      <?php
      // CODIGO PHP
       // Contruimos el primer combo con los valores de la tabla 'centros'.
       $db = Conec_con_pass();
       $cons_centros = @pg_exec($db, "SELECT * FROM  usuario where us_perfil = '7' ");
       
       for ($k = 0; $k < pg_num_rows($cons_centros); ++$k)
       {
        $centro = @pg_fetch_object($cons_centros, $k);
        if ($centro->idusuario == $row->instalador1){
        echo "<option value='$centro->idusuario' selected>".$centro->us_nombre;
        }else{
        echo "               <option value=\"".$centro->idusuario."\"  >".$centro->us_nombre."\n";
        }       
        }
      ?>
          </select>
           <select name="instalador2" id="instalador2"  tabindex="38" onkeypress="return handleEnter(this, event)">
          <option value="" selected>Seleccione Instalador
              
      <?php
      // CODIGO PHP
       // Contruimos el primer combo con los valores de la tabla 'centros'.
       $db = Conec_con_pass();
       $cons_centros = @pg_exec($db, "SELECT * FROM  usuario where us_perfil = '7' ");
       
       for ($k = 0; $k < pg_num_rows($cons_centros); ++$k)
       {
        $centro = @pg_fetch_object($cons_centros, $k);
        if ($centro->idusuario == $row->instalador2){
        echo "<option value='$centro->idusuario' selected>".$centro->us_nombre;
        }else{
        echo "               <option value=\"".$centro->idusuario."\"  >".$centro->us_nombre."\n";
        }       
        }
      ?>
          </select>
   </div>
 </div>
 <div>
  <input type="submit" value="Guardar" name="guardar" id="guardar_instalaciones" class="btn btn-primary" > 
  </div>
</div>
</div>

</form>
