<?php
session_start();
trim($_SESSION['dir_ip']);

/*if ($_SESSION['dir_ip'] != '10.0.100.152')
{
	header("location:missing.html");
        die;
}*/
//require('rotation.php');
require_once("../../Librerias/conn.php");
require_once('../../i25.php');
require_once '../../Controllers/AplicacionController.php';
//require('fpdf.php');
function digito_verificador($clie)
{
	$tar=array( "152".$clie);
	foreach ($tar as $key => $value)
	{
		$suma = 0;
		$suma = $value[0] * 1 + $suma;
		$suma = $value[1] * 3 + $suma;
		$suma = $value[2] * 5 + $suma;
		$suma = $value[3] * 7 + $suma;
		$suma = $value[4] * 9 + $suma;
		$suma = $value[5] * 3 + $suma;
		$suma = $value[6] * 5 + $suma;
		$suma = $value[7] * 7 + $suma;
		$suma = $value[8] * 9 + $suma;
		$suma = $value[9] * 3 + $suma;
		$suma = $value[10] * 5 + $suma;
		$suma = $value[11] * 7 + $suma;
		$suma = $value[12] * 9 + $suma;


	}

	$resultado = $suma / 2;

	if ($resultado < '100')
	{
		return (int)$sum =substr($resultado,1);
	}
	else
	{
		return (int)$sum =substr($resultado,2);
	}

}


function fecha_servidor_escrita ()
{
        
	putenv("TZ=America/Argentina/Mendoza");
        
	$sdate=date("d")." de ".date("M")." de ".date("Y");
	$stime=date("h").":".date("i");
	return $sdate;

}

function actual_date ()
 {
     $week_days = array ("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
     $months = array ("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
     $year_now = date ("Y");
     $month_now = date ("n");
     $day_now = date ("j");
     $week_day_now = date ("w");
     $date = $week_days[$week_day_now] . "   " . $day_now ." de " . $months[$month_now] . " de " . $year_now;
     return $date;
 } 


$imprimir = $_REQUEST["clie"];
$db=Conec_con_pass();

$pdf = new PDF_i25();
$pdf->AddPage('P', 'A4');


$pdf->SetFont('Arial','',12);

$val = split(',',$imprimir);
$total_rows = count($val);
$cant = 0;

foreach ($val as $pa=>$c)
{
$sql_mm = @pg_exec($db,"select * from ((clientes inner join instalaciones on clientes.idclientes = instalaciones.idclientes)inner join localidad on clientes.localid = localidad.idlocalidad) where clientes.idclientes ='$c'");

while ($cli = pg_fetch_object($sql_mm)) {
$cant = $cant + 1;
$pdf->SetFont('Arial','B',20);
$pdf->SetY(45);
$pdf->SetX(20);
$pdf->MultiCell(180,10,utf8_decode("ESTUDIO JURÍDICO"),''     ,'C');
$pdf->SetFont('Arial','I',8);
$pdf->SetY(55);
$pdf->SetX(90);
$pdf->MultiCell(40,3,utf8_decode("Dr. Jorge Nelson Petenatti   Dr. JoséCarlos Aliotta & Asociados"),''     ,'C');

$pdf->Line(20,66,180,66);
$fec_esc = actual_date();

$pdf->SetFont('Arial','I',8);
$pdf->SetY(70);
$pdf->SetX(120);
$pdf->MultiCell(65 ,5," Mendoza, $fec_esc ",'','J');


$pdf->SetFont('Arial','B',10);
$pdf->SetY(75);
$pdf->SetX(25);
$pdf->MultiCell(30 ,5,"Sr / a:",'','J');
$pdf->SetY(75);
$pdf->SetX(50);
$pdf->MultiCell(100 ,5,$cli->apellido." ".$cli->nombre,'','J');
$pdf->SetY(85);
$pdf->SetX(25);
$pdf->MultiCell(30 ,5,"Calle:",'','J');
$pdf->SetY(85);
$pdf->SetX(50);

$pdf->MultiCell(150 ,5,utf8_decode($cli->domicilio." - B° ".$cli->barrio),'','J');
$pdf->SetY(90);
$pdf->SetX(50);
$pdf->MultiCell(100 ,5,$cli->num_loc." - ".$cli->dpto." - ".$cli->cp,'','J');
$pdf->SetY(95);
$pdf->SetX(50);
$pdf->MultiCell(100 ,5,$cli->entre,'','J');

$pdf->SetY(105);
$pdf->SetX(25);
$pdf->MultiCell(100 ,5,"Referencia:",'','J');
$pdf->SetFont('Arial','B',13);
$pdf->SetY(105);
$pdf->SetX(50);
$pdf->MultiCell(130 ,5,utf8_decode(" DEUDA  EN CURSO DE EJECUCIÓN"),'','J');
$sql1 = pg_exec($db,"select * from pagos where idclientes = '$c'");
while ($pagos = pg_fetch_object($sql1))
		{
			$debe = $debe + $pagos->importe_deuda;
			$haber = $haber + $pagos->importe_pagado;
			$deuda = $debe - $haber;
                           $deuda = number_format($deuda,2);
				}

$pdf->SetFont('Arial','B',10);
$pdf->SetY(125);
$pdf->SetX(25);

$pdf->MultiCell(150 ,5,utf8_decode("De nuestra mayor consideración:
En nuestro carácter de representantes de TECHTRON ARGENTINA S.A. (Celer Internet) tenemos el agrado de dirigirnos a Ud. a fin de informarle que según nuestros registros mantiene un saldo impago de $ $deuda.
A fin de evitarle mayores gastos y costos, lo estamos invitando a regularizar su deuda en cualquier sucursal de PAGOFÁCIL  y RAPIPAGO con esta carta, utilizando a tal fin el código de barras impreso."),'','J');


$pdf->SetY(165);
$pdf->SetX(45);
$pdf->MultiCell(40 ,5,utf8_decode("IMPORTE A ABONAR"),'','J');



$pdf->SetY(170);
$pdf->SetX(45);
$pdf->MultiCell(40 ,5,"$ ".$deuda,'','J');

$tar = $cli->idnum_tarjeta;

		$digito = digito_verificador($tar);

		$pdf->i25(40,175,'152'.$tar.$digito);


$pdf->SetY(165);
$pdf->SetX(130);
$pdf->MultiCell(70 ,5,utf8_decode("FECHA DE VENCIMIENTO"),'','J');

$fec_ve = fecha_servidor_total();
$ndias = "20";

$fecha_ven = suma_fechas($fec_ve, $ndias);
$mes_actual = mes_actual();
$ano_actual = ano_actual();
$fecha_25 = "25/".$mes_actual."/".$ano_actual;

if($fec_ve > $fecha_25){
	$mes_siguiente = mesSiguiente(cambiaf_a_bd($fecha_25));
	$fecha_vencimiento = "25/".$mes_siguiente."/".$ano_actual;
}else{
	$fecha_vencimiento=$fecha_25;
}
$pdf->SetFont('Arial','B',10);
$pdf->SetY(175);
$pdf->SetX(140);
$pdf->MultiCell(70 ,5,$fecha_vencimiento,'','J');


$pdf->SetY(195);
$pdf->SetX(25);
$pdf->MultiCell(150 ,5,utf8_decode("Vencido este plazo, queda Ud. notificado que procederemos a iniciar las acciones judiciales tendientes a efectivizar el cobro de los importes reclamados, más los intereses, gastos y costos generados. Con el agravado de informar a Centralizadoras de Riesgo Crediticio como CODEME y VERAZ su situación de deudor."),'','J');

$pdf->Line(20,250,180,250);

$pdf->SetY(255);
$pdf->SetX(135);
$pdf->MultiCell(45 ,4,"Agente Recaudador/Fiscalizador de Deuda",'','J');

$pdf->SetY(265);
$pdf->SetX(140);
//$pdf->MultiCell(45 ,4,"FIRMA",'','J');

$pdf->Image('../../image/Firma.jpg', '140', '215', '33', '33');
}

if ($cant < $total_rows)
	{
		$pdf->AddPage('P', 'A4');
	}
}

$pdf->Output();
?>
