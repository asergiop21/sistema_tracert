
<?php
include("../../menu.php");
require_once("../../Librerias/conn.php");
require("../../Controllers/ClientesController.php");
require("../../Controllers/LocalidadesController.php");
require("../../Controllers/PlanesController.php");
require("../../Controllers/OrganizacionesController.php");
   // $cliente_id = '5400003277'; //$_REQUEST['clie'];
    $mensaje_error='';
    $mensaje_correcto='';
    //$sql_cliente =pg_exec($db, "select * from preclientes where idpreclientes = '5400003952'");
    //$row_pre = pg_fetch_object($sql_cliente);
    //$sql_datos_server = get_datos_server($cliente_id);

    if (isset($_POST['guardar'] ))
    {
         $cliente = set_nuevo_cliente($_POST);

         if ($cliente == true){
          $mensaje_correcto = "Cliente dado de Alta con exito";
         } else {
            $mensaje_error ="Error al dar de alta";
         }      
    }

    $localidades = listar_localidades();
    $planes = listar_planes();
    $organizaciones = listar_organizaciones();
    $tipos_comprobantes = pg_exec($db, "SELECT * FROM tipos_comprobantes order by nombre asc");
    

    ?>
    
    <body>
     <form  class="form" id="frmdatos_nuevo" name="frmdatos_nuevo" method="post" >


        <?php if ($mensaje_error != ""){ ?>
  <div class="alert alert-danger">
    <h4><?php echo $mensaje_error; ?></h4>
  </div>
<?php } ?>

<?php if ($mensaje_correcto != ""){ ?>
  <div class="alert alert-success">
    <h4><?php echo $mensaje_correcto; ?></h4>
  </div>
<?php
}
?>
      <div class="container">
          <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
              <div class="stepwizard-step span3"> 
                <a href="#step-1" type="button" class="btn btn-success  btn-circle">1</a>
                <p><small>Alta Cliente</small></p>
              </div>
              
              <div class="stepwizard-step span3"> 
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Facturacion</small></p>
              </div>
            </div>
          </div>
 
          <?php include 'partials/datos_cliente.php' ?>
          <div class="panel panel-primary setup-content" id="step-3">
            <?php include 'partials/datos_facturacion.php' ?>
        </div>

     </div>
  </form>


