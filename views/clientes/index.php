<?php
require_once("../../Librerias/conn.php");
require_once('../../Controllers/ReportesController.php');
require_once('../../Controllers/ClientesController.php');
require_once('../../Controllers/PlanesController.php');

$db= Conec_con_pass();

if ($_REQUEST['imprimir'] == true){
   $mes = date('m');
   $ano = date('Y');
   $a = generar_comprobantes_pdf($_REQUEST['cliente_id'],$mes, $ano);
}

if(isset($_POST['restaurar'])){
    $cliente_id = $_POST['restaurar'];
    $restaurar_cliente = restaurar_cliente_baja($cliente_id);

    if($restaurar_cliente[0] == 'true'){
         $mensaje_correcto =$restaurar_cliente[1];
    }else{
         $mensaje_error = $restaurar_cliente[1];
    }
}
$estado_cliente = 'false';

    $codni = $_POST["dni"];
    $coape = $_POST["apellido"];
    $conom = $_POST["nombre"];
    $cozona = $_POST["localidad"];
    $conumtar = $_POST["num_tarjeta"];
    $status = $_POST["status"];
    $estado_cliente = $_POST['estado_cliente'];
    $plan_id = $_POST['plan_id'];
    $sql = "select instalaciones.*, clientes.*, localidad.*, p.nombre as plan_nombre from (clientes 
                                inner join instalaciones on clientes.idclientes = instalaciones.idclientes)
                                inner join localidad on clientes.localid = localidad.idlocalidad 
                                inner join planes p on instalaciones.plan_id = p.id
                                where elim_clie=".$estado_cliente;

    if ($codni == "" && $coape == "" && $conom == "" && $cozona == ""  && $conumtar =="" && $status =="" && $plan_id ==""){
    $sql .= " order by clientes.apellido asc " ;
    }else{

    if ($codni != ""){
        $sql .= " and  dni ='" . trim($codni) . "'";
    }
    if ($coape != ""){
        $sql .= " and clientes.apellido ilike '" . trim($coape) . "%'";
    }
    if ($conom != ""){
        $sql .= " and clientes.nombre ilike '" . trim($conom) . "%' ";
    }
    if ($cozona != ""){
        $sql .= " and clientes.localid = '$cozona'";
    }
    if ($conumtar != ""){
        $sql .= " and clientes.idnum_tarjeta ilike '" .  trim ($conumtar) . "%' ";
    }
    if ($status != ""){
        $sql .= " and clientes.elim_ser ='". trim($status) . "' ";
    }
    if ($plan_id != ""){
        $sql .= " and instalaciones.plan_id='". trim($plan_id) ."'";        
    }
     

     $sql .= " order by clientes.apellido asc "  ;
    }
    
    $result = pg_exec($db, $sql);   

    $fdev= pg_num_rows($result);
    $count = "select count (idclientes) from clientes";     
    $count = 0;

require_once("../../menu.php");
$planes = listar_planes();
?>  
    <h1 class="titulo"> CONSULTA DE CLIENTES</h1> <hr width="60%" align="left"> 


    <form name="frmdatos" method="post" action="#">
<?php if ($mensaje_error != ""){ ?>
  <div class="alert alert-danger">
    <h4><?php echo $mensaje_error; ?></h4>
  </div>
<?php } ?>

<?php if ($mensaje_correcto != ""){ ?>
  <div class="alert alert-success">
    <h4><?php echo $mensaje_correcto; ?></h4>
  </div>
<?php
}
?>
        <div class="span12">
            <div class="span2">
               <label>Ingrese  Dni:</label>
               <input type="text" name="dni" id="dni" maxlength="8"  class="span2" value="<?php if($codni !=''){echo $codni;} ?>"/>
           </div>
           <div class="span2">
            <label>Ingrese  Num Tarjeta:</label>
            <input type="text" name="num_tarjeta" id="num_tarjeta" maxlength="10"  class="span2" value="<?php if($conumtar !=''){echo $conumtar;} ?>"/>
        </div>
        <div class="span3">
            <label>Ingrese  Apellido:</label>
            <input type="text" name="apellido" id="apellido" class="span3" value="<?php if($coape !=''){echo $coape;} ?>"/>
        </div>
        <div class="span3">
            <label>Ingrese Nombre:</label>
            <input type="text" name="nombre" id="nombre" class="span3" value="<?php if($conom !=''){echo $conom;} ?>"/>
        </div>
    </div>
    <div class="span12">
        <div class="span2">
            <label>ZONA</label>
            <?php
            $consulta  = "SELECT * FROM localidad order by dpto asc";
            $resultado = pg_query($consulta) or die('La consulta fall&oacute;: ' . pg_error());
            echo "<select name='localidad' id='localidad' tabindex='9' class='span2'>";
            echo "<option  selected>";
            while ($linea = pg_fetch_object($resultado)) {
                if($cozona != ''){
                    if($linea->idlocalidad == $cozona){
                    echo " <option value='$linea->idlocalidad' selected> $linea->dpto - $linea->num_loc </option>";
                    //foreach ($linea as $valor_col) {
                    }else{
                        echo " <option value='$linea->idlocalidad'> $linea->dpto - $linea->num_loc </option>";
                    }
            }else{
                echo " <option value='$linea->idlocalidad'> $linea->dpto - $linea->num_loc </option>";
            }
        }
            echo "</select>";
                // Liberar conjunto de resultados
            pg_free_result($resultado);
                // Cerrar la conexion
            pg_close($db);  
            ?>
        </div>
        <div class="span2">
            <label>Estado Servicio</label>
            <select name="status" id="status" class="span2">
                <option value =""></option>
                <option value ="1">Habilitado</option>
                <option value="2">Aviso</option>
                <option value="0">Deshabilitado</option>
            </select>
        </div>
        <div class="span2">
            <label>Estado Cliente</label>
            <select name="estado_cliente" id="estado_cliente" class="span2">
                <?php if($estado_cliente =='true'){ ?>
                <option value="true" selected>No activo</option>
                <option value ="false">Activo</option>
                <?php }else{?>
                <option value="true">No activo</option>
                <option value ="false" selected>Activo</option>
                <?php }?>
            </select>
        </div>

<div class="span4">
        <div class="span2">
            <label>Planes</label>
            <?php
            
            echo "<select name='plan_id' id='plan_id' tabindex='9' class='span2'>";
            echo "<option></option>";
            while ($plan = pg_fetch_object($planes)) {
              
                    if($plan->id == $plan_id){
                    echo " <option value='$plan->id' selected> $plan->nombre</option>";
                    //foreach ($linea as $valor_col) {
                    }else{
                        echo " <option value='$plan->id'> $plan->nombre</option>";
                    }
           
        }
            echo "</select>";
                // Liberar conjunto de resultados
            pg_free_result($resultado);
                // Cerrar la conexion
            pg_close($db);  
            ?>
        </div>

    </div>
    <div class="span12">
      <input type="submit" class="btn btn-primary" name="consulta" id="consulta" value="consultar">
  </div>

  <?php if ($fdev > 0 ){?>
  <table class="table table-striped" >
    <tr>
        <th></th>
        <th></th>
        <?php if($estado_cliente == 'true'){?>
            <th></th>
        <?php } ?>
        <th>Apellido y Nombre</th>
        <th>Localidad</th>
        <th>DNI</th>
        <th>Telefono</th>
        <th>Celular</th>
        <th>Plan</th>
    </tr>
   
    <?php

    while ($row = pg_fetch_object($result)) {               
        $count = $count + 1;
        $idclie = $row->idclientes;
        $sql_pagos = pg_exec($db, "Select * from pagos where idclientes = '$idclie'");

        $sum_import = 0;
        $sum_pagado = 0;
        $deuda = 0;

        while  ($row_pa = pg_fetch_object($sql_pagos)){
            $sum_import =$row_pa->importe_deuda + $sum_import;
            $sum_pagado = $row_pa->importe_pagado + $sum_pagado;
            $deuda = $sum_pagado - ($sum_import );
            $deuda_total =   $sum_pagado - $sum_import;
        }
            ?>  
            <tr>
                                 <?php if($row->elim_clie == 't'){?>
                        <td><button type="submit" value="<?php echo $row->idclientes?>" id="restaurar" name="restaurar" onclick="return confirm('Desea Habilitarlo?')">Restaurar</button></td>
                        <td><a href="imprimir_aviso.php?clie=<?php echo $row->idclientes; ?>">Carta</a></td>
                <?php }else{?>
                <td><a href="eliminar_clienteht.php?num_clie=<?php echo $row->idclientes ?>&elim=0&user_id=<?php echo $_SESSION['iduser'] ?>">Eliminar</a></td>
                <?php }?>
                <td><a href="?cliente_id=<?php echo $row->idclientes?>&imprimir=true;">Imprimir</a></td>
                <td><p><a href="<?php echo BASE_URL ?>historial.php?clie=<?php echo $row->idclientes; ?>" target="_blank" ><?php echo $ayp = $row->apellido.", ".$row->nombre;?></a> </p></td>
                <td><?php echo $row->num_loc." - ". $row->dpto;?></td>
                <td><?php echo $row->dni;?></td>
                <td><?php echo $row->tel_fijo;?></td>
                <td><?php echo $row->celular;?></td>
                <td><?php echo $row->plan_nombre;?></td>
            </tr>
            <?php
        }
    
    ?>
</table >
<?php }?>
</div>

<?php
if (isset($Conn)) {
    DesconectarBD($Conn);
}
?>
</form>
