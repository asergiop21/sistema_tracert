<?php
include_once("../../menu.php");
require_once("../../Librerias/conn.php");
require_once("../../Librerias/Librerias_nuevas.php");
require_once("../../Controllers/ClientesController.php");
require_once("../../Controllers/LocalidadesController.php");


$db= Conec_con_pass();
$cliente_id = $_REQUEST["cliente_id"];

$submit = $_POST['guardar'];


if ($submit == 'Actualizar' )
{
  $datos = $_POST;
  
  $update_clientes  = update_clientes($datos);
 
  /*
      if (pg_affected_rows($clientes))
      {
          pg_query("COMMIT");
          echo "<div class ='alert-success'>Actualizado Exitosamente </div>";
      }else{
          pg_query("ROLLBACK");
          echo "<div class ='alert-warning'>Registro no Actualizado </div>";
      }*/
      }

  $row =get_cliente($cliente_id);
  $localidades  = listar_localidades();
  $planes = pg_exec($db, "SELECT * FROM planes order by nombre asc");
  $telefonos = pg_exec($db, "SELECT * FROM phones where cliente_id = '$cliente_id'");
  $estado = pg_exec($db, "Select * from server where idclientes = '$cliente_id'");
  $estado = pg_fetch_object($estado); 
  $organizacion = pg_exec($db, "select * from organizaciones");
?>

  <h1> Editar Clientes </h1>
    <form name ="frmdatos" action="" method="POST">
      <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a href="">Datos</a></li>
        <li><a href="<?php echo BASE_URL; ?>views/clientes/instalacion.php?cliente_id=<?php echo $cliente_id;?>">Instalacion</a></li>
        <li><a href="<?php echo BASE_URL; ?>views/clientes/asignar_ip.php?cliente_id=<?php echo $cliente_id;?>">IPs</a></li>
        <li><a href="<?php echo BASE_URL; ?>views/clientes/asignar_equipos.php?cliente_id=<?php echo $cliente_id;?>">Asignar Equipos</a></li>
        <li><a href="<?php echo BASE_URL; ?>historial.php?clie=<?php echo $cliente_id;?>">Historial</a></li>
      </ul>
   <div class="row">
      <div class="span12">
        <div class="span5">

      <div>
          N° CLIENTE:<br>
          <input type="text" name="cliente_id" class="span1" value="<?php echo $row->idclientes; ?>" readonly> - <input type="text" name="idnum_tarjeta" class="span2" value="<?php echo $row->idnum_tarjeta; ?>" readonly>
        </div>
       <div>
          APELLIDO:<br>
          <input type="text" name="apellido" value="<?php echo $row->apellido; ?>">
      </div>
      <div>
          NOMBRE:<br>
          <input type="text" name="nombre" value="<?php echo $row->nombre; ?>">
      </div>
      <div>
          DOMICILIO:<br>
          <input type="text" name="domicilio" value="<?php echo $row->domicilio; ?>">
      </div>
      <div>
          BARRIO:<br>
          <input type="text" name="barrio" value="<?php echo $row->barrio; ?>">
      </div>
      <div>
          REFERENCIA:<br>
          <input type="text" name="referencia" value="<?php echo $row->entre; ?>">
      </div>
     <div>
    FECHA DE NACIMIENTO:<br>
    <input type="text" name="fecha_nacimiento" value="<?php echo cambiaf_a_normal($row->fecha_nac); ?>">

      </div> 
       <div>
          DNI:<br>
          <input type="text" name="dni" value="<?php echo $row->dni; ?>">
      </div>
      
         </div>
         <div class="span6">
<div>
          CUIT:<br>
          <input type="text" name="cuit" value="<?php echo $row->cuit; ?>">
      </div>
    <div>
          LOCALIDAD:<br>
           <?php
          $localidad_id = $row->localid;
          echo "<select name='localidad_id' id='localidad_id'>";
          while ($localidad = pg_fetch_object($localidades)){
           if ($localidad_id == $localidad->idlocalidad){
            echo "<option value='$localidad->idlocalidad' selected > $localidad->num_loc - $localidad->dpto </option>";
          }else{
           echo "<option value='$localidad->idlocalidad'> $localidad->num_loc - $localidad->dpto </option>";
         }
       }
       echo "</select>";
       ?>
     </div>

    <div class="input_field" id="phone_number_form"  >
    TELEFONO: <br>
<?php
    while($phone = pg_fetch_object($telefonos))
    { $i = $phone->id;
?>
      <input type="text" name="telefono[<?php echo $i ?>]"  value="<?php echo $phone->number_phone; ?>"> <br>
<?php
    }
?>
    <input type="button" value="Agregar Telefono" id="add_phone_number" class="btn btn-primary">

    </div>
    <div>
      OBSERVACIONES:<br>
      <textarea rows="5" name = "observacion" id="observacion" ><?php echo $row->observacion; ?></textarea>
    </div>

    <div>
     EMAIL:<br>
     <input type="text" name="email" value="<?php echo $row->email; ?>">
   </div>
   <div>
   
   

   </div>
   
 </div>
</div>


</div>
<input type="hidden" id="count_phones" name="count_phones">
<input type="submit" value="Actualizar" id="guardar" name="guardar" class="btn btn-primary">
</form>
