<?php
require('clases/cliente.class.php');

include "menu.php";
$objCliente=new Cliente;
$consulta=$objCliente->mostrar_clientes();
?>
<html>
    <head>
<script type="text/javascript">
$(document).ready(function(){
	// mostrar formulario de actualizar datos
	$("table tr .modi a").click(function(){
		$('#tabla').hide();
		$("#formulario").show();
		$.ajax({
			url: this.href,
			type: "GET",
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});

	// llamar a formulario nuevo
	$("#nuevo a").click(function(){
		$("#formulario").show();
		$("#tabla").hide();
		$.ajax({
			type: "GET",
			url: 'nuevo.php',
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
});

</script>
<script src="js/jquery-1.3.1.min.js" type="text/javascript"></script>
<script src="js/jquery.functions.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="CSS/estilo_nuevo.css" />
 </head>
    <body>

<h1> Consulta de Localidades</h1>
<hr>
<div id="contenedor">
        <div id="formulario" style="display:none;">
    </div>
        <div id="tabla">
  <span id="nuevo"><a href="nuevo.php"><img src="img/add.png" alt="Agregar datos" ></a></span>
	<table>
   		<tr>
                    <th>Nombres</th>
                    <th>CP</th>
                    <th>Departamento</th>
                    <th></th>
                    <th></th>
        </tr>
<?php
if($consulta) {
	while( $cliente = pg_fetch_array($consulta) ){
	?>

		  <tr id="fila-<?php echo $cliente['idlocalidad'] ?>">
			  <td><?php echo $cliente['num_loc'] ?></td>
                          <td><?php echo $cliente['cp'] ?></td>
                          <td><?php echo $cliente['dpto'] ?></td>

			  <td><span class="modi"><a href="actualizar_local.php?id=<?php echo $cliente['idlocalidad'] ?>"><img src="img/database_edit.png" title="Editar" alt="Editar" /></a></span></td>
			  <td><span class="dele"><a onClick="EliminarDato(<?php echo $cliente['idlocalidad'] ?>); return false" href="eliminar.php?id=<?php echo $cliente['id'] ?>"><img src="img/delete.png" title="Eliminar" alt="Eliminar" /></a></span></td>
		  </tr>
	<?php
	}
}
?>
    </table>
        </div>
</div>
    </body>
</html>