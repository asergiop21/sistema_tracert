$().ready(function() {


  function formatItem(row){

    return row[0] +"(" + row[1] +")";
  }
  function formatResult(row) {
    return row[0].replace(/(<.+?>)/gi, '');
  }

  $('#enviar_nota').on('click', function (e) {

    $('#enviar_nota').hide(true);

});

  $("#clientenota").autocomplete("../../Librerias/search.php", {
    width:250,
    max : 50,
    selectFirst: false,
    autoFill: true,
    formatItem: formatItem
  });
  var cl_id;
  $("#clientenota").result(function(event, data, formatted) {
    if (data)
			//$(this).parent().next().find("input").val(data[1]);
    document.getElementById("idclientes_nc").value = data[1];
    document.getElementById("num_tar_nc").value = data[2];
    document.getElementById("cl_nya_nc").value = data[0];
    cliente_id = data[1];
    var data_pagos='';

    $.ajax({
      type:'POST',
      url:'../../Librerias/datosComprobantes.php',
      data:'cliente_id='+cliente_id,
      success:function(data_pagos){
        $('#pagos_comprobantes').empty();
        $.each(data_pagos, function(a,b){
          $('#comprobante_id').append("<option value='"+b[0]+"'>"+b[0]+" - " + b[1] +" - " + b[2]+ "</option>");   
        });
      }
    }); 
  });

  $("#clientenota").keypress(function(event) {
           // alert(event.keyCode);
           if (event.keyCode=='13') $this.closest('form').submit();
         });


  $("#cliente").autocomplete("../../Librerias/search.php", {
    width:250,
    max : 50,
    selectFirst: false,
    autoFill: true,
    formatItem: formatItem
  });

  $("#cliente").result(function(event, data, formatted) {
    if (data)
      console.log(data);  
      //$(this).parent().next().find("input").val(data[1]);
    document.getElementById("idclientes_abono").value = data[1];
    document.getElementById("num_tar").value = data[2];
    document.getElementById("cl_nya").value = data[0];
    cliente_id = data[1];
    var data_pagos='';

  });

  $("#cliente").keypress(function(event) {
           // alert(event.keyCode);
           if (event.keyCode=='13') $this.closest('form').submit();
         });

  $("#usuario").autocomplete("../../Librerias/search_user.php", {
    width:250,
    max : 50,
    selectFirst: false,
    autoFill: true,
    formatItem: formatItem

  });

  $("#usuario").result(function(event, data, formatted) {
    if (data)
                                //$(this).parent().next().find("input").val(data[1]);
                              document.getElementById("iduser").value = data[1];

                            });

  $("#usuario_rec").autocomplete("../../Librerias/search_user.php", {
    width:250,
    max : 50,
    selectFirst: false,
    autoFill: true,
    formatItem: formatItem

  });

  $("#usuario_rec").result(function(event, data, formatted) {
    if (data)
                                //$(this).parent().next().find("input").val(data[1]);
                              document.getElementById("iduser_rec").value = data[1];

                            });

  $("#cta_gas").autocomplete("../../Librerias/search_cta.php", {
    width:250,
    max : 50,
    selectFirst: false,
    autoFill: true,
    formatItem: formatItem


  });

  $("#cta_gas").result(function(event, data, formatted) {
    if (data)
                                //$(this).parent().next().find("input").val(data[1]);
                              document.getElementById("idcc_gas").value = data[1];

                            });


  $('#guardar_pagos').on('click', function(){

    $('#frmdatos').validate({

     rules: {
      'num_fac': 'required',
      'cond_vta': 'required',
      'tipo_tarjeta': 'required',
      'txt_imp_pag': 'required',

    },
    messages: {
     'num_fac': 'Debe ingresar el nombre',

   }}

   );


  });


  $("#tipo_tarjeta").hide();
  $("#cond_vta").change(function(){

    $("#tipo_tarjeta").show();
    $("#tipo_tarjeta").children('option').hide();
    $("#tipo_tarjeta").children("option[id^=" + $(this).val() + "]").show();

  })
});