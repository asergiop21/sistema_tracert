<?php
include "menu.php";

require_once "Librerias/conn.php";
require_once "Controllers/ControllerPagos.php";
require_once "Controllers/ControllerTratamientos.php";
require_once "Controllers/ControllerVisitastecnicas.php";
require_once "Controllers/ControllerInstalaciones.php";
//nuevo Model
$visitas = index();

while ($row = pg_fetch_object($visitas))
{
        //Consulta para soportes pendientes 
        $sop_pend =  $row->id_sop_pend;
        //Consulta cambios de equipos pendientes 
        $camb_pend = $row->id_camb_equi;
        //Consulta equipos a retirar 
        $equip_retirar = $id_equi_retirar; 
}

$cobranzas = mostrar();
while ($row_cob = pg_fetch_object($cobranzas))
{
          $user_con_promesas = $row_cob->idc_usuario_promesa;
          $user_sin_promesas = $row_cob->idc_usuario_deuda_s_prom;
          $deuda_total = $row_cob->idc_deuda_total;
          $usuario_con_deuda = $row_cob->idc_usuario_c_deuda;


}

$instalaciones =  mostrar_instalaciones();

while ($row_insta = pg_fetch_object($instalaciones))
{
          //consulta instalaciones pendientes
          $inst_pend = $row_insta->idi_inst_pend;
          //consulta instalacion sin equipos 
          $inst_pend_sinasig =  $row_insta->idi_inst_pend_s_asig;
          //consulta instalacion sin equipos wifi 
          $inst_pend_sinasigwifi =  $row_insta->idi_inst_pend_s_wifi;
}

$tratamientos = mostrar_datos_tratamientos();
while ($row_tra = pg_fetch_object($tratamientos))
{
    $cant_user_tratam = $row_tra->idt_usuario_tratam;
    $cant_user_tratam_prome = $row_tra->idt_usuario_prom_tratam;
}

?>
<html>
  <head>
  
<link type="text/css" rel="stylesheet" href="CSS/estilo_nuevo.css" /> 
<link type="text/css" rel="stylesheet" href="CSS/bootstrap/bootstrap.css" /> 
        <link href="CSS/thickbox.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="CSS/jquery-ui.css" />
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/thickbox/thickbox.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>
</head>
  <body>
    <div>
      <form>
        <div class="span12">
           <h3>Instalaciones y Stock</h3> 
            <div class="span5">
              <label class="span4">Soporte Pendiente</label>  
              <input type="text" class="input-mini" value="<?php echo $sop_pend; ?> "> 
            </div>     
            <div class="span5">
              <label class="span4">Demoras Soporte </label>  
              <input type="text" class="input-mini"> 
            </div>     
            <div class="span5" >
               <label class="span4">Instalaciones Pendiente </label>  
               <input type="text"class="input-mini" value="<?php echo $inst_pend;?> ">  
            </div>     
            <div class=span5>
              <label class="span4">Demora Instalacion </label>  
              <input type="text" class="input-mini"> 
            </div>     
        <div class="span12"><hr></div>
            <div class="span5">
             <label class="span4">Instalaciones Sin Equipo</label>  
             <input type="text" class="input-mini" value="<?php echo $inst_pend_sinasig ; ?> "> 
            </div>     
            <div class="span5">
              <label class="span4">Cambios de Equipos Pendientes</label>  
              <input type="text" class="input-mini" value="<?php echo $camb_pend; ?> "> 
            </div>     
      <div class="span12"><hr></div>
           <div class="span5">
              <label class="span4">Total equipos necesarios</label>  
              <input type="text" class="input-mini"  value="<?php echo $equip_neces = $inst_pend_sinasig + $camb_pend; ?> "> 
          </div>     
          <div class="span5">
              <label class="span4">Equipos en Stock</label>  
              <input type="text" class="input-mini"> 
          </div>    
          <div class="span5" >
               <label class="span4">Total eq wifi neces. </label>  
               <input type="text"class="input-mini" value="<?php echo $inst_pend_sinasigwifi ; ?> "> 
          </div>     
          <div class="span5">
              <label class="span4">Equipos a retirar</label>  
              <input type="text" class="input-mini" value="<?php echo $equip_retirar; ?> ">  
          </div>    
<div class="span12"><hr></div>
      <div class="span12">
           <h3>Cobranzas</h3>
</div> 
          <div class="span5" >
              <label class="span3">Deuda total</label>  
              <input type="text" class="input-small" value="<?php echo $deuda_total;?> "> 
         </div>
        <div class="span5">
               <label class="span3">usuario con  deuda</label>  
              <input type="text" class="input-small" value="<?php echo $usuario_con_deuda;?> "> 
        </div>
        <div class="span5">
               <label class="span3">usuario en deuda sin promesas</label>  
              <input type="text" class="input-small" value="<?php echo $user_sin_promesas;?> "> 
        </div>
        <div class="span5">
               <label class="span3">usuario con promesas</label>  
              <input type="text" class="input-small" value="<?php echo $user_con_promesas;?> "> 
        </div>  
        <div class="span12"> 
        <td><a href="grafico2.php?TB_iframe=true&height=400&width=700&opcion=0&nd__d=<?php echo $row_ca->idclientes?>" title="Promesas de Pagos...." class="thickbox">Grafico Cobranzas Diario</a></td></div>
<div class="span12"><hr></div>

<div class="span12">          <h3>Tratamientos</h3> </div> 
          <div class="span5" >
              <label class="span3">Total recaudado tratamiento</label>  
              <input type="text" class="input-small" value="<?php //echo $deuda_total;?> "> 
         </div>
         <div class="span5" >
              <label class="span3">Usuarios en Tratamientos</label>  
              <input type="text" class="input-small" value="<?php echo $cant_user_tratam ;?> "> 
         </div>
         <div class="span5" >
              <label class="span3">Promesas vencidad en el mes</label>  
              <input type="text" class="input-small" value="<?php //echo $deuda_total;?> "> 
         </div>
         <div class="span5" >
              <label class="span3">Usuarios con Promesas Tratamientos</label>  
              <input type="text" class="input-small" value="<?php echo $cant_user_tratam_prome;?> "> 
         </div>
          <div class="span5" >
              <label class="span3">Promesas cumplida en el mes</label>  
              <input type="text" class="input-small" value="<?php echo $datos;?> "> 
         </div>




</div>
    </form>
    </div>
  </body>
</html>
