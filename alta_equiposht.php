<?php
include "menu.php";
require_once "Librerias/conn.php";
?>

<html>

    <head>
        <script type="text/javascript">

 function rellenaCombo(formulario)
      {

        with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci�n.
        {
          var centro = idtipo[idtipo.selectedIndex].text; // Valor seleccionado en el primer combo.
          var n = idsub.length;  // Numero de l�neas del segundo combo.

          idsub.disabled = false;  // Activamos el segundo combo.

          for (var i = 0; i < n; ++i)
            idsub.remove(idsub.options[i]); // Eliminamos todas las l�neas del segundo combo.

          idsub[0] = new Option("Seleccione Subtipo", 'null'); // Creamos la primera l�nea del segundo combo.

          if (centro != 'null')  // Si el valor del primer combo es distinto de 'null'.
          {
   <?php
   // CODIGO PHP
    // Para cada centro, construimos el segundo combo con los empleados del mismo.
$db = Conec_con_pass();

    $cons_cen = @pg_exec($db, "SELECT * FROM marcas;");

    for ($l = 0; $l < pg_numrows($cons_cen); ++$l)
    {
     $cen = @pg_fetch_object($cons_cen, $l);
   ?>
            if (centro == '<?php echo $cen->marcas;?>')
            {

    <?php
    // CODIGO PHP
      // Construimos los valores del segundo combo con los empleados del centro.

      $cons_emp = @pg_exec($db, "SELECT  * FROM modelos WHERE idmarcas = ".$cen->idmarcas);

      for ($m = 0; $m < pg_numrows($cons_emp); ++$m)
      {
       $emp = @pg_fetch_object($cons_emp, $m);
    ?>
              idsub[idsub.length] = new Option('<?php echo $emp->modelos;?>','<?php echo $emp->idmodelos;?>' );
    <?php
    // CODIGO PHP
      }
    ?>
            }
   <?php
   // CODIGO PHP
    }
   ?>
            idsub.focus();  // Enviamos el foco al segundo combo.
          }
          else  // El valor del primer combo es 'null'.
          {
            idsub.disabled = true;  // Desactivamos el segundo combo (que estar� vac�o).
            idtipo.focus();  // Enviamos el foco al primer combo.
          }

          idsub.selectedIndex = 0;  // Seleccionamos el primer valor del segundo combo ('null').
        }

      }

    function test_mac()
    {
        var mac = document.getElementById("eq_mac").value;

        regex=/^([0-9a-f]{2}){6}$|([0-9a-f]{4}([.]|$)){3}$/i;
        if (mac != "")
            {
        if (!regex.test(mac))
        {
            alert("Mac no es valida");
            document.all.eq_mac.value = "";
            document.all.eq_mac.focus();
        }
        else
            {
                var url = 'validar_mac.php?mac=' + mac; // creaci�n de la URL.
                http.open("GET", url, true); // fijando los parametros para el env�o de datos.
                http.onreadystatechange = handler; // Qu� funci�n utilizar en caso de que el estado de la petici�n cambie.
                http.send(null); // enviar petici�n.

        }
}}

 // Esta funci�n se encarga de crear el objeto XMLHTTPRequest y lo devuelve.
function getXMLHTTPRequest() {
  try {
    req = new XMLHttpRequest();
  } catch(err1) {
    try {
      req = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (err2) {
      try {
        req = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (err3) {
        req = false;
      }
    }
  }
  return req;
}
var http = getXMLHTTPRequest(); // creo una instancia del objeto XMLHTTPRequest.


function handler() {
  if (http.readyState == 4) {
    if(http.status == 200) {
		//alert(http.responseText);

                var mac12 =  http.responseText.trim();

                if (mac12 > '0')
                    {
                        alert ("La Mac ya existe");
                        document.getElementById("eq_mac").value = "";
                         document.all.eq_mac.focus();
                    }
    }
  }
}

function validar()
{
    var tip = document.getElementById("idtipo").value;
    var sub = document.getElementById("idsub").value;
    var mac = document.getElementById("eq_mac").value;
    var est = document.getElementById("eq_est").value;



    if (tip == "" || sub =="" || mac =="" || est =="")
        {
            alert ("Complete los campos con *");
            return false;
        }
        else
            {
                return true;
            }

}
    </script>



<link rel="stylesheet" type="text/css" href="CSS/estilo.css">
    </head>
    <body>
    
        <form action="alta_equipos.php" method="post" name="frmdatos" onsubmit="return validar()">
<div class="titulo">
		<h1> ALTA DE EQUIPOS</h1>
	</div>

<hr>
            <table>
                <tr>
                       <td class="nombre_campos">* Marca</td>

                    <td>
		 <select name="idtipo" id="idtipo" onchange=" rellenaCombo('frmdatos');"   tabindex="1">
              <option value="null" selected>Seleccione Marcas
              
			<?php
			// CODIGO PHP
			 // Contruimos el primer combo con los valores de la tabla 'centros'.
			 $db = Conec_con_pass();
			 $cons_centros = @pg_exec($db, "SELECT * FROM marcas;");
			 
			 for ($k = 0; $k < pg_numrows($cons_centros); ++$k)
			 {
			  $centro = @pg_fetch_object($cons_centros, $k);
			  echo "               <option value=\"".$centro->idmarcas."\"  >".$centro->marcas."\n";
			 
			  }
			?>
          </select>       
           </td>

           <td>* Modelos</td>
        <td>

            <select name="idsub" id="idsub"  tabindex="2">
            <option value="null" disabled >Seleccione Modelos
          </select>
    	
    	 </td>
                </tr>
                <tr>
                    <td class="nombre_campos">* Mac:</td>
                    <td><input type="text" id="eq_mac" name="eq_mac" maxlength="12" onblur="this.value=this.value.toUpperCase(); test_mac()"  tabindex="3" > </td>
                </tr>
                <tr>
                    <td width="10%" class="nombre_campos"><span>Proveedor </span></td>
		<td>
    	  <?php
    	  // Conexion, seleccion de base de datos
	
				// Realizar una consulta SQL
				$consulta  = "SELECT * FROM proveedor";
				$resultado = pg_exec($db, $consulta) or die('La consulta fall&oacute;: ' . pg_error());
				// Impresion de resultados en HTML

				echo "<select name='eq_prov' id='eq_prov'   tabindex='4'>";
				echo "<option  selected>";
					while ($linea = pg_fetch_object($resultado)) {

   					//foreach ($linea as $valor_col) {
				echo " <option value='$linea->idproveedor'> $linea->pv_nombre </option>";
				 }
				echo "</select>";
				// Liberar conjunto de resultados
				pg_free_result($resultado);
				// Cerrar la conexion
				pg_close($conexion);
    		?>
    </td>
                </tr>
                 <tr>
                     <td class="nombre_campos" >* Estado:</td>
                     <td><select id="eq_est" name="eq_est"  tabindex="5">
                            <option value="1">Stock</option>
                 </select></td>
                </tr>
                
 <tr>
                     <td class="nombre_campos" >* Referencia:</td>
                     <td><select id="eq_ref" name="eq_ref"   tabindex="5">
                            <option value="1">Clientes</option>
                            <option value="2">Repetidora</option>
                 </select></td>
                </tr>
                
                
                
                <tr>
                    <td class="nombre_campos" >WIFI</td>
                    <td><input  type="checkbox" name="eq_wifi" id="eq_wifi" >   </td>
                    
                </tr>
                
                
                <tr>
                    <td class="nombre_campos">Observaciones:</td>
                    <td><textarea rows="6" cols="40" id="eq_obs" name="eq_obs" ></textarea></td>
                </tr>

              
            </table>
<table>
        <tr>
            <td><input type="submit" class="boton" value="Guardar"  tabindex="7"> </td>
        </tr>
</table>

        </form>
        
    </body>
</html>