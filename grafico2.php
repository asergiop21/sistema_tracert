<?php
require "Librerias/conn.php";
$db = Conec_con_pass();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Raphaël · Analytics</title>
        <link rel="stylesheet" href="CSS/grafico/demo.css" type="text/css" media="screen">
        <link rel="stylesheet" href="CSS/grafico/demo-print.css" type="text/css" media="print">
        <script src="js/grafico/raphael.js"></script>
        <script src="js/grafico/popup.js"></script>
        <script src="js/grafico/jquery.js"></script>
        <script src="js/grafico/analytics.js"></script>
        <style type="text/css" media="screen">
            #holder {
                height: 250px;
                margin: -125px 0 0 -400px;
                width: 800px;
            }
        </style>
    </head>
    <body>

        <table id="data">


<?php $sql_c = pg_exec($db, "Select * from indicadores_cobranzas");

while ($row_1 = pg_fetch_object($sql_c))
{

?>

            <tfoot>
          <tr>
                  <th><?php echo $row_1->idc_date;?></th>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                   <td><?php echo $row_1->idc_deuda_total; ?></td>
                </tr>
            </tbody>


<?php }  ?>
        </table>
        <div id="holder"></div>
    </body>
</html>

